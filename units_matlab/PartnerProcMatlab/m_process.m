%disp('Matlab process')

n_blocks = 0;

% usually we would check whether the input data (either signal stream or event) was modified in last processing cycle
if (INPUTS_REFRESHED.('EVENT_DUMMY'))
	simulate_load = true;
end

if (INPUTS_REFRESHED.('STREAM_EEG'))

    % when data was refreshed, we can receive one or more data blocks along with their timestamp
	% blocks are concatenated in matlab, so we must check the length of the input signal
	n_blocks = size(STREAM_EEG.data,2) / 8;
	tot_blocks = tot_blocks + n_blocks;
	
	% copy input data to the output data, adding a new timestamp
	STREAM_DUMMY.data = STREAM_EEG.data;
	STREAM_DUMMY.t = repmat(hat,1,n_blocks);
	OUTPUTS_MODIFIED.('STREAM_DUMMY') = 1;
	
	if (simulate_load)
		if (time2sleep>0)
			pause(time2sleep/1000);
		end
		simulate_load = false;
	end
end
