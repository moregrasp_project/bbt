function [time] = hat
    %HAT High Accuracy Timer
    %  [TIME] = HAT uses the high-resolution performance counter
    %  UTC time format (unix) in microseconds from 1 Jan 1970
    [high, low] = mexhat;
    time = ((high*2^32+low)/10)-11644473600000000;
end
