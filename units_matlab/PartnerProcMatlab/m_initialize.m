disp('Matlab initialize')

% add the path to the hat function (high accuracy timer)
addpath(strcat(getenv('BZI_FRAMEWORK_ROOT'),'/units_matlab/hat'));

% PARAMETERS CAN BE ACCESSED
% PARAMS.dummyInt
% PARAMS.dummyIntVector
% ...

% We want to do some dummy processing (copy identical signal from STREAM_EEG to STREAM_DUMMY)
% when we receive an DUMMY EVENT then we sleep for some ms to simulate overload
time2sleep = PARAMS_PartnerProcMatlab.time2sleep;
tot_blocks = 0;
simulate_load = false;
