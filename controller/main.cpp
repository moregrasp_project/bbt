/*
* File: bbtLauncherGui.cpp
* Description: main application to load a configuration file
* Author: Carlos Escolano (carlos.escolano@bitbrain.es)
*		  BitBrain Technologies
* Date: 01/04/2015
*/

// operator gui
#include "moregraspController.h"
#include "moregraspGui.h"

using namespace std;

int main(int argc, char* argv[])
{
	// Qt app
	QApplication app( argc, argv );

	// controller
    MoregraspController *pController = new MoregraspController("Controller");
	pController->start();

    qApp->thread()->msleep(500);

	// operator gui
    MoregraspGui* pGui = new MoregraspGui("_tablet");
	pGui->show();

	// connect signal/slots
	QObject::connect(pGui, SIGNAL(sigLoadConfig(QString)),
					 pController, SLOT(onLoadConfig(QString)));
	QObject::connect(pController, SIGNAL(sigLoadedConfig(QStringList)),
					 pGui, SLOT(onLoadedConfig(QStringList)));

    QObject::connect(pGui, SIGNAL(sigLaunch(QString, QString)),
                     pController, SLOT(onLaunch(QString, QString)));
	QObject::connect(pController, SIGNAL(sigLaunched(bool)),
					 pGui, SLOT(onLaunched(bool)));
    QObject::connect(pController, SIGNAL(sigReseted(bool)),
                     pGui, SLOT(onReseted(bool)));
    QObject::connect(pController, SIGNAL(sigStatus(QString)),
                     pGui, SLOT(onStatus(QString)));


    QObject::connect(pGui, SIGNAL(sigStart()),
                     pController, SLOT(onStart()));
	QObject::connect(pGui, SIGNAL(sigStop()),
					 pController, SLOT(onStop()));
	QObject::connect(pGui, SIGNAL(sigReset()),
					 pController, SLOT(onReset()));
    QObject::connect(pGui, SIGNAL(sigFinish()),
                     pController, SLOT(onFinish()));
    QObject::connect(pGui, SIGNAL(sigSendData(QString)),
                     pController, SLOT(onSendData(QString)));

    return app.exec();
}

