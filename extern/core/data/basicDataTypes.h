/*
* File: basicDataTypes.h	
* Description: header file with the inclusion of all bzi basic data types
*		       and basicType template specializations.
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef BASIC_DATA_TYPES_H
#define BASIC_DATA_TYPES_H

#include <QRegExp>
#include "basicType.h"

namespace Bzi
{
    //no typedef used due to its expansion when RegisterData(BasicType<int>) ...

    class  BziInt: public BasicType<int>
    {
        public:
            BziInt(){}
            BziInt(int v):BasicType<int>(v){}
            BziInt(const BziInt& obj): BasicType<int>(obj){}

            //redefine
            Data* clone()
            {
                return new BziInt(*this);
            }
            //redefine
            virtual bool checkStr(const std::string &strValue)
            /**
             *@param const std::string &strValue: std::string to check format.
             *@pre PRE: --
             *@post POST: true==checkStr(s)-> s is a valid std::string rep. for BziInt
             */
            {
                std::cout << "BziInt::checkStr()->" << strValue <<std::endl;
                int pos;
                QRegExp rx("^(\\-?\\d+(\\.\\d+)?)$"); //only integers or floats! (CHECK!)
                QString q = QString::fromStdString(strValue );
                if ((pos= rx.indexIn(q))==-1) return false;
                else return true;
            }
    };

    class BziDouble: public BasicType<double>
    {
        public:
            BziDouble(){}
            BziDouble(double v): BasicType<double>(v){}
            BziDouble(const BziDouble& obj): BasicType<double>(obj){}

            //redefine
            Data* clone()
            {
                return new BziDouble(*this);
            }
            //redefine
            virtual bool checkStr(const std::string &strValue)
            /**
             *@param const std::string &strValue: std::string to check format.
             *@pre PRE: --
             *@post POST: true==checkStr(s)-> s is a valid std::string rep. for BziDouble
             */
            {
                std::cout << "BziDouble::checkStr()"<<std::endl;
                int pos;
                QRegExp rx("^(\\-?\\d+(\\.\\d+)?)$"); //only integers or floats! (CHECK!)
                QString q = QString::fromStdString(strValue );
                if ((pos= rx.indexIn(q))==-1) return false;
                else return true;
            }
    };

    class BziChar: public BasicType<char>
    {
        public:
            BziChar(){}
            BziChar(const BziChar& obj): BasicType<char>(obj){}

            //redefine
            Data* clone()
            {
                return new BziChar(*this);
            }
            //redefine
            virtual bool checkStr(const std::string &strValue)
            /**
             *@param const std::string &strValue: std::string to check format.
             *@pre PRE: --
             *@post POST: true==checkStr(s)-> s is a valid std::string rep. for BziChar
             */
            {
                std::cout << "BziChar::checkStr()"<<std::endl;
                int pos;
                QRegExp rx("^.$"); //Any character
                QString q = QString::fromStdString(strValue );
                if ((pos= rx.indexIn(q))==-1) return false;
                else return true;
            }
    };

    class BziString: public BasicType<std::string>
    {
        public:
            BziString(){}
            BziString(const std::string &str):BasicType<std::string>(str){}
            BziString(const BziString& obj): BasicType<std::string>(obj){}

            //redefine
            Data* clone()
            {
                return new BziString(*this);
            }
            //redefine
            virtual bool checkStr(const std::string &)
            {
                std::cout << "BziString::checkStr()"<<std::endl;
                return true;
            }
    };

    class BziBool: public BasicType<bool>
    {
        public:
            BziBool(){}
            BziBool(double v): BasicType<bool>(v){}
            BziBool(const BziBool& obj): BasicType<bool>(obj){}

            //redefine
            Data* clone()
            {
                return new BziBool(*this);
            }
            //redefine
            virtual bool checkStr(const std::string &strValue)
            /**
             *@param const std::string &strValue: std::string to check format.
             *@pre PRE: --
             *@post POST: true==checkStr(s)-> s is a valid std::string rep. for BziBool
             */
            {
                std::cout << "BziBool::checkStr()->" << strValue <<std::endl;
                int pos;
                QRegExp rx("^(TRUE|FALSE|true|false|0|1)$");
                QString q = QString::fromStdString(strValue );
                pos= rx.indexIn(q);
                if (pos==-1) return false;
                else return true;
            }

            void setValueFromStr(const std::string &strValue)
            {
                if(strValue=="true")
                    _mVal = true;
                else if(strValue=="false")
                    _mVal = false;
                else
                    std::cerr << "Error: BziBool::setValueFromStr() init std::string not valid" << std::endl;
            }
    };
}
#endif //BASIC_DATA_TYPES_
