/*
* File: socketException.h	
* Description: exception produced by a socket error
*			   
* Date: March 11st 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
*		  BitBrain Technologies
*/

#pragma once

#ifndef SOCKET_EXCEPTION_H
#define SOCKET_EXCEPTION_H

#include "networkException.h"
#include <string>

namespace Bzi
{
    class SocketException : public NetworkException
	{
		public:

            SocketException(const std::string& message)
				:NetworkException(message)
			{}

            std::ostream& debugBzi(std::ostream& os) const
			{ 
				return os<<_sName
						 <<":\n\t"
						 <<_mMessage
                         <<std::endl;
			}

		private:
            static const std::string _sName;
		
	};
}

#endif //SOCKET_EXCEPTION_H
