#pragma once

#ifndef DATA_BUFFER_H
#define DATA_BUFFER_H

#include "core/data/dataStream.h"

namespace Bzi
{
    class LIBRARY_API DataBuffer
    {
        public:

            typedef std::vector<qint64> TimestampVector;

            DataBuffer(bool isCircular = false);
            DataBuffer(int length, const DataProperties& p, bool isCircular = false);
            DataBuffer(int length, int rows, int cols, bool isCircular = false);
            ~DataBuffer() {_finalize();}

            DataBuffer& operator=(const DataBuffer& buffer);

            virtual void setProperties(int length, const DataProperties& p, bool isCircular = false);
            DataProperties properties() const;

            void add(DataBlock *dataBlock);
            void add(DataStream *dataStream);
            void clear();

            // values
            void setValue(int row, int col, DataBlock::ValueType value);
            DataBlock::ValueType value(int row, int col) const;
            DataBlock::ValueType* values();
            int length() const
            { return _mpBuffer->cols(); }
            DataBlock::ValueType* ch(int ch)
            { return _mpBuffer->ch(ch); }

            // raw accessor, use only if you know what you're doing
            DataBlock* raw() const;

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

        protected:
            //attributes
            DataProperties*     _properties;
            DataBlock*          _mpBuffer;
            int                 _mCurrentIndex;
            bool				_mCircular;

            //methods
            void _finalize();
    };

    template <class DATA, class PROP_CONTAINER>
    class DataBufferContainerT
    {
        public:
            DataBufferContainerT() {}
            DataBufferContainerT(std::vector<int> length, const PROP_CONTAINER& p)
            {
                setProperties(length, p);
            }

            ~DataBufferContainerT()
            {
                deleteSignals();
            }

            DataBufferContainerT& operator=(const DataBufferContainerT& container)
            {
                if (&container != this) {
                    _properties = container._properties;
                    for (int i = 0; i < container.size(); i++)
                    {
                        DATA* buffer = container.get(i);
                        DATA* bufferCopy = new DATA;
                        *bufferCopy = *buffer;
                        _buffers.push_back(bufferCopy);
                        _map.insert(make_pair(_properties.getId(i), bufferCopy));
                    }
                }
                return *this;
            }

            void setProperties(std::vector<int> length, const PROP_CONTAINER& p)
            {
                _map.clear();
                _properties = p;
                for ( int i = 0; i < p.size(); i++ )
                {
                    DATA *buffer = new DATA(length[i], p.getProp(i));
                    _buffers.push_back(buffer);
                    _map.insert(make_pair(_properties.getId(i), buffer));
                }
            }

            void add(DATA* buffer)
            {
                _buffers.push_back(buffer);
            }

            DATA* get(int index) const
            {
                return _buffers[index];
            }

            DATA* get(const std::string& id) const
            {
                typename std::map<std::string,DATA*>::const_iterator it = _map.find(id);
                if (it==_map.end())
                    return NULL;
                else
                    return it->second;
            }

            const std::string& getId(int index) const
            {
                return _properties.getId(index);
            }

            size_t size() const
            {
                return _buffers.size();
            }

            void clear()
            {
                _buffers.clear();
                _map.clear();
            }

            void deleteSignals()
            {
                for ( int i = 0; i < _buffers.size(); i++ )
                    delete _buffers[i];
                _buffers.clear();
                _map.clear();
            }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const
            {
                for (size_t i=0; i < _buffers.size(); i++)
                {
                    os << "id: " << _properties.getId(i) << std::endl;
                    os << "length: " << _buffers[i]->length() << std::endl;
                }
                return os;
            }

        private:
            std::vector<DATA*>              _buffers;
            std::map<std::string, DATA*>    _map;
            PROP_CONTAINER                  _properties;
    };

    class LIBRARY_API DataBufferContainer: public DataBufferContainerT<DataBuffer, DataPropertiesContainer>
    {

    };
}

#endif //DATA_BUFFER_H
