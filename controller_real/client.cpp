#include <QApplication>
#include "testclient.h"

int main(int argc, char **argv)
{
    // Qt app
    QApplication app( argc, argv );


    std::cout << "Instantiate client" << std::endl;
    TestClient* client = new TestClient();
    client->show();
    std::cout << "Client instantiated" << std::endl;

    return app.exec();
}
