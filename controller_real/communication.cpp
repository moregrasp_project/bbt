#include "communication.h"

#include "sockets/streamSocket.h"

#include <sstream>
#include <iostream>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>



Communication::Communication(Bzi::StreamSocket * const connection, bool log):
    _requests(connection), _receiver(connection), _log(log)
{
    std::cout << "[SERVER] Tablet connected" << std::endl;
    _receiver.start();
}

Communication::~Communication()
{
    _receiver.stop();
    _receiver.quit();
    _receiver.wait();
}

void Communication::onDisconnect()
{
    _log_file.close();
}

void Communication::enableLog(bool enable)
{
    _log = enable;
}

void Communication::processRequest(const QJsonObject& object)
{    
    QJsonDocument doc(object);
    _log_file << "RECEIVE: " << doc.toJson(QJsonDocument::Compact).toStdString() << std::endl;

    for(QJsonObject::const_iterator entry = object.constBegin(); entry != object.constEnd(); ++entry)
    {
        if (entry.key().toLower() == "load") //LOAD
        {
            if (entry.value().isString())
            {
                QString operation_mode = entry.value().toString();
                QString filename = operation_mode + ".conf";
                std::cout << "[SERVER] Request: Load " << filename.toStdString() << std::endl;
                _operation_mode = operation_mode;
                onLoadCommand(filename);
            }
            else
                replyError("The LOAD value must be a string", "load");
        }
        else if (entry.key().toLower() == "config") //CONFIG
        {
            std::cout << "[SERVER] Request: Config " << std::endl;
            if (entry.value().isArray())
            {
                QJsonArray array = entry.value().toArray();

                onConfigCommand(array);
            }
            else
                replyError("The CONFIG value must be a list", "load");
        }
        else if (entry.key().toLower() == "command")
        {
            if (entry.value().isString())
            {
                QString command = entry.value().toString().toLower();
                if (command == "config request")
                {
                    std::cout << "[SERVER] Request: COMMAND config request " << std::endl;
                    onConfigRequest();
                }
                else if (command == "start")
                {
                    std::cout << "[SERVER] Request: COMMAND start " << std::endl;
                    onStartCommand();
                }
                else if (command == "stop")
                {
                    std::cout << "[SERVER] Request: COMMAND stop " << std::endl;
                    onStopCommand();
                }
                else
                    replyError("The COMMAND value " + command + " is not a valid command", "command");
            }
            else
                replyError("The COMMAND value must be a string", "command");
        }
        else if (entry.key().toLower() == "data")
        {
            if (entry.value().isArray())
            {
                QJsonArray array = entry.value().toArray();
                onEventCommand(array);
            }            
        }
        else
        {
            replyError("Unknown entry key " + entry.key(), "key");
        }
    }
}

void Communication::sendDataStream(const QJsonArray& data)
{
    QJsonObject object;
    object.insert("DATA", data);
    reply(object);
}

void Communication::replyConfig(const QJsonObject& config)
{
    QJsonObject object = config;
    //object.insert("STATUS", QJsonValue("OK"));
    reply(object);    
}

void Communication::replyOk(const QString &operation)
{
    QJsonObject object;
    object.insert("STATUS", QJsonValue("OK"));
    object.insert("OPERATION", QJsonValue(operation));
    object.insert("MODE", QJsonValue(_operation_mode));
    reply(object);    
}

void Communication::replyError(const QString& error_msg, const QString &operation)
{
    QJsonObject object;
    object.insert("STATUS", QJsonValue("Error"));
    object.insert("MSG", QJsonValue(error_msg));
    object.insert("OPERATION", QJsonValue(operation));
    object.insert("MODE", QJsonValue(_operation_mode));
    reply(object);    
}

void Communication::reply(const QJsonObject &object)
{
    QJsonDocument doc(object);
    QByteArray msg = doc.toJson(QJsonDocument::Compact);

    _log_file << "SEND: " << msg.toStdString() << std::endl;

    _requests->Write(msg.data(), msg.size());    
}


