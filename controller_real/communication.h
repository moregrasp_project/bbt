#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "receiver.h"

#include <fstream>

#include <QJsonObject>
class QJsonArray;
class QString;

namespace Bzi {
    class StreamSocket;
}

class Communication
{
    public:
        Communication(Bzi::StreamSocket* const connection, bool log=false);
        ~Communication();

        void receiveRequests();

        void enableLog(bool enable=true);

    protected:
        void replyOk(const QString& operation);
        void replyConfig(const QJsonObject &config);
        void replyError(const QString& error_msg, const QString& operation);
        void sendDataStream(const QJsonArray &data);

        virtual void onLoadCommand(const QString& operation_mode){replyOk("LOAD");}
        virtual void onStartCommand() {replyOk("Start");}
        virtual void onStopCommand() {replyOk("Stop");}
        virtual void onConfigCommand(const QJsonArray& config) {replyOk("config");}
        virtual void onConfigRequest() {replyConfig(QJsonObject());}
        virtual void onEventCommand(const QJsonArray& events) {}
        virtual void onDisconnect();

    protected:
        Bzi::StreamSocket* const _requests;
        Receiver _receiver;

        QString _operation_mode;

        void processRequest(const QJsonObject& object);
        void reply(const QJsonObject &object);

        std::ofstream _log_file;
        bool _log;
};

#endif // COMMUNICATION_H
