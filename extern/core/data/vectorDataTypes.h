/*
* File: vectorDataTypes.h	
* Description: std vector specializations with different bzi Data (basic or not)
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef VECTOR_DATA_TYPES_H
#define VECTOR_DATA_TYPES_H

#include "core/data/basicDataTypes.h"
#include "vectorType.h"
#include "core/utils/libHeader.h"

#include <QRegExp>
#include <QStringList>


namespace Bzi
{
    class LIBRARY_API BziIntVector: public VectorType<BziInt>
    {
        public:

            BziIntVector(){}

            std::vector<int> value();
            void value(const std::vector<int> &vec);
            void setValueFromStr(const std::string &strValue);
            static std::vector<int> convertFromStr(const std::string &strValue);
            //redefined
            Data* clone();
            virtual bool checkStr(const std::string &strValue);
    };

    class LIBRARY_API BziBoolVector: public VectorType<BziBool>
    {
        public:

            BziBoolVector(){}

            std::vector<bool> value();
            void value(const std::vector<bool> &vec);
            void setValueFromStr(const std::string &strValue);
            //redefined
            Data* clone();
            virtual bool checkStr(const std::string &strValue);
    };

    class LIBRARY_API BziDoubleVector: public VectorType<BziDouble>
    {
        public:

            BziDoubleVector(){}

            std::vector<double> value();
            void value(const std::vector<double> &vec);
            void setValueFromStr (const std::string &strValue);
            //redefined
            Data* clone();
            virtual bool checkStr(const std::string &strValue);
    };

    class LIBRARY_API BziStringVector: public VectorType<BziString>
    {
        public:

            BziStringVector(){}

            std::vector<std::string> value();
            void value(const std::vector<std::string> &vec);
            void setValueFromStr (const std::string &strValue);

            static std::vector<std::string> convertFromStr(const std::string &strValue);
            //redefined
            Data* clone();
            virtual bool checkStr(const std::string &strValue);
    };
}

#endif //VECTOR_DATA_TYPES_H

