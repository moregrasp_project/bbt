#include "moregraspGui.h"
#include <iostream>
#include <sstream>

using namespace std;


MoregraspGui::MoregraspGui(QString name)
    : _configured(false)
{
    // Title
    setWindowTitle(name);

    // Config Groupbox
    _pLoadLine = new QLineEdit( this );
    _pLoadFormLayout = new QFormLayout();
    _pLoadFormLayout->addRow( tr("&file:"), _pLoadLine);
    _pLoadButton = new QPushButton( this );
    _pLoadButton->setText( "load" );
    _pLoadButton->setEnabled( true );
    _pLoadButton->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );

    _pConfigLayout = new QGridLayout();
    _pConfigLayout->addLayout( _pLoadFormLayout, 0, 0 );
    _pConfigLayout->addWidget( _pLoadButton, 0, 1 );
    _pConfigBox = new QGroupBox( "1) Load operation mode (.conf)" );
    _pConfigBox->setLayout( _pConfigLayout );

    // Sub-config Combobox
    _pComboBox = new QComboBox( this );
    _pComboBox->setEnabled( false );
    _pLaunchButton = new QPushButton( this );
    _pLaunchButton->setText( "start" );
    _pLaunchButton->setEnabled( false );
    _pLaunchButton->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
    _pConfigText = new QPlainTextEdit( this );
    _pConfigText->setToolTip("Optionally enter parameters in the form: unitName#paramName#paramValue");
    QFontMetrics m1(_pConfigText->font()) ;
    int rowHeight = m1.lineSpacing() ;
    _pConfigText->setFixedHeight(5 * rowHeight);

    _pSubconfigLayout = new QGridLayout();
    _pSubconfigLayout->addWidget( _pConfigText, 0, 0, 1, 2 );
    _pSubconfigLayout->addWidget( _pComboBox, 1, 0 );
    _pSubconfigLayout->addWidget( _pLaunchButton, 1, 1 );
    _pSubconfigBox = new QGroupBox( "2) Launch system" );
    _pSubconfigBox->setLayout( _pSubconfigLayout );

    // During operation
    _pStopButton	= new QPushButton( this );
    _pStopButton->setText( "stop" );
    _pStopButton->setEnabled( false );
    _pStopButton->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Expanding );
    _pSendDataButton = new QPushButton( this );
    _pSendDataButton->setText( "send event" );
    _pSendDataButton->setEnabled( false );
    _pSendDataButton->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Expanding );
    _pOperationText = new QPlainTextEdit( this );
    _pOperationText->setToolTip("Enter event in the form: eventName#eventValue");
    QFontMetrics m2(_pOperationText->font()) ;
    rowHeight = m2.lineSpacing() ;
    _pOperationText->setFixedHeight(3 * rowHeight);

    _pOperationBox = new QGroupBox( "3) Online operation" );
    _pOperationLayout = new QGridLayout();
    _pOperationLayout->addWidget(_pStopButton, 0, 0, 1, 3);
    _pOperationLayout->addWidget(_pOperationText, 1, 0, 1, 3);
    _pOperationLayout->addWidget(_pSendDataButton, 2, 0, 1, 3);
    _pOperationBox->setLayout(_pOperationLayout);

    // StatusLine
    _pStatusLog = new QPlainTextEdit( this );
    QFontMetrics m3(_pStatusLog->font()) ;
    rowHeight = m3.lineSpacing() ;
    _pStatusLog->setFixedHeight(7 * rowHeight);
    _pStatusLog->setEnabled( false );
    _pStatusLog->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );

    // Signal-slot connections
    connect( _pLoadButton, SIGNAL(clicked(bool)), this, SLOT(onLoadButton()) );
    connect( _pLaunchButton, SIGNAL(clicked(bool)), this, SLOT(onLaunchButton()) );
    connect( _pStopButton, SIGNAL(clicked(bool)), this, SLOT(onStopButton()) );
    connect( _pSendDataButton, SIGNAL(clicked(bool)), this, SLOT(onSendDataButton()) );

    // Layout
    _pGridLayout = new QGridLayout( this );
    _pGridLayout->addWidget( _pConfigBox, 0, 0, 1, 3 );
    _pGridLayout->addWidget( _pSubconfigBox, 1, 0, 1, 3 );
    _pGridLayout->addWidget( _pOperationBox, 2, 0, 1, 3 );
    _pGridLayout->addWidget( _pStatusLog, 3, 0, 1, 3 );
    setLayout( _pGridLayout );
}


// PUBLIC SLOTS
void MoregraspGui::onLoadedConfig(QStringList names)
{
    if (names.size() > 0) {
        // fill subconfig
        _pComboBox->clear();
        for (size_t i = 0; i < names.size(); i++) {
            _pComboBox->addItem(names[i]);
        }
        // logic
        _pLaunchButton->setEnabled(true);
        _pComboBox->setEnabled(true);
    } else {
        // logic
        _pLaunchButton->setEnabled(false);
        _pComboBox->setEnabled(false);
    }
}

void MoregraspGui::onLaunched(bool ok)
{
    _configured = ok;
    if (ok) {
        // logic
        _pLoadButton->setEnabled( false );
        _pLaunchButton->setEnabled( false );
        _pComboBox->setEnabled( false );
        _pStatusLog->appendPlainText("Configuration: " + _subconfig);
        onStartButton();
    }
}

void MoregraspGui::onReseted(bool ok)
{
    _pLoadButton->setEnabled( true );
    _pLaunchButton->setEnabled( true );
    _pComboBox->setEnabled( true );
    _pStopButton->setEnabled( false );
    _pSendDataButton->setEnabled( false );
    _pStatusLog->appendPlainText("Reseted");
}

void MoregraspGui::onStatus(QString text)
{
    _pStatusLog->appendPlainText(text);
}

// PRIVATE SLOTS
void MoregraspGui::onLoadButton()
{
    _configFilename = QFileDialog::getOpenFileName(this, tr("Open file"), tr("config"), tr("Configuration (*.conf)"));
    if ( !_configFilename.isEmpty() ) {
        _pLoadLine->setText(_configFilename);
        emit sigLoadConfig(_configFilename);
    }
}


void MoregraspGui::onLaunchButton()
{
    _subconfig = _pComboBox->currentText();
    emit sigLaunch(_subconfig, _pConfigText->toPlainText());
}

void MoregraspGui::onStartButton()
{
    // logic
    _pLoadButton->setEnabled( false );
    _pLaunchButton->setEnabled( false );
    _pComboBox->setEnabled( false );
    _pStopButton->setEnabled( true );
    _pSendDataButton->setEnabled( true );
    emit sigStart();
}

void MoregraspGui::onStopButton()
{
    // logic
    _pLoadButton->setEnabled( false );
    _pLaunchButton->setEnabled( false );
    _pComboBox->setEnabled( false );
    _pStopButton->setEnabled( false );
    _pSendDataButton->setEnabled( false );
    emit sigStop();
}

void MoregraspGui::onResetButton()
{
    _pLoadButton->setEnabled( true );
    _pLaunchButton->setEnabled( true );
    _pComboBox->setEnabled( true );
    _pStopButton->setEnabled( false );
    _pSendDataButton->setEnabled( false );
    emit sigReset();
}

void MoregraspGui::onSendDataButton()
{
    QString data = _pOperationText->toPlainText();
    emit sigSendData(data);
}

