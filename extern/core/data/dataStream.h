/*
* File: dataStream.h
* Description: class defining a data stream.
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef DATA_STREAM_H
#define DATA_STREAM_H

#include "core/data/data.h"
#include "core/data/dataProperties.h"
#include <QtGlobal>
#include <vector>


namespace Bzi
{
    class LIBRARY_API DataBlock: public Data
    {
            friend class DataStream;

        public:
            typedef double ValueType;

            DataBlock();
            DataBlock(const DataProperties& p);
            DataBlock(int rows, int cols);
            DataBlock(const DataBlock& data); // copy constructor used for clonning
            void setProperties(const DataProperties& p);
            virtual ~DataBlock();

            virtual DataBlock& operator=(const DataBlock& data);

            //cloning interface
            virtual Data* clone();

            //values
            void setValue(int row, int col, ValueType value);
            void setValues(ValueType* values, bool transposed = false);
            ValueType value(int row, int col) const;
            ValueType* values();
            ValueType* ch(int ch);
            void  setDirty();
            std::string str() const;
            virtual void setValueFromStr(const std::string &strValue);

            //accesors
            std::string name() const
            {return _mId; }
            int	rows() const
            { return _properties->rows(); }
            int	cols() const
            { return _properties->cols(); }
            DataProperties* prop() const
            { return _properties; }

            // subset of the matrix
            DataBlock subset(const std::vector<int>& chList);
            void subset(const std::vector<int>& chList, DataBlock& block);

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            virtual void marshall(QDataStream &qDataStream);
            virtual void unmarshall(QDataStream &qDataStream);

            //file interface
            virtual void toFileStream(QDataStream &qDataStream, const std::string& version);
            virtual void fromFileStream(QDataStream &qDataStream, const std::string& version);

            // armadillo interface
#ifdef COMPILE_ARMADILLO
            virtual void toArmadilloMat(arma::mat& armaMat, bool copyMemory=false);
            virtual void fromArmadilloMat(const arma::mat& armaMat);
#endif
        protected:
            void _assign(const DataBlock &data);
            void _resize(const DataProperties &p);
            DataProperties*         _properties;

        private:
            std::vector<ValueType>	_values;
            // transposed signal
            std::vector<ValueType>	_trans_values;
            bool					_trans_updated;
    };


    class LIBRARY_API DataStream: public Data
    {
        public:
            DataStream();
            DataStream(const DataProperties& p, int blocks);
            DataStream(int rows, int cols, int blocks);
            DataStream(const DataStream& stream); // copy constructor used for clonning
            virtual ~DataStream();

            virtual DataStream& operator=(const DataStream& stream);

            //cloning interface
            virtual Data* clone();

            //buffering pourposes in ASYNC modules
            void add(DataStream* stream);

            //values
            DataBlock* block(int block);
            DataBlock* block_n();
            int size() const {return _numBlocks;}
            void resize(int n);
            virtual void setValueFromStr(const std::string &strValue);

            //accesors
            std::string name() const
            {return _mId; }
            int	rows() const
            { return _properties->rows(); }
            int	cols() const
            { return _properties->cols(); }
            const DataProperties& prop() const
            { return *_properties; }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //file interface
            void toFileStream(QDataStream &qDataStream, const std::string& version);
            void fromFileStream(QDataStream &qDataStream, const std::string& version);

            //configuration
            virtual void initializeData(const InitParameterMap &initParams);

            //matlab conversion interface
#ifdef COMPILE_MATLAB
            void toMatlabMatrix(mxArray*& pMatlabArray);
            void fromMatlabMatrix(mxArray* pMatlabArray);
#endif
        protected:
#ifdef COMPILE_MATLAB
            mxArray* _putDataMatrix();
            void _getDataMatrix(mxArray *pMatlabArray);
            mxArray* _putAttrMatrix(const std::vector<double>& attr);
            void _getAttrMatrix(mxArray *pAttrMat, std::vector<double>& attr);
#endif
            DataProperties* _properties;

            virtual void _resize(int blocks);
            void _assign(const DataStream &stream);

            std::vector<DataBlock*>	_blocks;
            int                     _numBlocks;
    };


    template <class DATA, class PROP_CONTAINER>
    class DataStreamContainerT
    {
        public:
            DataStreamContainerT(){}
            DataStreamContainerT(const PROP_CONTAINER& p)
            { setProperties(p); }
            ~DataStreamContainerT() {}

            void setProperties(const PROP_CONTAINER& p)
            {
                _properties = p;
                for ( int i = 0; i < p.size(); i++ )
                {
                    DATA *data = new DATA(p.getProp(i), 1);
                    _signals.push_back(data);
                }
            }

            void add(DATA* signal)
            {
                _signals.push_back(signal);
            }

            DATA* get(int index) const
            {
                return _signals[ index ];
            }

            const std::string& getId(int index) const
            {
                return _properties.getId(index);
            }

            size_t size() const
            {
                return _signals.size();
            }

            void clear()
            {
                _signals.clear();
            }

            void deleteSignals()
            {
                for ( int i = 0; i < _signals.size(); i++ )
                    delete _signals[i];
                _signals.clear();
            }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const
            {
                for(size_t i=0; i < _signals.size(); i++)
                    os << _signals[i] << std::endl;
                return os;
            }

            //used to read/write binary files in Carlos format
            void toFileStream(QDataStream &qDataStream, const std::string& version)
            {
                for (size_t i = 0; i < _signals.size(); i++)
                    _signals[i]->toFileStream(qDataStream, version);
            }

            void fromFileStream(QDataStream &qDataStream, const std::string& version)
            {
                for (size_t i = 0; i < _signals.size(); i++)
                    _signals[i]->fromFileStream(qDataStream, version);
            }

        private:
            std::vector<DATA*>  _signals;
            PROP_CONTAINER      _properties;
    };
}

#endif //DATA_STREAM_H
