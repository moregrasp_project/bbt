# Main project declaration
PROJECT(bbt)
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.11)
cmake_policy(SET CMP0020 NEW)

### MOREGRASP UNITS ###################################
set(BBT_UNITS
	partnerProcUnit
	partnerProtUnit
	eegPreprocUnit
	offlineUnit
)
#######################################################

SET(BUILD_CONTROLLER "FALSE" CACHE BOOL "Build Controller")

SET(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR})
SET(SOURCE_DIR "${ROOT_DIR}/units/src")
SET(LIBRARY_INC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/extern")
SET(LIBRARY_LIB_DIR "${CMAKE_CURRENT_SOURCE_DIR}/lib")
SET(EXECUTABLE_OUTPUT_PATH "${CMAKE_CURRENT_LIST_DIR}/bin")
SET(LIBRARY_OUTPUT_PATH "${CMAKE_CURRENT_SOURCE_DIR}/lib/units")

LINK_DIRECTORIES("${CMAKE_CURRENT_SOURCE_DIR}/lib")
INCLUDE_DIRECTORIES("${CMAKE_CURRENT_SOURCE_DIR}/extern")

SET(BZI_LIB_NAME bzicore)

# Basic Qt package
find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Gui REQUIRED)
include_directories(${Qt5Core_INCLUDE_DIRS})
IF(WIN32)
    include_directories(${Qt5Gui_INCLUDE_DIRS})
ELSE(WIN32)
    include_directories(${Qt5Gui_INCLUDE_DIRS})
    include_directories(${Qt5Gui_PRIVATE_INCLUDE_DIRS})
ENDIF(WIN32)
include_directories(${Qt5Widgets_INCLUDE_DIRS})
add_definitions(${Qt5Core_DEFINITIONS})
add_definitions(${Qt5Gui_DEFINITIONS})
add_definitions(${Qt5Widgets_DEFINITIONS})
SET(LINK_LIBRARIES ${Qt5Widgets_LIBRARIES})
SET(LINK_LIBRARIES ${Qt5Gui_LIBRARIES})

SET(CMAKE_INCLUDE_CURRENT_DIR ON)
SET(CMAKE_AUTOMOC ON)

    IF(${CMAKE_SYSTEM_VERSION} GREATER 6.1 ) # Windows 10
        MESSAGE(STATUS "UTC_T")
        ADD_DEFINITIONS(-DUTC_T)
    ENDIF()

IF(BUILD_CONTROLLER)
    ADD_SUBDIRECTORY(controller)
    ADD_SUBDIRECTORY(controller_real)
ENDIF(BUILD_CONTROLLER)
ADD_SUBDIRECTORY(units)
