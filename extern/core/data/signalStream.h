/*
* File: signalStream.h
* Description: class defining a signal stream.
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef SIGNAL_STREAM_H
#define SIGNAL_STREAM_H

#include "core/data/dataStream.h"
#include "core/data/signalProperties.h"

namespace Bzi
{
    class LIBRARY_API SignalBlock: public DataBlock
    {
        public:
            SignalBlock();
            SignalBlock(const SignalProperties& p);
            //SignalBlock(int rows, int cols, int fields, int samplingRate);
            SignalBlock(const SignalBlock& data); // copy constructor used for clonning
            virtual ~SignalBlock() {}

            DataBlock& operator=(const DataBlock& data);
            SignalBlock& operator=(const SignalBlock& data);

            //cloning interface
            Data *clone();

            //attributes
            void        setTimestamp(qint64 stamp) {_timestamp = stamp;}
            qint64      timestamp() const {return _timestamp;}
            void        setField(int n, ValueType value) {_field.setValue(n, 0, value);}
            ValueType   field(int n) {return _field.value(n, 0);}
            DataBlock*  field() {return &_field;}

            //accesors
            int samplingRate() const
            { return static_cast<SignalProperties*>(_properties)->samplingRate(); }
            int fields() const
            { return static_cast<SignalProperties*>(_properties)->fields(); }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //file interface
            void toFileStream(QDataStream &qDataStream, const std::string& version);
            void fromFileStream(QDataStream &qDataStream, const std::string& version);
        private:
            void _assign(const SignalBlock &data);

        private:
            qint64      _timestamp;
            DataBlock   _field;
    };

    class LIBRARY_API SignalStream: public DataStream
    {
        public:
            SignalStream();
            SignalStream(const SignalProperties& p, int blocks);
            //SignalStream(int rows, int cols, int fields, int samplingRate, int blocks);
            SignalStream(const SignalStream& signal); // copy constructor used for clonning
            virtual ~SignalStream() {}

            DataStream& operator=(const DataStream& stream);
            SignalStream& operator=(const SignalStream& signal);

            //cloning interface
            Data* clone();

            //values
            SignalBlock* block(int block);

            //accesors
            int	samplingRate() const
            { return static_cast<SignalProperties*>(_properties)->samplingRate(); }
            int fields() const
            { return static_cast<SignalProperties*>(_properties)->fields(); }
            const SignalProperties& prop() const
            { return *static_cast<SignalProperties*>(_properties); }

            // debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //configuration
            void initializeData(const InitParameterMap &initParams);

            //matlab conversion interface
#ifdef COMPILE_MATLAB
            void toMatlabMatrix(mxArray*& pMatlabArray);
            void fromMatlabMatrix(mxArray* pMatlabArray);

            mxArray* _putFieldMatrix();
            void _getFieldMatrix(mxArray *pMatlabArray);
#endif
        private:
            void _resize(int blocks);
    };

    class LIBRARY_API SignalStreamContainer: public DataStreamContainerT<SignalStream, SignalPropertiesContainer>
    {

    };
}

#endif //SIGNAL_STREAM_H
