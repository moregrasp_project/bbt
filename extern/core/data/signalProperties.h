/*
* File: signalProperties.h
* Description: class defining the properties of a signal stream.
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef SIGNAL_PROPERTIES_H
#define SIGNAL_PROPERTIES_H

#include "core/data/dataProperties.h"

namespace Bzi
{
    class LIBRARY_API SignalProperties: public DataProperties
    {
        public:
            SignalProperties() {}
            SignalProperties(int rows, int cols, int fields, int samplingRate);
            SignalProperties(int rows, int cols, const std::string& subjectName, const std::string& deviceName, const std::string& deviceType, int fields, int samplingRate);
            SignalProperties(const SignalProperties &p);
            SignalProperties& operator=(const SignalProperties& p);
            virtual ~SignalProperties() {}

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //cloning interface
            Data *clone();

            //accesors
            int samplingRate() const
            { return _samplingRate; }
            int fields() const
            { return _fields; }
            std::string subjectName() const
            { return _subjectName; }
            std::string deviceName() const
            { return _deviceName; }
            std::string deviceType() const
            { return _deviceType; }
        private:
            void    _assign(const SignalProperties &sP);

        private:
            int             _samplingRate,
                            _fields;
            std::string     _subjectName,
                            _deviceName,
                            _deviceType;
    };

    class LIBRARY_API SignalPropertiesContainer: public DataPropertiesContainerT<SignalProperties>
    {
    };
}

#endif //SIGNAL_PROPERTIES_H
