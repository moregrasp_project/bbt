#include "jsonstreamreader.h"
#include "sockets/streamSocket.h"
#include <QJsonDocument>
#include <stdexcept>

JsonStreamReader::JsonStreamReader(Bzi::StreamSocket * const socket, unsigned int timeout):
    _socket(socket)
{
    if (timeout > 0)
        _socket->setRecTimeout(timeout);
}

JsonStreamReader::~JsonStreamReader()
{

}

QJsonObject JsonStreamReader::readMessage()
{
    QJsonObject output;

    while (true)
    {
        QByteArray localBuffer(1024, Qt::Uninitialized);        
        int ret = _socket->Read(localBuffer.data(), localBuffer.size());
        if (ret == -2) //timeout
            throw Timeout();

        if (ret == -1) //connection error
            throw Disconnected();


        if (ret > 0 and ret <= localBuffer.size())
        {            
            _buffer += localBuffer.left(ret);
            if (parse(output))                            
                return output;            
        }
    }
}

bool JsonStreamReader::parse(QJsonObject& output)
{
    //std::cout << "buffer: " << _buffer.toStdString() << std::endl;
    int level = 0;
    int start = 0;
    for (size_t i = 0; i < _buffer.size(); ++i)
    {
        if (_buffer[i] == '{')
        {
            if (level == 0)
                start = i;
            level++;
        }
        else if (_buffer[i] == '}')
        {
            level--;
            if (level == 0)
            {
                QJsonDocument doc = QJsonDocument::fromJson(_buffer.mid(start, i+1));
                //std::cout << "bytes (" << i+1 << "): " << doc.toJson().toStdString() << std::endl;
                output = doc.object();
                _buffer.remove(0, start + i + 1);
                return true;
            }

            if (level < 0)
                throw std::runtime_error("Negative level");
        }
    }
    return false;
}

