/*
* File: MoregraspController.h
* Description: main application to load a configuration file
* Author: Carlos Escolano (carlos.escolano@bitbrain.es)
*		  BitBrain Technologies
* Date: 01/04/2015
*/
#ifndef OPERATOR_CONTROLLER_H
#define OPERATOR_CONTROLLER_H

// controller
#include "core/exceptions/exception.h"
#include "core/controller/controller.h"
#include "core/configuration/configurationMap.h"
#include "core/utils/hat.h"
#include "core/module/moduleLauncher/moduleLauncher.h"

class MoregraspController : public Bzi::Controller
{
        Q_OBJECT

    public:
        MoregraspController(const std::string& name);
        ~MoregraspController() {}

    private:
        // Controller
        Bzi::ConfigurationMap	_configsMap;
        bool                _configured;
        std::string         _config_id;
        Bzi::Hat            _clock;

        void _handleDataMessage(const Bzi::ReceptionIdentifier &reception);

    signals:
        void	sigLoadedConfig(QStringList names);
        void	sigLaunched(bool ok);
        void    sigReseted(bool ok);
        void    sigStatus(QString text);

    public slots:
        void	onLoadConfig(QString filename);
        void	onLaunch(QString name, QString params);
        void	onStart();
        void	onStop();
        void	onReset();
        void    onFinish();
        void    onSendData(QString data);
};

#endif // OPERATOR_CONTROLLER_H
