/*
* File: realTimeModule.h	
* Description: class defining real time modules (modules with network
*			   interfaces to comunicate by TCP/IP mechanisms and also locally under
*				manager synchronization)			   
* Date: March 11th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
          BitBrain Technologies
*/

#pragma once

#ifndef REAL_TIME_MODULE_H
#define REAL_TIME_MODULE_H

#include "genericModule.h"

namespace Bzi
{
    class UnitPipeline;

    class LIBRARY_API RealTimeModule : public GenericModule
    {
            Q_OBJECT

        public:

            enum DisplayType
            {
                SYNCHRONOUS_DISPLAY = 0, //false
                ASYNCHRONOUS_DISPLAY = 1, //true
                MAX_DISPLAY_TYPES
            };

            enum ConfigurationState
            {
                NOT_CONFIGURED,
                CONFIGURED
            };

            RealTimeModule(const std::string& name,
                           unsigned int tcpPort = DEFAULT_TCP_PORT,
                           DisplayType displayType = ASYNCHRONOUS_DISPLAY);
            ~RealTimeModule();

            std::ostream& debugBzi(std::ostream& os) const;

            const std::string name() const;
            GenericModule::ModuleState state();
            void setState(ModuleState state);
            RealTimeModule::ConfigurationState configurationState();
            void setConfigurationState(ConfigurationState state);
            virtual bool networkConnect(const std::string &host, unsigned int tcpPort);
            virtual void networkDisconnect(const std::string &host, unsigned int tcpPort);
            bool isConnected();

            //stop network loop
            //void finish();

            static const std::string SYNCHRONOUS_DISPLAY_STR;
            static const std::string ASYNCHRONOUS_DISPLAY_STR;

        public slots:
            //Entry into network loop
            void run();

        protected:

            //attributes
            UnitPipeline *_mpPipeLine;
            ConfigurationState _mConfigurationState;

            //methods
            virtual void _handleNetworkMessage(const ReceptionIdentifier &reception);
            virtual void _handleStartMessage();
            virtual void _handleStopMessage(const ReceptionIdentifier &reception);
            virtual void _handleFinishMessage(const ReceptionIdentifier &reception);
            virtual void _handleFinishSelectiveMessage(const ReceptionIdentifier &){} //nothing just manager redefine it
            virtual void _handleResetSelectiveMessage(const ReceptionIdentifier &){} //nothing just manager redefine it
            virtual void _handleConfigurationMessage(const ReceptionIdentifier &reception);
            virtual void _handleActivationMessage();
            virtual void _handleResetConfigurationMessage();
            virtual void _handleTickMessage(const ReceptionIdentifier &reception);
            virtual void _handleDataMessage(const ReceptionIdentifier &reception);

            void _configureRepository( ModuleConfiguration &moduleConfig, RepositoryInterface* pRepo);
            void _addToRepository( RepositoryInterface::InOut type, std::vector<Parameter> dataVector, RepositoryInterface* pRepo, const std::string& unitAlias = "" );

            virtual void _configurePipeline(const ModuleConfiguration &moduleConfig, Configuration &config);
        private:
            //methods
            void _initialize();
            void _finalize();
    };

}

#endif //REAL_TIME_MODULE_H
