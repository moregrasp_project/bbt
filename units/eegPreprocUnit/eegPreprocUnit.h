#ifndef EEG_PREPROC_H
#define EEG_PREPROC_H

#include "core/units/genericUnit.h"
#include "core/utils/math/mkfilterWrapper.h"
#include "libraries/bbtCap/bbtCap.h"

namespace Bzi
{
    namespace Units
    {
        class IIR
        {
            public:
                IIR(int chans, int SR);
                ~IIR();

                void filter(SignalStream* in, SignalStream* out);
            private:
                MkfilterWrapper*	_mpFilter,
                               *    _mpNotchFilter;
        };

        class BattEstimate
        {
            public:
                BattEstimate(int blockSize, int samplingRate, double sec_init, double sec);
                ~BattEstimate() {}

                bool take(SignalStream* stream, int& state);
            private:
                int level(double meanValue);

                std::vector<int> _threshold;
                int _blocks_init, _blocks;
                bool _init;
                int _counter, _sumValue;
                int _currentState;
        };

        class CuEstimate
        {
            public:
                CuEstimate(EventStream* cuBattery);
                ~CuEstimate() {}

                bool get();

            private:
                SYSTEM_POWER_STATUS stats;
                EventStream     *_cuBattery;
        };


        class EegPreprocUnit: public Bzi::GenericUnit
        {
            public:
                EegPreprocUnit();
                ~EegPreprocUnit();

                void initialize();
                void process();
                void reset();

            private:
                double _cycle_time;    // in ms
                unsigned long _current_cycle;

                //preproc
                IIR*            _iir;
                Impedance*      _imp;
                BattEstimate*   _bat;
                CuEstimate*     _cu;
                int             _cycles_missed;

                SignalStream    *_inputEEGStream,
                                *_outputEEGStream;

                EventStream     *_eegImpedance,
                                *_eegBattery,
                                *_eegConnection,
                                *_cuBattery;
        };
    }
} // end namespace

#endif //EEG_PREPROC_H
