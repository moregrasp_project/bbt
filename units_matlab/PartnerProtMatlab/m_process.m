%disp('Matlab process')

counter_cycles = counter_cycles+1;
if (counter_cycles == cycles2event)
	counter_cycles = 0;
	counter_events = counter_events+1;
	if (counter_events < 20)
		% GENERATE DUMMY EVENT
		EVENT_DUMMY.data = counter_events;
		EVENT_DUMMY.t = hat;
		OUTPUTS_MODIFIED.('EVENT_DUMMY') = 1;
		disp('New event!')
	else
		% GENERATE RESET EVENT TO STOP THE SYSTEM
		EVENT_RESET.data = 1;
		OUTPUTS_MODIFIED.('EVENT_RESET') = 1;
	end
end

	