#ifndef TESTCLIENT_H
#define TESTCLIENT_H

#include <QJsonObject>
#include <QTimer>
#include <QWidget>

#include "sockets/streamSocket.h"
#include "receiver.h"
#include "core/utils/hat.h"


class QByteArray;
class QGroupBox;
class QHBoxLayout;
class QLineEdit;
class QPlainTextEdit;
class QPushButton;
class QStatusBar;
class QVBoxLayout;


class TestClient : public QWidget
{
        Q_OBJECT
    public:
        explicit TestClient(QWidget *parent = 0);
        ~TestClient();

    public slots:
        void onConnect();
        void onRun();
        void onLoad();
        void onSendConfig();
        void onRequestConfig();
        void onSendEvent();
        void abort();
    private:
        Bzi::StreamSocket _server;

        Receiver* _receiverThread;

        //state
        bool _connected, _loaded, _running;
        bool _abort;

        //widgets

        //connect Box
        QLineEdit* _serverLine;
        QLineEdit* _portLine;
        QPushButton* _connectButton;
        QGroupBox* _connectBox;
        QHBoxLayout* _connectBoxLayout;

        //load Box
        QLineEdit* _operationModeLine;
        QPushButton* _loadButton;
        QGroupBox* _loadBox;
        QHBoxLayout* _loadBoxLayout;

        //config box
        QPlainTextEdit* _configTextEdit;
        QPushButton* _sendConfigButton;
        QPushButton* _requestConfigButton;
        QGroupBox* _configBox;
        QVBoxLayout* _configBoxLayout;

        //run box
        QPushButton* _runButton;
        QGroupBox* _runBox;
        QHBoxLayout* _runBoxLayout;

        //event box
        QPlainTextEdit* _eventTextEdit;
        QPushButton* _sendEventButton;
        QGroupBox* _eventBox;
        QVBoxLayout* _eventBoxLayout;

        QStatusBar* _statusBar;

        //main Layout
        QVBoxLayout* _mainLayout;        

        bool stop();
        bool start();

        std::ofstream _log_file;
    private slots:
        void onNewMessage(const QJsonObject& msg);
};

#endif // TESTCLIENT_H
