#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

#include <QObject>
#include <QQueue>
#include <QJsonObject>
#include <QMutex>
#include <QWaitCondition>
#include <stdexcept>

class MessageQueue: public QObject
{
        Q_OBJECT
    public:

        class Timeout: public std::runtime_error
        {
            public:
                Timeout(): std::runtime_error("Timeout"){}
        };

        MessageQueue(unsigned long timeout = ULONG_MAX);
        ~MessageQueue();
        bool empty();
        void enqueue(const QJsonObject& object);
        QJsonObject dequeue();

    signals:
        void enqueued(QJsonObject data);

    private:
        unsigned long _timeout;
        QQueue<QJsonObject> _queue;
        QMutex _mutex;
        QWaitCondition _not_empty_condition;
};

#endif // MESSAGEQUEUE_H
