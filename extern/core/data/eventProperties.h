/*
* File: eventProperties.h
* Description: class defining the properties of an event stream.
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef EVENT_PROPERTIES_H
#define EVENT_PROPERTIES_H

#include "core/data/dataStream.h"
#include "core/data/dataProperties.h"

namespace Bzi
{
    class LIBRARY_API EventProperties: public DataProperties
    {
        public:
            EventProperties() {}
            EventProperties(int rows, int cols, const std::string& subjectName, const std::string& deviceName, const std::string& deviceType, const std::vector<DataBlock::ValueType>& initValues);
            EventProperties(const EventProperties &p);
            EventProperties& operator=(const EventProperties& p);
            virtual ~EventProperties() {}

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //cloning interface
            Data *clone();

            //accesors
            std::vector<DataBlock::ValueType> initValues() const
            { return _initValues; }
            std::string subjectName() const
            { return _subjectName; }
            std::string deviceName() const
            { return _deviceName; }
            std::string deviceType() const
            { return _deviceType; }
        private:
            void    _assign(const EventProperties &sP);

        private:
            std::vector<DataBlock::ValueType> _initValues;
            std::string     _subjectName,
                            _deviceName,
                            _deviceType;
    };

    class LIBRARY_API EventPropertiesContainer: public DataPropertiesContainerT<EventProperties>
    {

    };
}

#endif //EVENT_PROPERTIES_H
