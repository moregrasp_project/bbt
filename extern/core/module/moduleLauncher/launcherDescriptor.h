/*
* File: launcherDescriptor.h
* Description: LauncherDescriptor class defining all properties needed
*			   to launch a module
* Date: May 23rd 2012 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#ifndef LAUNCHER_DESCRIPTOR_H
#define LAUNCHER_DESCRIPTOR_H

#include <QFile>
#include <QXmlStreamReader>
#include <QString>
#include <QMap>

#include "string.h"
#include "core/interfaces/debuggingInterface.h"
#include "core/configuration/configuration.h"
#include "core/module/moduleLaunchProperties.h"

namespace Bzi
{
	class LIBRARY_API LauncherDescriptor: public DebuggingInterface
	{

		public:
			
			LauncherDescriptor();
			LauncherDescriptor( Bzi::Configuration config,
                                const std::string &xmlConfigFile = std::string(),
                                const std::string &configNamesStr = std::string());
			~LauncherDescriptor();

            std::ostream& debugBzi(std::ostream &os) const;

			void setConfiguration(const Bzi::Configuration &config);
			void join(const LauncherDescriptor &launchDescr);

			QMap<QString,Bzi::RealTimeModuleLaunchProperties> realTimeModulesLaunchProp() const;

			Bzi::ManagerLaunchProperties managerLaunchProp()const;
			Bzi::ControllerLaunchProperties controllerLaunchProp() const;

            bool containsRealtimeModule(const std::string &moduleName) const;

		private:
			//attributes
            std::map<QString,Bzi::RealTimeModuleLaunchProperties> _mRealTimeModules;
			Bzi::ManagerLaunchProperties _mManager;
			Bzi::ControllerLaunchProperties _mController;

			//methods
			void _obtainRealTimeModulesLaunchProperties(const Bzi::Configuration &config);
			void _obtainManagerLaunchProperties(const Bzi::Configuration &config);
			void _obtainControllerLaunchProperties(const Bzi::Configuration &config,
                                                   const std::string &xmlConfigFile = "",
                                                   const std::string &configNamesStr = "");

			void _fillManagerConnections();

			void _finalize();
	};
}

#endif //MODULE_LAUNCHER_H

