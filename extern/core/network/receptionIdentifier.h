/*
* File: networkMessage.h	
* Description: class defining network messages
*			   that transit throw TCP/IP network
* Date: March 16th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#pragma once

#ifndef RECEPTION_IDENTIFIER_H
#define RECEPTION_IDENTIFIER_H

#include <string>
#include <vector>

namespace Bzi
{
    class ReceptionIdentifier
    {
        public:
            ReceptionIdentifier()
            { }

            int receptionCode;
            std::string senderId;
            std::vector<std::string> dataIds;
    };
}

#endif //RECEPTION_IDENTIFIER_H
