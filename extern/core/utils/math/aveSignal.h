#ifndef AVE_SIGNAL_H
#define AVE_SIGNAL_H

#include <iostream>
#include <vector>
#include <fstream>

#include "core/utils/libHeader.h"
#include "core/data/dataStream.h"

//#define AVE_SIGNAL_DEBUG

namespace Bzi
{
    class LIBRARY_API AveSignal
    {
        public:

            AveSignal(int channels, int bufferSize, int blockstoUpdate, bool absValue);
            ~AveSignal();

            bool process(DataBlock* signal, std::vector<float>& out);


        private:
            int					_channels;
            int					_bufferSize;
            int					_pointerWrite;
            std::vector<double>	_buffer;
            bool                _full;
            int					_newBlock;
            int					_blockstoUpdate;
            bool                _absValue;

            bool                _hasData();
            void                _compute(std::vector<float>& out);

            #ifdef AVE_SIGNAL_DEBUG
            std::ofstream       outStream;
            #endif

    };
}
#endif // AVE_SIGNAL
