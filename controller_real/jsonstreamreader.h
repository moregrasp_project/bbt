#ifndef JSONSTREAMREADER_H
#define JSONSTREAMREADER_H

#include <QByteArray>
#include <QJsonObject>
#include <stdexcept>

namespace Bzi {
class StreamSocket;
}

class JsonStreamReader
{
    public:
        JsonStreamReader(Bzi::StreamSocket* const socket, unsigned int timeout = 0);
        ~JsonStreamReader();

        QJsonObject readMessage();

        class Timeout : public std::runtime_error
        {
            public:
                Timeout():
                    std::runtime_error("Timeout")
                {}
        };

        class Disconnected : public std::runtime_error
        {
            public:
                Disconnected():
                    std::runtime_error("Disconnected")
                {}
        };

    private:
        Bzi::StreamSocket* const _socket;
        QByteArray _buffer;

        bool parse(QJsonObject& output);
};

#endif // JSONSTREAMREADER_H
