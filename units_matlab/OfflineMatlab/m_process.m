%disp('Matlab process')

if (first)
	disp('Doing the offline processing...');
	
	pause(5);
	
	% we are done...
	EVENT_RESET.data = 1;
	EVENT_RESET.t = hat;
	OUTPUTS_MODIFIED.('EVENT_RESET') = 1;
	
	first = false;
end
