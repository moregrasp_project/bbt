/*
* File: bbtLauncherGui.cpp
* Description: main application to load a configuration file
* Author: Carlos Escolano (carlos.escolano@bitbrain.es)
*		  BitBrain Technologies
* Date: 01/04/2015
*/

// operator gui
#include "moregraspController.h"

#include "sockets/serverSocket.h"
#include "sockets/streamSocket.h"

#include <QThread>

using namespace std;

int main(int argc, char* argv[])
{
    // Qt app
    QApplication app( argc, argv );

    int port = 5555;

    while (true)
    {
        Bzi::ServerSocket clients(5555);
        clients.Init();

        std::cout << "Service at port " << port << std::endl;
        std::cout << "Waiting for the tablet to connect ... " << std::endl;
        clients.Listen();

        Bzi::StreamSocket* connection = clients.Accept();
        std::cout << "New client " << connection->getRemoteHostName() << ":" << connection->getRemoteTcpPort() << std::endl;
        clients.Close();

        MoregraspController* controller = new MoregraspController("Controller", connection);

        QObject::connect(controller, SIGNAL(disconnected()), controller, SLOT(deleteLater()));

        std::cout << "Launch controller" << std::endl;
        controller->start();        
        std::cout << "Controller up and running" << std::endl;
        controller->wait();
        std::cout << "Controller finished" << std::endl;
        controller->deleteLater();
        std::cout << "Controller destroyed" << std::endl;
    }

    return app.exec();
}

