/*
* File: dataProperties.h
* Description: class defining the properties of a data stream.
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef DATA_PROPERTIES_H
#define DATA_PROPERTIES_H

#include "core/data/data.h"

namespace Bzi
{
    class LIBRARY_API DataProperties: public Data
    {
        public:
            DataProperties();
            DataProperties(int rows, int cols);
            DataProperties(const DataProperties &p);
            DataProperties& operator=(const DataProperties& p);
            virtual ~DataProperties() {}

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            virtual void marshall(QDataStream &qDataStream);
            virtual void unmarshall(QDataStream &qDataStream);

            //cloning interface
            virtual Data *clone();

            //accesors
            int rows() const
            { return _rows; }
            int cols() const
            { return _cols; }

        protected:
            void    _assign(const DataProperties &sP);

        private:
            int		_rows,
                    _cols;
    };

    template <class PROP>
    class DataPropertiesContainerT
    {
        public:
            DataPropertiesContainerT() {}
            ~DataPropertiesContainerT() {}

            void add(const std::string &id, const PROP& p)
            {
                _id.push_back(id);
                _prop.push_back(p);
            }

            const PROP& getProp(int index) const
            {
                return _prop[index];
            }

            const std::string& getId(int index) const
            {
                return _id[index];
            }

            int size() const
            {
                return _prop.size();
            }

            void clear()
            {
                _id.clear();
                _prop.clear();
            }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const
            {
                os <<  "-------------------" << std::endl;
                os <<  "DataPropertiesContainer" << std::endl;
                os <<  "> NumSignals: " << size() << std::endl;
                for ( int i=0; i<size(); i++ )
                {
                    os << "> Data " << i << " "<< getId(i) << std::endl;
                    os << "---" << getProp(i);
                }
                os << "-------------------" << std::endl;
                return os;
            }

            //marshalling interface
            void marshall(QDataStream &qDataStream)
            {
                int size = _prop.size();
                Bzi::marshall(size, qDataStream);
                for(size_t i=0; i < _prop.size(); i++)
                {
                    Bzi::marshall(_id[i],qDataStream);
                    Bzi::marshall(_prop[i],qDataStream);
                }
            }

            void unmarshall(QDataStream &qDataStream)
            {
                int size; Bzi::unmarshall(size,qDataStream);

                std::string id; PROP prop;

                _id.clear(); _prop.clear();
                for(int i=0; i <size; i++)
                {
                    Bzi::unmarshall(id,qDataStream);
                    _id.push_back(id);
                    Bzi::unmarshall(prop,qDataStream);
                    _prop.push_back(prop);
                }
            }

        private:
            std::vector<std::string>    _id;
            std::vector<PROP>           _prop;
    };

    class LIBRARY_API DataPropertiesContainer: public DataPropertiesContainerT<DataProperties>
    {

    };
}

#endif //DATA_PROPERTIES_H
