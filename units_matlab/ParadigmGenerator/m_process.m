% disp('Matlab process')

%% get run time
current_time = hat; % [�s]
if start_time == 0
    start_time = current_time;
end
run_time = (current_time - start_time) / 1e6; % [s]

if isempty(timeline)
    % no more events available, stop system
    EVENT_RESET.data = 1;
    EVENT_RESET.t = current_time;
    OUTPUTS_MODIFIED.('EVENT_RESET') = 1;
else
    % find events to yield
    events_to_yield_idcs = timeline <= run_time;
    events_to_yield = events(events_to_yield_idcs);
    events_to_yield(events_to_yield == end_code) = []; % don't ouput end code, just stop the system in the next script call
    n_events_to_yield = length(events_to_yield);
    
    % delete obsolete events
    events(events_to_yield_idcs) = [];
    timeline(events_to_yield_idcs) = [];
    
    if n_events_to_yield > 0
        assert(n_events_to_yield <= length(EVENT_PARADIGM.data));
        EVENT_PARADIGM.data(:) = 0;
        EVENT_PARADIGM.data(1:n_events_to_yield) = events_to_yield;
% 		events_to_yield
        EVENT_PARADIGM.t = current_time;
        OUTPUTS_MODIFIED.('EVENT_PARADIGM') = 1;
    end
end
