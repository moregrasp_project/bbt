/*! \class Socket
    \brief A multi-platform wrapper to Berkeley sockets.

	A multi-platform wrapper to Berkeley sockets.
	Only TCP is supported.
*/

#pragma once

#ifndef SOCKET_H
#define SOCKET_H

#include "socketDefs.h"
#include "socketException.h"
#include "socketTimeoutException.h"

#include <sstream>
#include <vector>

namespace Bzi
{

    class Socket
	{
		//socket options
		public:
			enum 
			{
				BLOCKING = true, 
				NO_DELAY = true,
				NO_TIMEOUT = -1
			};

		friend class SocketList;

		protected:
			/** Construct a new Socket of the specified type. */
			Socket();

		public:
			/** Destructor. Closes and destroys the socket. */
			virtual ~Socket();

			/**
			* Returns true if both sockets share the same
			* handler, false otherwise
			*/
			bool operator == (const Socket& socket) const;
			
			/**
			* Returns false if both sockets share the same
			* handler, false otherwise
			*/
			bool operator != (const Socket& socket) const;

			/**
			* Initialize the socket. This method creates the underlying socket
			* object.
			*
			* @throw SocketException If the socket could not be created.
			*/
			virtual void Init() {};

			/** Shut down the socket. Closes the connection and destroys the
			* underlying socket object.
			*/
			virtual int Shutdown(int shutMode=SHUT_RDWR);
			void Close();

			/**	Returns the number of bytes available that can be read
			* without causing the socket to block.
			*/
			int Available();

			/** Set the blocking for the socket.
			*
			* @param blocking Blocking or nonbling.
			* @throw SocketException If an error occurs.
			*/
			void SetBlocking(bool blocking) ;

			/** Set the size of the socket's internal receive buffer.
			*
			* @param size The new size, in bytes.
			* @throw SocketException If the size could not be changed.
			*/
			void SetReceiveBufSize(size_t size) ;

			/** Get the size of the socket's internal receive buffer.
			*
			* @return The size, in bytes.
			* @throw SocketException If the size could not be changed.
			*/
			size_t ReceiveBufSize() const ;

			/** Set the size of the socket's internal send buffer.
			*
			* @param size The new size, in bytes.
			* @throw SocketException If the size could not be changed.
			*/
			void SetSendBufSize(size_t size) ;

			/** Get the size of the socket's internal send buffer.
			*
			* @return The size, in bytes.
			* @throw SocketException If the size could not be changed.
			*/
			size_t SendBufSize() const ;

			/** Set the linger time for the socket.
			*
			* @param msec The new linger time, in milliseconds; a negative value
			* disables the linger feature.
			* @throw SocketException If the linger time could not be changed.
			*/
			void SetLingerTime(int msec) ;

			/** Get the linger time for the socket.
			*
			* @return The linger time, in milliseconds, or -1 if the feature is
			* disabled.
			* @throw SocketException If the linger time could not be retrieved.
			*/
			int LingerTime() const ;

			/** Enable or disable the SO_KEEPALIVE option on the socket.
			*
			* @param enable <b>true</b> to enable the option <b>false</b> to
			* disable it.
			* @throw SocketException If a socket error occurs.
			*/
			void SetKeepAlive(bool enable) ;

			/** Determine if the SO_KEEPALIVE option is enabled or disabled.
			*
			* @return <b>true</b> if the option is enabled, <b>false</b> otherwise.
			* @throw SocketException If a socket error occurs.
			*/
			bool KeepAlive() ;

			/** Enable or disable the Nagle algorithm for send coalescing.
			*
			* @param enable <b>true</b> to disable the Nagle algorithm
			* (TCP_NODELAY on), <b>false</b> to enable it (TCP_NODELAY off).
			* @throw SocketException If a socket error occurs.
			*/
			void SetNoDelay(bool enable) ;

			/** Determine if the Nagle algorithm is enabled or disabled.
			*
			* @return <b>true</b> if the Nagle algorithm is disabled (TCP_NODELAY on),
			* <b>false</b> otherwise.
			* @throw SocketException If a socket error occurs.
			*/
			bool NoDelay() ;

			/** Determine if the socket has been initialized. */
			inline bool Initialized() const
			{ return(mSocket != INVALID_SOCKET_HANDLE); }

			void setRecTimeout(unsigned int msec);

			/** Wait for the socket to become ready for reading.
			*
			* if (msec < 0) no timeout is set.
			* @throw SocketException If a socket error occurs.
			* @throw SocketTimeoutException On timeout
			*/
			void WaitForRead(int msec) ;

			/** Wait for the socket to become ready for writing.
			*
			* if (msec < 0) no timeout is set.
			* @throw SocketException If a socket error occurs.
			* @throw SocketTimeoutException On timeout
			*/
			void WaitForWrite(int msec) ;


            std::ostream& debugBzi(std::ostream& os) const
			{
                os  << "Handle: " << mSocket << std::endl
                    << "Blocking: " << ( (mBlocking) ? "true": "false") << std::endl
                    << "Instances: " << mInstances << std::endl;
 				return os;
			}

		protected:

			/** Get the underlying socket handle for this socket. */
			inline SocketHandle Handle() const
			{ return(mSocket); }

			/** A wrapper for the ioctl system call. */
			int ioctl(int request, int& arg) ;

			/** A handle to the socket itself. */
			SocketHandle mSocket;

			/** The timeout value. */
			bool mBlocking;

			/** Instances. */
			static int mInstances;
	};
}

#endif // SOCKET_H
