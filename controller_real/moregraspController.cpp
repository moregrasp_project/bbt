#include "moregraspController.h"
#include <iostream>
#include <sstream>

#include "core/data/signalStream.h"
#include "core/data/eventStream.h"
#include "core/utils/sysUtils.h"
#include "core/utils/hat.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>
#include <QDateTime>

using namespace std;
using namespace Bzi;

MoregraspController::MoregraspController(const string& name, Bzi::StreamSocket* const connection)
    : Bzi::Controller(name),
      _requests(connection),
      _receiver(connection),
      _configured(false),
      _running(false)
{
    connect(&_receiver, &Receiver::pendingMessage, this, &MoregraspController::onNewMessage);

    _log = true;
    if (_log)
    {
        std::ostringstream oss;
        oss << "C:/data/moregrasp_server_" << QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss").toStdString() << ".log";
        _log_file.open(oss.str().c_str());
    }
    _receiver.start();
}

MoregraspController::~MoregraspController()
{
    _log_file.close();
    finish();
    _receiver.stop();
    _receiver.quit();
    _receiver.wait();
}

void MoregraspController::wait()
{
    _receiver.wait();
    if (_running)
        onStop();
}

void MoregraspController::_handleDataMessage(const Bzi::ReceptionIdentifier &reception)
{
    RealTimeModule::_handleDataMessage(reception);
    vector<Data*> dataVector = getRefreshedData();

    qint64 last_ts = 0;
    QJsonArray jsondata;

    for (unsigned int i=0; i<dataVector.size(); i++)
    {
        SignalStream* signalData = dynamic_cast<SignalStream*>(dataVector[i]);
        if (signalData != NULL)
        {
            std::cout << "Signal " << signalData->getId() << " data sent to the tablet" << std::endl;
            QString txt;
            txt.append("Signal ").append(signalData->getId().c_str()).append(" received!");
            emit sigStatus(txt);

            for (int b = 0; b < signalData->size(); ++b)
            {

                QJsonObject object;

                object.insert("ID", signalData->getId().c_str());
                QJsonArray valuelist;
                SignalBlock* block = signalData->block(signalData->size() - 1);
                qint64 ts = block->timestamp();
                if (ts < last_ts)
                    std::cout << "Desordenado!!! anterior: " << last_ts << " actual: " << ts << std::endl;
                object.insert("TIMESTAMP", ts);
                //object.insert("TIMESTAMP2", clock.get());
                for (int i = 0; i < block->rows(); ++i)
                    for (int j = 0; j < block->cols(); ++j)
                        valuelist.append(block->value(i, j));
                object.insert("VALUES", valuelist);

                jsondata.append(object);
            }

        }
        else
        {
            EventStream* eventData = dynamic_cast<EventStream*>(dataVector[i]);
            if (eventData!= NULL)
            {
                std::cout << "Event " << eventData->getId() << " sent to the tablet" << std::endl;

                QString txt;
                txt.append("Event ").append(eventData->getId().c_str()).append(" received!");
                emit sigStatus(txt);

                for (int b = 0; b < eventData->size(); ++b)
                {
                    QJsonObject object;

                    object.insert("ID", eventData->getId().c_str());
                    QJsonArray valuelist;
                    EventBlock* block = eventData->block(eventData->size() - 1);
                    object.insert("TIMESTAMP", block->timestamp());
                    for (int i = 0; i < block->rows(); ++i)
                        for (int j = 0; j < block->cols(); ++j)
                            valuelist.append(block->value(i, j));
                    object.insert("VALUES", valuelist);

                    jsondata.append(object);
                }

                if (eventData->getId() == "EVENT_RESET")
                {
                    onStop();
                    _running = false;
                }
            }
        }
    }

    sendDataStream(jsondata);
}

void MoregraspController::sendDataStream(const QJsonArray &data)
{
    QJsonObject object;
    object.insert("DATA", data);
    reply(object);
}

void MoregraspController::replyConfig(const QJsonObject& config)
{
    QJsonObject object = config;
    reply(object);
}

void MoregraspController::replyOk(const QString &operation)
{
    QJsonObject object;
    object.insert("STATUS", QJsonValue("OK"));
    object.insert("OPERATION", QJsonValue(operation));
    object.insert("MODE", QJsonValue(_operation_mode));
    reply(object);
}

void MoregraspController::replyError(const QString& error_msg, const QString &operation)
{
    QJsonObject object;
    object.insert("STATUS", QJsonValue("Error"));
    object.insert("MSG", QJsonValue(error_msg));
    object.insert("OPERATION", QJsonValue(operation));
    object.insert("MODE", QJsonValue(_operation_mode));
    reply(object);
}

void MoregraspController::reply(const QJsonObject &object)
{
    QJsonDocument doc(object);
    QByteArray msg = doc.toJson(QJsonDocument::Compact);

    _log_file << "SEND: " << msg.toStdString() << std::endl;

    _requests->Write(msg.data(), msg.size());
}

void MoregraspController::onNewMessage(const QJsonObject& object)
{
    QJsonDocument doc(object);
    _log_file << "RECEIVE: " << doc.toJson(QJsonDocument::Compact).toStdString() << std::endl;

    if (object.isEmpty())
    {
        std::cout << "[SERVER] Bad Json format" << std::endl;
        replyError("Bad message format. Cannot parse json", "Message");
        return;
    }

    for(QJsonObject::const_iterator entry = object.constBegin(); entry != object.constEnd(); ++entry)
    {
        if (entry.key().toLower() == "load") //LOAD
        {
            if (entry.value().isString())
            {
                QString operation_mode = entry.value().toString();
                QString filename = operation_mode + ".conf";
                std::cout << "[SERVER] Request: Load " << filename.toStdString() << std::endl;
                _operation_mode = operation_mode;
                onLoadCommand(filename);
            }
            else
                replyError("The LOAD value must be a string", "load");
        }
        else if (entry.key().toLower() == "config") //CONFIG
        {
            std::cout << "[SERVER] Request: Config " << std::endl;
            if (entry.value().isArray())
            {
                QJsonArray array = entry.value().toArray();

                onConfigCommand(array);
            }
            else
                replyError("The CONFIG value must be a list", "load");
        }
        else if (entry.key().toLower() == "command") //COMMAND
        {
            if (entry.value().isString())
            {
                QString command = entry.value().toString().toLower();
                if (command == "config request") // CONFIG REQUEST
                {
                    std::cout << "[SERVER] Request: COMMAND config request " << std::endl;
                    onConfigRequest();
                }
                else if (command == "start") //START
                {
                    std::cout << "[SERVER] Request: COMMAND start " << std::endl;
                    onStartCommand();
                }
                else if (command == "stop") //STOP
                {
                    std::cout << "[SERVER] Request: COMMAND stop " << std::endl;
                    onStopCommand();
                }
                else
                    replyError("The COMMAND value " + command + " is not a valid command", "command");
            }
            else
                replyError("The COMMAND value must be a string", "command");
        }
        else if (entry.key().toLower() == "data")
        {
            if (entry.value().isArray())
            {
                QJsonArray array = entry.value().toArray();
                std::cout << "[SERVER] Event received " << std::endl;
                onEventCommand(array);
            }
        }
        else
        {
            replyError("Unknown entry key " + entry.key(), "key");
        }
    }
}

bool MoregraspController::onLoadConfig(QString filename, QStringList& listNames)
{
    _configsMap.setCurrentDir(SysUtils::env_resolve("BZI_FRAMEWORK_ROOT")+"/config");
    if (not _configsMap.unserialize(filename.toStdString()))
        return false;

    std::vector<std::string> names = _configsMap.names();
    //QStringList listNames;
    for (size_t i = 0; i < names.size(); i++) {
        listNames.append(QString::fromStdString(names[i]));
    }

    emit sigLoadedConfig(listNames);
    return true;
}

bool MoregraspController::onLaunch()
{
    Bzi::LauncherDescriptor launchDesc(_configuration);
    Bzi::ModuleLauncher launcher;
    if (not launcher.launchRealTimeModules(launchDesc))
    {
        std::cout << "Error launching real time modules" << std::endl;
        return false;
    }
    std::cout << "MODULES LAUNCHED" << std::endl;
    this->thread()->msleep(1000);

    if (not launcher.launchManager(launchDesc))
    {
        std::cout << "Error launching manager" << std::endl;
        return false;
    }
    std::cout << "MANAGER LAUNCHED" << std::endl;
    this->thread()->msleep(1000);
    if (not networkConnect(launchDesc.managerLaunchProp().mIp.toStdString(),launchDesc.managerLaunchProp().mTcpPort.toInt()))
    {
        std::cout << "Unable to connect to manager" << std::endl;
    }
    std::cout << "MANAGER CONNECTED" << std::endl;

    configureSystem(_configuration);
    emit sigLaunched(true);

    this->thread()->msleep(1000);
    return true;
}

bool MoregraspController::onStart()
{
    return sendStart();
}

bool MoregraspController::onStop()
{
    sendStop();
    onReset();
    return true;
}

bool MoregraspController::onReset()
{
    //resetSystemConfiguration();
    sendFinish();

    _configured = false;
    emit sigReseted();
    return true;
}

void MoregraspController::onFinish()
{
    finish();
}

void MoregraspController::onSendData(QString data)
{
    std::cout << "DATA: " << data.toStdString() << std::endl;
    QStringList pairs = data.split("\n");
    for (size_t i=0; i<pairs.size(); i++)
    {
        if (!pairs.at(i).isEmpty())
        {
            QStringList split = pairs.at(i).split("#");
            if (split.size() != 2)
            {
                QString txt;
                txt.append("Incorrect format. Use: data_name#value");
                emit sigStatus(txt);
            }
            else
            {
                Bzi::EventStream event;
                std::string data_name   = split.at(0).toStdString();
                std::string data_value  = split.at(1).toStdString();

                if (!getData(data_name, event))
                {
                    QString txt;
                    txt.append("Event ").append(data_name.c_str()).append(" does not exist");
                    emit sigStatus(txt);
                }
                else
                {
                    bool sent = true;
                    try
                    {
                        event.setValueFromStr(data_value);
                        for (size_t b=0; b<event.size(); b++)
                            event.block(b)->setTimestamp(_clock.get());
                        sendData(&event,_mManagerName);
                    }
                    catch(Exception &e) //capture unit exceptions but go on in pipeline process!!
                    {
                        sent = false;
                    }

                    QString txt;
                    if (sent)
                        txt.append("Event ").append(data_name.c_str()).append(" sent!");
                    else
                        txt.append("Event ").append(data_name.c_str()).append(" could not be sent, dimensions mismatch!");
                    emit sigStatus(txt);
                }
            }
        }
    }
}

void MoregraspController::onLoadCommand(const QString& operation_mode)
{
    QStringList names;

    if (onLoadConfig(operation_mode, names))
    {
        _config_name = names.front();        
        _configuration = _configsMap.get(_config_name.toStdString());
        _configured = true;

        replyOk("LOAD");
    }
    else
    {
        _configured = false;
        replyError("Cannot load operation mode", "LOAD");
    }
}

void MoregraspController::onStartCommand()
{
    if (not _configured)
    {
        replyError("Not yet configured", "start");
    }
    else if (_running)
    {
        replyError("Already running", "start");
    }
    else {
        if (onLaunch())
        {
            if (onStart())
            {
                _running = true;
                replyOk("start");
            }
            else
            {
                //onReset();
                _running = false;
                replyError("Unable to start", "start");
            }
        }
        else
        {
            replyError("Unable to launch configuration", "start");
        }
    }
}

void MoregraspController::onStopCommand()
{
    if (_configured and _running)
    {
        std::cout << "Reseting ..." << std::endl;
        if (onStop())
        {
            std::cout << "OK" << std::endl;
            _running = false;
            _configured = false;
            replyOk("stop");
        }
        else
            replyError("Unable to stop.", "stop");
    }
    else
        replyError("Not running", "stop");
}

void MoregraspController::onConfigCommand(const QJsonArray &config)
{
    if (_configured)
    {
        for (QJsonArray::const_iterator parameter = config.begin(); parameter != config.end(); ++parameter)
        {
            QJsonObject object = parameter->toObject();
            QJsonArray values (object.value("Values").toArray());
            std::string unit = object["Unit name"].toString().toStdString();
            std::string id = object["ID"].toString().toStdString();

            if (_configuration.existParameter(id, unit))
            {
                const Parameter& p = _configuration.parameter(id, unit);

                std::string param_value = parameterValue(p, values).toStdString();

                _configuration.updateParameter(id, param_value, unit);

                std::cout << unit << "#" << id << "#" << param_value << "\n";
            }
        }
        replyOk("config");
    }
    else
        replyError("Define an operation mode first", "config");
}

void MoregraspController::onConfigRequest()
{
    if (_configured)
    {
        QJsonObject json;
        QJsonArray array;
        ModuleMap modules = _configuration.modules();
        for (ModuleMap::const_iterator m = modules.begin(); m != modules.end(); ++m)
        {
            const std::vector<UnitConfiguration>& units = m->second.units();
            for (std::vector<UnitConfiguration>::const_iterator u = units.begin(); u != units.end(); ++u)
            {
                const std::vector<Parameter>& parameters = u->parameters();
                for (std::vector<Parameter>::const_iterator p = parameters.begin(); p != parameters.end(); ++p)
                {
                    QJsonObject object;
                    object.insert("Unit name", u->name().c_str());
                    object.insert("ID", p->name().c_str());
                    object.insert("Values", parameterValue(*p));
                    std::cout << u->name() << "#" << p->name() << "#" << p->value() << std::endl;
                    array.append(object);
                }
            }
        }
        json.insert("CONFIG", array);
        replyConfig(json);
    }
    else
    {
        replyError("Define an operation mode first", "config request");
    }
}

void MoregraspController::onEventCommand(const QJsonArray& events)
{
    if (_configured and _running)
    {
        QString data;

        for (QJsonArray::const_iterator event = events.begin(); event != events.end(); ++event)
        {
            QJsonObject object = event->toObject();
            QString id = object["ID"].toString();
            QJsonDocument values(object["VALUES"].toArray());

            QString values_str = QString::fromStdString(values.toJson(QJsonDocument::Compact).toStdString());
/*
            if (object["Values"].isArray())
            {
                values_str += "[";
                QJsonArray values = object["Values"];
                for (QJsonArray::const_iterator value = values.begin(); value != values.end(); ++value)
                {
                     values_str += QString::number(value->toDouble()) + " ";
                }
                values_str.= "]";
            }
            */

            data += id + "#" + values_str + "\n";
        }

        onSendData(data);
    }
    else {
        //replyError("Not running", "event");
        std::cout << "Not running. Events are discarded" << std::endl;
    }
}

void MoregraspController::onDisconnect()
{
    std::cout << "stopping because of disconnection" << std::endl;
    if (_running)
        onStop();
    _running = false;
    std::cout << "done" << std::endl;
    emit disconnected();
}


QJsonArray MoregraspController::parameterValue(const Bzi::Parameter& p) const
{

    QJsonArray array;
    if (p.type() == "Bzi::BziString")
    {
        array.append(QJsonValue(p.value().c_str()));
    }
    else if(p.type() == "Bzi::BziInt")
    {
        QString s = p.value().c_str();
        array.append(QJsonValue(s.toInt()));
    }
    else if(p.type() == "Bzi::BziDouble")
    {

        QString s = p.value().c_str();
        array.append(QJsonValue(s.toDouble()));
    }
    else if(p.type() == "Bzi::BziBool")
    {
        bool value = (p.value() == "true");
        array.append(QJsonValue(value));
    }
    else if(p.type() == "Bzi::BziIntVector")
    {
        QString s = p.value().c_str();
        QString s2 = s.mid(1, s.length()-2); //remove brackets


        QStringList values = s2.split(" ");

        foreach (QString value, values) {
            array.append(QJsonValue(value.toInt()));
        }

    }
    else if(p.type() == "Bzi::BziStringVector")
    {
        QString s = p.value().c_str();
        QString s2 = s.mid(1, s.length()-2); //remove brackets

        QStringList values = s2.split(" ");

        foreach (QString value, values) {
            array.append(QJsonValue(value.toStdString().c_str()));
        }

    }
    else if(p.type() == "Bzi::BziDoubleVector")
    {
        QString s = p.value().c_str();
        QString s2 = s.mid(1, s.length()-2); //remove brackets

        QStringList values = s2.split(" ");

        foreach (QString value, values) {
            array.append(QJsonValue(value.toDouble()));
        }

    }
    else if(p.type() == "Bzi::BziBoolVector")
    {
        QString s = p.value().c_str();
        QString s2 = s.mid(1, s.length()-2); //remove brackets

        std::cout << s2.toStdString() << std::endl;

        QStringList values = s2.split(" ");

        foreach (QString value, values) {
            bool v = (value == "true");
            array.append(QJsonValue(v));
        }

    }
    else
        std::cout << "Unknown type " << p.type() << std::endl;

    return array;
}

QString MoregraspController::parameterValue(const Parameter &p, const QJsonArray& array) const
{
    if (p.type() == "Bzi::BziString")
    {
        return array.first().toString();
    }
    else if(p.type() == "Bzi::BziInt")
    {
        return QString::number(array.first().toInt());
    }
    else if(p.type() == "Bzi::BziDouble")
    {
        return QString::number(array.first().toDouble());
    }
    else if(p.type() == "Bzi::BziBool")
    {
        if (array.first().toBool())
            return QString("true");
        else
            return QString("false");

    }
    else if(p.type() == "Bzi::BziIntVector")
    {
        QString output = "[";
        for (QJsonArray::const_iterator it = array.begin(); it != array.end(); ++it)
            output += QString::number(it->toInt()) + " ";
        output[output.length()-1] = ']';
        return output;

    }
    else if(p.type() == "Bzi::BziStringVector")
    {
        QString output = "[";
        for (QJsonArray::const_iterator it = array.begin(); it != array.end(); ++it)
            output += it->toString() + " ";
        output[output.length()-1] = ']';
        return output;
    }
    else if(p.type() == "Bzi::BziDoubleVector")
    {
        QString output = "[";
        for (QJsonArray::const_iterator it = array.begin(); it != array.end(); ++it)
            output += QString::number(it->toDouble()) + " ";
        output[output.length()-1] = ']';
        return output;

    }
    else if(p.type() == "Bzi::BziBoolVector")
    {
        QString output = "[";
        for (QJsonArray::const_iterator it = array.begin(); it != array.end(); ++it)
            if (it->toBool())
                output += "true ";
            else
                output += "false ";

        output[output.length()-1] = ']';
        return output;
    }
    else
        std::cout << "Unknown type " << p.type() << std::endl;

    return "";

}
