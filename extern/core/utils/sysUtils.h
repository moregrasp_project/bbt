/*
* File: sysUtils.h
* Description: class defining a set of sys utils
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef SYS_UTILS_H
#define SYS_UTILS_H

#include "core/utils/libHeader.h"
#include <string>

namespace Bzi
{
    class LIBRARY_API SysUtils
	{

        public:
            static std::string env_resolve(const std::string& var);
            static void env_replace(std::string& var);
	};
}

#endif //SYS_UTILS_H
