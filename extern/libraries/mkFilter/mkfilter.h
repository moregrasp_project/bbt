/* mkfilter -- given n, compute recurrence relation
   to implement Butterworth, Bessel or Chebyshev filter of order n
   A.J. Fisher, University of York   <fisher@minster.york.ac.uk>
   September 1992 */

/* Header file */

#pragma once

#ifndef BZI_MKFILTER__H
#define BZI_MKFILTER__H


#include <cmath>
#include <complex>
#include <vector>


// C++ types for the MkfilterWrapper handling
typedef double                    real_type;
typedef std::vector<real_type>    real_vector;
typedef std::complex<real_type>   complex_type;
typedef std::vector<complex_type> complex_vector;

#define global
#define unless(x)   if(!(x))
#define until(x)    while(!(x))

/* Microsoft C++ does not define M_PI ! */
#undef	PI
#define PI			3.14159265358979323846
#define TWOPI		(2.0 * PI)
#define EPS			1e-10
#define MAXORDER	10
#define MAXPZ		512
/* .ge. 2*MAXORDER, to allow for doubling of poles in BP filter; high values needed for FIR filters */
#define MAXSTRING   256

typedef void (*proc)();
typedef unsigned int uint;

inline double sqr(double x)
	{ return x*x; }

inline bool onebit(uint m)
	{ return (m != 0) && ((m & (m-1)) == 0); }

/* Microsoft C++ does not define */
inline double asinh(double x)
{ return log(x + sqrt(1.0 + sqr(x))); }

/* nearest integer */
inline double fix(double x)
{ return (x >= 0.0) ? floor(0.5+x) : -floor(0.5-x); }


enum filter_type_t
{
	BESSEL		=	0,		/* -Be		Bessel characteristic		*/
	BUTTERWORTH,			/* -Bu		Butterworth characteristic	*/
	CHEBYSHEV,				/* -Ch		Chebyshev characteristic	*/
	RESONATOR,				/* -Re		Resonator					*/
	PROPORTIONAL_INTEGRAL,	/* -Pi		proportional-integral		*/
};

enum filter_pass_t
{
	LOW_PASS	=	0,		/* -Lp	lowpass		*/
	HIGH_PASS,				/* -Hp	highpass	*/
	BAND_PASS,				/* -Bp	bandpass	*/
	BAND_STOP,				/* -Bs	bandstop	*/
	ALL_PASS,				/* -Ap	allpass		*/
};

int mkfilter(filter_type_t type,
			 filter_pass_t pass,
			 int order,
			 real_type alpha1,
			 real_type alpha2,
			 real_type ripple,
			 int& numzero,
			 real_vector& xcoeffs,
			 int& numpole,
			 real_vector& ycoeffs,
			 real_type& gain);


#endif
