/*
* File: moduleLauncher.h
* Description: ModuleLauncher class defining methods to
*			   launch modules as described in a LauncherDescriptor object
* Date: May 23rd 2012 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#ifndef MODULE_LAUNCHER_H
#define MODULE_LAUNCHER_H

#include <QFile>
#include <QXmlStreamReader>
#include <QString>
#include <QMap>
#include <QProcess>

#include "string.h"
#include "launcherDescriptor.h"

namespace Bzi
{
	class LIBRARY_API ModuleLauncher: public QObject
	{
		public:

			ModuleLauncher();
			~ModuleLauncher();

			bool launchRealTimeModules(LauncherDescriptor &launchDesc);
            bool launchRealTimeModule(LauncherDescriptor &launchDesc, const std::string &moduleName);
			bool launchManager(LauncherDescriptor &launchDesc);
			bool terminate();

		private:
			//attributes
            std::vector<QProcess*> _mAttachedLaunched;

			//methods
			bool _launchRealTimeModules(const QMap<QString,RealTimeModuleLaunchProperties> &rtModProp);
			bool _launchManager(const ManagerLaunchProperties &managerProp);
			//bool _launchController(const ControllerLaunchProperties &controllerProp);
            bool _launch(QString exeFile,QStringList arguments, bool detached);
            bool _remoteLaunch(QString exeFile,const QStringList &arguments,const QString &ip,const QString &sshUser = "");

			void _finalize();
            QString _compoundExeFileName(const QString &relativeName);
            void _prepareArgumentsAndExeForLinux(QString &exeFile, QStringList &arguments);
	};
}

#endif //MODULE_LAUNCHER_H
