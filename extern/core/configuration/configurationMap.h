/*
* File: configurationMap.h	
* Description: class defining a configuration container to keep
*			   different configurations accessed by name.
* Date: February 06th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#pragma once

#ifndef CONFIGURATION_MAP_H
#define CONFIGURATION_MAP_H

#include "core/configuration/configuration.h"

#include <QFile>
#include <QFileInfo>
#include <QXmlStreamReader>
#include <QXmlStreamAttributes>

#include <map>

namespace Bzi
{
	class LIBRARY_API ConfigurationMap: public DebuggingInterface
	{

		public:
			ConfigurationMap();
			ConfigurationMap(const std::string &xmlFileName);
			ConfigurationMap(const ConfigurationMap &confMap);
			~ConfigurationMap();

			std::ostream& debugBzi(std::ostream &os) const;

            void setCurrentDir(const std::string& s);

			bool unserialize(const std::string &xmlFileName);
			bool unserializeFromStr(const std::string &xmlStr);
			bool serialize(const std::string &xmlFileName);

            /**
             * @brief exist Check whether a precise configuration exists or not
             * @param configurationName the name of the configuration
             * @return true if it exists
             */
			bool exist(const std::string &configurationName) const;
            /**
             * @brief contains Checks if the configuration contains a precise module
             * @param moduleName the name of the module
             * @return true if the module is contained
             */
            bool contains(const std::string &moduleName) const; //true if any configuration has this module
			
			/**
			 * @brief get Retrieves the names of all the configurations
			 * @return Names of all the configurations
			 */
			std::vector<std::string> names() const;

            /**
             * @brief get Retrieves a precise configuration
             * @param configurationName The name of the configuration
             * @return A configuration with a given name
             */
			Configuration get(const std::string &configurationName) const;

			Configuration getFirstThatContains(const std::string &moduleName) const;

            void put(Configuration config);

			static const std::string XML_FILE_EXTENSION;

		private:
			//static attributes
			static const QString _XML_ROOT_ELEMENT;

			std::map<std::string,Configuration> _mConfigurations; //key = configurationName, value = Configuration

			void _assign(const ConfigurationMap &configMap);
			bool _unserialize(QXmlStreamReader &xmlStreamReader);

	};
}

#endif //CONFIGURATION_MAP_H
