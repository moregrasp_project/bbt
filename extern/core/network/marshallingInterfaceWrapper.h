/*
 * File: marshallingInterfaceWrapper.h
 * Description: class defining the marshalling interface template to wrapper  qt primitives types marshalling
 * Date: -
 * Author: BitBrain Technologies
*/

#pragma once


#ifndef MARSHALLING_INTERFACE_WRAPPER_H
#define MARSHALLING_INTERFACE_WRAPPER_H

#include <string>
#include <QDataStream>
#include "core/utils/libHeader.h"

namespace Bzi
{
//GENERIC FUNCTIONS: BEGIN
    template <class T> inline
    void  marshall(T &obj, QDataStream &qDataStream)
    {
		obj.marshall(qDataStream);
    }

    template <class T> inline
    void  unmarshall(T &obj, QDataStream &qDataStream)
    {
		obj.unmarshall(qDataStream);
    }

//POINTERS
    template <class T> inline
    void  marshall(T *pObj, QDataStream &qDataStream)
    {
		if( pObj )
			pObj->marshall(qDataStream);
    }

    template <class T> inline
    void  unmarshall(T *pObj, QDataStream &qDataStream)
    {
		if( pObj )
			pObj->unmarshall(qDataStream);
    }

//GENERIC FUNCTIONS: END

//TEMPLATE SPECIALIZATIONS: BEGIN

//CHAR

    template <> inline
    //void LIBRARY_API marshall(char &obj, QDataStream &qDataStream)
    void marshall(char &obj, QDataStream &qDataStream)
    {
		//qDataStream << (qint8)obj;
		qDataStream.writeRawData(&obj,1);
    }

    template <> inline
    //void LIBRARY_API unmarshall(char &obj, QDataStream &qDataStream)
    void unmarshall(char &obj, QDataStream &qDataStream)
    {
		//qDataStream >> (qint8&)obj;
		qDataStream.readRawData(&obj,1);

    }

    template <> inline
    //void LIBRARY_API marshall(unsigned char &obj, QDataStream &qDataStream)
    void marshall(unsigned char &obj, QDataStream &qDataStream)
    {
		qDataStream << (quint8)obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(unsigned char &obj, QDataStream &qDataStream)
    void unmarshall(unsigned char &obj, QDataStream &qDataStream)
    {
		qDataStream >> (quint8&)obj;
    }

//STRING
    template <> inline
    //void LIBRARY_API marshall(std::string &obj, QDataStream &qDataStream)
    void marshall(std::string &obj, QDataStream &qDataStream)
    {
		qDataStream <<  (quint32)(obj.length()); //note the null char* termination
		qDataStream.writeRawData(obj.c_str(),obj.length());
    }

    template <> inline
    //void LIBRARY_API unmarshall(std::string &obj, QDataStream &qDataStream)
    void unmarshall(std::string &obj, QDataStream &qDataStream)
    {
		unsigned int length=0;
		
		qDataStream >> length;
		char* cStr = new char[length +1];

		qDataStream.readRawData(cStr,length);
		cStr[length] = '\0';
		obj.assign( cStr );
		delete[] cStr;
    }

//CHAR*
    template <> inline
    //void LIBRARY_API marshall(char* &obj, QDataStream &qDataStream)
    void marshall(char* &obj, QDataStream &qDataStream)
    {
		qDataStream <<  obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(char* &obj, QDataStream &qDataStream)
    void unmarshall(char* &obj, QDataStream &qDataStream)
    {
		qDataStream >> obj;
    }

//INT
    template <> inline
    //void LIBRARY_API marshall(int &obj, QDataStream &qDataStream)
    void marshall(int &obj, QDataStream &qDataStream)
    {
		qDataStream << (qint32)obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(int &obj, QDataStream &qDataStream)
    void unmarshall(int &obj, QDataStream &qDataStream)
    {
		qDataStream >> (qint32&)obj;
    }

    template <> inline
    //void LIBRARY_API marshall(unsigned int &obj, QDataStream &qDataStream)
    void marshall(unsigned int &obj, QDataStream &qDataStream)
    {
		qDataStream << (quint32)obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(unsigned int &obj, QDataStream &qDataStream)
    void unmarshall(unsigned int &obj, QDataStream &qDataStream)
    {
		qDataStream >> (quint32&)obj;
    }

    template <> inline
    //void LIBRARY_API marshall(long int &obj, QDataStream &qDataStream)
    void marshall(long int &obj, QDataStream &qDataStream)
    {
		qDataStream << (qint64)obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(long int &obj, QDataStream &qDataStream)
    void unmarshall(long int &obj, QDataStream &qDataStream)
    {
		qDataStream >> (qint64&)obj;
    }

    template <> inline
    //void LIBRARY_API marshall(long int &obj, QDataStream &qDataStream)
    void marshall(qint64 &obj, QDataStream &qDataStream)
    {
        qDataStream << (qint64)obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(long int &obj, QDataStream &qDataStream)
    void unmarshall(qint64 &obj, QDataStream &qDataStream)
    {
        qDataStream >> (qint64&)obj;
    }

//FLOAT
    template <> inline
    //void LIBRARY_API marshall(float &obj, QDataStream &qDataStream)
    void marshall(float &obj, QDataStream &qDataStream)
    {
		qDataStream << obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(float &obj, QDataStream &qDataStream)
    void unmarshall(float &obj, QDataStream &qDataStream)
    {
		qDataStream >> obj;
    }

//DOUBLE
    template <> inline
    //void LIBRARY_API marshall(double &obj, QDataStream &qDataStream)
    void marshall(double &obj, QDataStream &qDataStream)
    {
		qDataStream << obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(double &obj, QDataStream &qDataStream)
    void unmarshall(double &obj, QDataStream &qDataStream)
    {
		qDataStream >> obj;
    }

//BOOL
    template <> inline
    //void LIBRARY_API marshall(bool &obj, QDataStream &qDataStream)
    void marshall(bool &obj, QDataStream &qDataStream)
    {
		qDataStream << obj;
    }

    template <> inline
    //void LIBRARY_API unmarshall(bool &obj, QDataStream &qDataStream)
    void unmarshall(bool &obj, QDataStream &qDataStream)
    {
		qDataStream >> obj;
    }

//TEMPLATE SPECIALIZATIONS: END

}

#endif // MARSHALLING_INTERFACE_WRAPPER_H
