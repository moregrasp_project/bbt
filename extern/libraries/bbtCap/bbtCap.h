#ifndef BBT_CAP_H
#define BBT_CAP_H

#include "core/utils/math/pBand.h"
#include "core/utils/math/aveSignal.h"
#include "core/data/signalStream.h"
#include "core/data/eventStream.h"
#include "core/utils/libHeader.h"

namespace Bzi
{

    class LIBRARY_API Impedance
    {
        public:
            Impedance(int chans, int samples, int SR, float sat, std::vector<float> thres, float updateTime = 2.5);
            ~Impedance();

            bool process(SignalStream* in);
            int getValue(int ch) const;

        private:
            Pband*                      _mpPowerBand;
            std::vector<float>          _mPowOutput;

            AveSignal*                  _mpAveSignal;
            std::vector<float>          _mAveOutput;

            float                       _mSat;
            std::vector<float>          _mThresh;
            std::vector<int>            _mStatus;
    };
}

#endif // BBT_CAP_H
