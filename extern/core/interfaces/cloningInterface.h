/*
* File: cloningInterface.h	
* Description: interface for clonning objects
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef CLONING_INTERFACE_H
#define CLONING_INTERFACE_H

#include "core/utils/libHeader.h"

namespace Bzi
{
	class Data;
	class DataClonningInterface
	{
		public:
            virtual ~DataClonningInterface() {}
            virtual Data* clone() = 0;
	};

	class Slice;
	class SliceClonningInterface
	{
		public:
            virtual ~SliceClonningInterface() {}
            virtual Slice* cloneSlice() = 0;
	};
}
#endif //CLONING_INTERFACE_H
