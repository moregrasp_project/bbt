/*
* File: unknownDataException.h	
* Description: exception produced when getting a data from repository and it does not exist
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef UNKNOWN_DATA_EXCEPTION_H
#define UNKNOWN_DATA_EXCEPTION_H

#include "core/exceptions/exception.h"
#include <string>

namespace Bzi
{
	class LIBRARY_API UnknownDataException : public Exception
	{
		public:

            UnknownDataException(const std::string& dataName ,const std::string& message)
				:_mDataName(dataName),
				  _mMessage(message)

			{}

            std::ostream& debugBzi(std::ostream& os) const
			{ 
				return os<<_sName
						 <<":\n\t"
                         << _mDataName << std::endl
						 << "\t" <<_mMessage
                         <<std::endl;
			}

		private:
        std::string _mDataName;
        std::string _mMessage;
        static const std::string _sName;
		
	};
}

#endif //UNKNOWN_DATA_EXCEPTION_H
