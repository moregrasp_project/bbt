clear all; close all; clc;

%% LOADING DATA FILES...
% loading a bbt file (check that exists...)
filename_bbt = 'C:/data/mg/Session00/mgS00R000.bbt';
[ PROP, SIGNALS, EVENTS, PARAMS ] = bbt_load(filename_bbt);

% PARAMS: struct containing all the parameters grouped by unit
PARAMS

% PROP: struct containing the signals and events properties:
%   PROP.SIGNALS: struct array with fields (ID, NumChannels, NumElements, NumFields, SamplingRate)
%   PROP.EVENTS:  struct array with fields (ID, NumChannels, NumElements, InitValues)
% Arrays in SIGNALS and EVENTS are ordered in the same way as PROP
PROP.SIGNALS
PROP.EVENTS

% SIGNALS: struct containing (data, timestamp, sequence number, battery level)
%   SIGNALS.VALUES: data values, cell array (NumChannels x NumSamples)
%   SIGNALS.T: timestamp, cell array (1 x NumBlocks), in microseconds.
%   SIGNALS.F: null, unused
% Note that NumBlocks = (NumSamples/NumElements)
SIGNALS.VALUES
SIGNALS.T

% EVENTS: struct containing (data, timestamp)
EVENTS.VALUES
EVENTS.T
%%

return

%% LOADING LOG FILES...
% loading log files (check that exist...)
filename_log_module = 'C:/data/logs/2016_01_26_13_21_01_Processing_module.log';
filename_log_units = 'C:/data/logs/2016_01_26_13_21_01_Processing_units.log';

log_module = bbt_loadLog_module(filename_log_module);
% cell matrix in format: cycle timestamp process {data_input/data_output} dataPacket1:dataPacketN
%   each dataPacket is composed of the id and number of blocks
%   tick denotes a null input or output

log_units = bbt_loadLog_units(filename_log_units);
% cell matrix in format: cycle timestamp unit_name {initialize/process/reset/delete} {entry/exit}
%%
