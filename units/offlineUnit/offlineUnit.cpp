#define OFFLINE_UNIT_CPP

#include "offlineUnit.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace std;

namespace Bzi
{
    namespace Units
    {
        UNIT_FACTORY( OfflineUnit ) //register Unit in compilation time.

        OfflineUnit::OfflineUnit()
            : _first(true)
        {
            // constructor will be called once before initialize()
            // parameters CANNOT be accessed
        }

        OfflineUnit::~OfflineUnit()
        {
            // destructor will be called once after reset()
        }

        void OfflineUnit::initialize()
        {
            _resetEvent = getEventStream("EVENT_RESET");
        }


        void OfflineUnit::process()
        {
            if (_first)
            {
                ::Sleep(5000);

                _resetEvent->resize(1);
                _resetEvent->block(0)->setValue(0,0,1);
                setDataModified(_resetEvent);

                _first = false;
            }
        }

        void OfflineUnit::reset()
        {
        }
    }
} // end namespace
