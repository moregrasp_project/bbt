#ifndef LOGGER_H
#define LOGGER_H

#include "core/utils/libHeader.h"
#include "core/utils/hat.h"

#include <QFile>
#include <QTextStream>

namespace Bzi
{
    class LIBRARY_API Logger
	{
		public:
            static Logger& instance();

            void createLogFile(const std::string &fileName);
            void closeLogFile();
            QTextStream& moduleStream() {return _mModuleStream;}
            QTextStream& unitStream() {return _mUnitsStream;}

            qint64 t() const;
            quint64 c() const;
            QString sep() const;

            void nextC();
            void resetC();

		private:
            Logger();
            ~Logger();

            QFile _mModuleFile;
            QTextStream _mModuleStream;
            QFile _mUnitsFile;
            QTextStream _mUnitsStream;

            quint64         _cycles;
            Hat             _clock;
            QString         _sep;
            QString _currentDateTime() const;
			void _finalize();
	};
}

#endif //LOGGER_H
