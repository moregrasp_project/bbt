#define PARTNER_PROT_UNIT_CPP

#include "partnerProtUnit.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace std;

namespace Bzi
{
    namespace Units
    {
        UNIT_FACTORY( PartnerProtUnit ) //register Unit in compilation time.

        PartnerProtUnit::PartnerProtUnit()
        {
            // constructor will be called once before initialize()
            // parameters CANNOT be accessed
        }

        PartnerProtUnit::~PartnerProtUnit()
        {
            // destructor will be called once after reset()
        }

        void PartnerProtUnit::initialize()
        {
            // PARAMETERS CAN BE ACCESSED ------------------------------------------
                // basic data types
                int dummyInt            = getInt("dummyInt");
                double dummyDouble      = getDouble("dummyDouble");
                bool dummyBool          = getBool("dummyBool");
                std::string dummyString = getString("dummyString");
                // vector data types of basic data types
                std::vector<int> dummyIntVector             = getIntVector("dummyIntVector");
                std::vector<double> dummyDoubleVector       = getDoubleVector("dummyDoubleVector");
                std::vector<bool> dummyBoolVector           = getBoolVector("dummyBoolVector");
                std::vector<std::string> dummyStringVector  = getStringVector("dummyStringVector");
            // ---------------------------------------------------------------------


            // PROPERTIES OF THE SIGNAL STREAMS AND EVENTS CAN BE ACCESSED ---------
            // - only those signal streams and events declared as inputs or outputs in
            // in the configuration file are accessible
            // - note that at this point only the following properties are readable,
            // data matrix values and timestamps are only accesible in process()
            _mpOutputDummyEvent  = getEventStream("EVENT_DUMMY");
            int out_event_rows_dummy = _mpOutputDummyEvent->rows();
            int out_event_cols_dummy = _mpOutputDummyEvent->cols();
            std::vector<double> out_event_init_dummy = _mpOutputDummyEvent->initValues();

            _mpOutputResetEvent  = getEventStream("EVENT_RESET");
            int out_event_rows_reset = _mpOutputDummyEvent->rows();
            int out_event_cols_reset = _mpOutputDummyEvent->cols();
            std::vector<double> out_event_init_reset = _mpOutputDummyEvent->initValues();
            // ---------------------------------------------------------------------

            // we want to generate an output event (EVENT_DUMMY) every time2event ms
            _cycle_time = getDouble("softCycle");
            _time2event = getInt("time2event");
            _cycles2event = int(double(_time2event)/_cycle_time);
            _counter_cycles = 0;
            _counter_events = 0;
        }


        void PartnerProtUnit::process()
        {
            _counter_cycles++;
            if (_counter_cycles == _cycles2event)
            {
                _counter_cycles = 0;
                _counter_events++;
                if (_counter_events < 20)
                {
                    // GENERATE DUMMY EVENT
                    // first we must defined the number of blocks and then to set the data values as desired
                    // timestamp can be set by calling the hat (high accuracy timer) method
                    _mpOutputDummyEvent->resize(1);
                    _mpOutputDummyEvent->block(0)->setValue(0, 0, _counter_events);
                    _mpOutputDummyEvent->block(0)->setTimestamp(hat());
                    // finally we must call setModified() to make the event available to the system
                    setDataModified("EVENT_DUMMY");
                }
                else
                {
                    // GENERATE RESET EVENT TO STOP THE SYSTEM
                    _mpOutputResetEvent->resize(1);
                    _mpOutputResetEvent->block(0)->setValue(0, 0, 1);
                    setDataModified("EVENT_RESET");
                }
            }
        }

        void PartnerProtUnit::reset()
        {
        }
    }
} // end namespace
