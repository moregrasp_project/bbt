disp('Matlab initialize')

% add the path to the hat function (high accuracy timer)
addpath(strcat(getenv('BZI_FRAMEWORK_ROOT'),'/units_matlab/hat'));

%% define paradigm types
CLASSIC_MI = 0;
NATURAL_MI = 1;
REST = 2;
EYEMOV = 3;

%% define feedback types
NO_FB = 0;
CONT_FB = 1;
DISC_FB = 2;

%% define event codes
trial_start_code = hex2dec('300');
trial_end_code = trial_start_code + hex2dec('8000');
beep_code = hex2dec('311');
cross_start_code = hex2dec('312');
cross_end_code = cross_start_code + hex2dec('8000');
continuousfeedback_start_code = hex2dec('30D');
continuousfeedback_end_code = hex2dec('830D');
discretefeedback_start_code = hex2dec('30E');
discretefeedback_end_code = hex2dec('830E');
end_code = 0;


%% get parameters
cue_types = PARAMS_ParadigmGenerator.CueTypes(:);
cue_frequency = PARAMS_ParadigmGenerator.CueFrequency(:);
assert(length(cue_types) == length(cue_frequency));
feedback = PARAMS_ParadigmGenerator.Feedback;

pre_duration = PARAMS_ParadigmGenerator.PreDuration;
post_duration = PARAMS_ParadigmGenerator.PostDuration;

beep_start = PARAMS_ParadigmGenerator.BeepStart;
cross_start = PARAMS_ParadigmGenerator.CrossStart;
cross_duration = PARAMS_ParadigmGenerator.CrossDuration;
cue_start = PARAMS_ParadigmGenerator.CueStart;
cue_duration = PARAMS_ParadigmGenerator.CueDuration;

intertrial_interval_min_duration = PARAMS_ParadigmGenerator.InterTrialIntervalMinDuration;
intertrial_interval_max_duration = PARAMS_ParadigmGenerator.InterTrialIntervalMaxDuration;

paradigm_type = PARAMS_ParadigmGenerator.ParadigmType;
switch paradigm_type
    case CLASSIC_MI
        trial_duration = PARAMS_ParadigmGenerator.ClassicMIMinimumTrialDuration;
    case NATURAL_MI
        trial_duration = PARAMS_ParadigmGenerator.NaturalMIMinimumTrialDuration;
    case REST
        trial_duration = PARAMS_ParadigmGenerator.RestTrialDuration;
    case EYEMOV
        trial_duration = PARAMS_ParadigmGenerator.EyeMovTrialDuration;
    otherwise
        error('unsupported paradigm type');
end

switch feedback
    case NO_FB
        feedback_start_code = nan;
        feedback_end_code = nan;
        feedback_start = 0;
        feedback_duration = 0;
    case CONT_FB
        feedback_start_code = continuousfeedback_start_code;
        feedback_end_code = continuousfeedback_end_code;
        feedback_start = PARAMS_ParadigmGenerator.ContinuousFeedbackStart;
        feedback_duration = PARAMS_ParadigmGenerator.ContinuousFeedbackDuration;
    case DISC_FB
        feedback_start_code = discretefeedback_start_code;
        feedback_end_code = discretefeedback_end_code;
        feedback_start = PARAMS_ParadigmGenerator.DiscreteFeedbackStart;
        feedback_duration = PARAMS_ParadigmGenerator.DiscreteFeedbackDuration;
    otherwise
        error('unsupported feedback type');
end

start_time = 0;
timeline = [];
events = [];
time = pre_duration;


switch(paradigm_type)
    case {CLASSIC_MI, NATURAL_MI}
        % build trial sequence
        cue_sequence = [];
        for cue_idx = 1:length(cue_frequency)
            cue_sequence = [cue_sequence; repmat(cue_types(cue_idx), cue_frequency(cue_idx), 1)];
        end
        trial_sequence_length = length(cue_sequence);
        % shuffle trials
        cue_sequence = cue_sequence(randperm(trial_sequence_length));
        
        % build event code sequence
        for trial_idx = 1:trial_sequence_length
            % trial start
            timeline = [timeline; time];
            events = [events; trial_start_code];
            % trial end
            timeline = [timeline; time + trial_duration];
            events = [events; trial_end_code];
            
            % beep
            timeline = [timeline; time + beep_start];
            events = [events; beep_code];
            
            % cross start
            timeline = [timeline; time + cross_start];
            events = [events; cross_start_code];
            % cross end
            timeline = [timeline; time + cross_start + cross_duration];
            events = [events; cross_end_code];
            
            % cue start
            timeline = [timeline; time + cue_start];
            events = [events; cue_sequence(trial_idx)];
            % cue end
            timeline = [timeline; time + cue_start + cue_duration];
            events = [events; cue_sequence(trial_idx) + hex2dec('8000')];
            
            if feedback > 0
                % feedback start
                timeline = [timeline; time + feedback_start];
                events = [events; feedback_start_code];
                % feedback end
                timeline = [timeline; time + feedback_start + feedback_duration];
                events = [events; feedback_end_code];
            end

            iti = intertrial_interval_min_duration + (intertrial_interval_max_duration-intertrial_interval_min_duration)*rand;
            actual_trial_duration = max([trial_duration, beep_start, cross_start + cross_duration, cue_start + cue_duration, feedback_start + feedback_duration]);
            time = time +  actual_trial_duration + iti;
        end
        
    case REST
        % trial start
        timeline = [timeline; time];
        events = [events; trial_start_code];
        % trial end
        timeline = [timeline; time + trial_duration];
        events = [events; trial_end_code];
        
        % beep
        timeline = [timeline; time];
        events = [events; beep_code];
        
        % cross start
        timeline = [timeline; time];
        events = [events; cross_start_code];
        % cross end
        timeline = [timeline; time + trial_duration];
        events = [events; cross_end_code];
        
        time = time +  trial_duration;
        
    case EYEMOV
        % trial start
        timeline = [timeline; time];
        events = [events; trial_start_code];
        % trial end
        timeline = [timeline; time + trial_duration];
        events = [events; trial_end_code];
        
        % beep
        timeline = [timeline; time];
        events = [events; beep_code];
        
        time = time + trial_duration;
    otherwise
        error('unsupported paradigm type');
end

time = time + post_duration;
timeline = [timeline; time];
events = [events; end_code];

% sort
[timeline, sort_idcs] = sort(timeline, 1, 'ascend');
events = events(sort_idcs);

event_pointer = 0;
n_events = length(events);
