function [highTime, lowTime] = mexhat
%MEXHAT High Accuracy Timer
%  [TIME] = MEXHAT uses the high-resolution performance counter to get the 
%  time which exceeds one microsecond accuracy.
