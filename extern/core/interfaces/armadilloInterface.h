/*
* File: ArmadilloInterface.h	
* Description: interface (abstract class) defining methods
*			   for convert a data object to-from armadillo matrices 
* Date: August 17th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#pragma once

#ifndef ARMADILLO_INTERFACE_H
#define ARMADILLO_INTERFACE_H

#ifdef COMPILE_ARMADILLO
#include "armadillo"
#endif

#include "core/utils/libHeader.h"

namespace Bzi
{
    /**
     * @brief The ArmadilloInterface class
     *      DataBlock
     */
    class LIBRARY_API ArmadilloInterface
	{
		public:
            virtual ~ArmadilloInterface() {}

#ifdef COMPILE_ARMADILLO
            virtual void toArmadilloMat(arma::mat&, bool copyMemory=true) {}
            virtual void fromArmadilloMat(const arma::mat&) {}
#endif
	};
}

#endif //ARMADILLO_INTERFACE_H
