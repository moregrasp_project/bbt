/*
* File: socketTimeoutException.h	
* Description: exception produced by a socket timeout error
*			   
* Date: March 11st 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
*		  BitBrain Technologies
*/


#pragma once

#ifndef SOCKET_TIMEOUT_EXCEPTION_H
#define SOCKET_TIMEOUT_EXCEPTION_H

#include "networkException.h"
#include <string>

namespace Bzi
{
	class SocketTimeoutException : public NetworkException
	{
		public:

            SocketTimeoutException(const std::string& message);
            std::ostream& put(std::ostream& os) const ;

		private:
            /*std::string _mMessage;*/
            static const std::string _sName;
		
	};
}

#endif //SOCKET_TIMEOUT_EXCEPTION_H
