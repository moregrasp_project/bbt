disp('Matlab initialize')

% add the path to the hat function (high accuracy timer)
addpath(strcat(getenv('BZI_FRAMEWORK_ROOT'),'/units_matlab/hat'));

% PARAMETERS CAN BE ACCESSED
% PARAMS.dummyInt
% PARAMS.dummyIntVector
% ...

% we want to generate an output event (EVENT_DUMMY) every time2event ms
cycle_time = PARAMS_PartnerProtMatlab.softCycle;
time2event = PARAMS_PartnerProtMatlab.time2event;
cycles2event = time2event/cycle_time;
counter_cycles = 0;
counter_events = 0;
