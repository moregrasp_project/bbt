/*
* File: MoregraspController.h
* Description: main application to load a configuration file
* Author: Carlos Escolano (carlos.escolano@bitbrain.es)
*		  BitBrain Technologies
* Date: 01/04/2015
*/
#ifndef OPERATOR_CONTROLLER_H
#define OPERATOR_CONTROLLER_H

#include <QJsonArray>
#include <QJsonObject>

// controller
#include "core/exceptions/exception.h"
#include "core/controller/controller.h"
#include "core/configuration/configurationMap.h"
#include "core/utils/hat.h"
#include "core/module/moduleLauncher/moduleLauncher.h"

#include <QJsonArray>
#include <QJsonObject>

#include "receiver.h"

class MoregraspController : public Bzi::Controller
{
        Q_OBJECT

    public:
        MoregraspController(const std::string& name, Bzi::StreamSocket * const connection);
        ~MoregraspController();

        void wait();

    private:
        // Controller
        Bzi::ConfigurationMap	_configsMap;
        Bzi::Hat _clock;
        QString _config_name;        

        Bzi::Configuration _configuration;
        bool _configured;
        bool _running;
        void _handleDataMessage(const Bzi::ReceptionIdentifier &reception);
        QJsonArray parameterValue(const Bzi::Parameter& p) const;
        QString parameterValue(const Bzi::Parameter &parameter, const QJsonArray& array) const;

        Bzi::StreamSocket* const _requests;
        Receiver _receiver;
        std::ofstream _log_file;
        bool _log;
        QString _operation_mode;

    signals:
        void	sigLoadedConfig(QStringList names);
        void	sigLaunched(bool ok);
        void    sigReseted();
        void    disconnected();
        void    sigStatus(QString text);

    public slots:
        bool onLoadConfig(QString filename, QStringList &names);
        bool onLaunch();
        bool onStart();
        bool onStop();
        bool onReset();
        void onFinish();
        void onSendData(QString data);

        void onLoadCommand(const QString& operation_mode);
        void onStartCommand();
        void onStopCommand();
        void onConfigCommand(const QJsonArray& config);
        void onConfigRequest();
        void onEventCommand(const QJsonArray& events);
        virtual void onDisconnect();

        //low level communication
        void onNewMessage(const QJsonObject& msg);
        void sendDataStream(const QJsonArray &data);
        void reply(const QJsonObject &object);
        void replyOk(const QString& operation);
        void replyConfig(const QJsonObject &config);
        void replyError(const QString& error_msg, const QString& operation);
};

#endif // OPERATOR_CONTROLLER_H
