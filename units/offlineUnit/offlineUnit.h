#ifndef OFFLINE_UNIT_H
#define OFFLINE_UNIT_H

#include "core/units/genericUnit.h"

namespace Bzi
{
    namespace Units
    {
        class OfflineUnit: public Bzi::GenericUnit
        {
            public:
                OfflineUnit();
                ~OfflineUnit();

                void initialize();
                void process();
                void reset();

            private:
                EventStream*    _resetEvent;

                bool    _first;
        };
    }
} // end namespace

#endif //OFFLINE_UNIT_H
