/*
* File: initializingParameter.h	
* Description: class defining a initializing parameter for any kind of Data
*			   this objects will be passed to the initializeData() function defined for
*			   Data that need initialization after being created.
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef INITIALIZING_PARAMETER_H
#define INITIALIZING_PARAMETER_H	

#include <map>

#include "core/interfaces/debuggingInterface.h"
#include "core/network/marshallingInterface.h"
#include "core/utils/libHeader.h"

namespace Bzi
{
    class LIBRARY_API InitializingParameter: public DebuggingInterface,
            public MarshallingInterface
    {
        public:
            std::string	mName, mValue;
            //possible regular Expression indicating format??

            InitializingParameter(){}
            InitializingParameter(const std::string &name, const std::string &value)
            {
                mName = name;
                mValue = value;
            }
            InitializingParameter(const InitializingParameter &initParam)
            {
                _assign(initParam);
            }

            bool operator==(const InitializingParameter &initParam)
            {
                return ((mName==initParam.mName)&&
                        (mValue==initParam.mValue));
            }

            std::ostream& debugBzi(std::ostream& os) const
            {
                os << " name: "  << mName
                   << " value: " << mValue
                   << std::endl;

                return os;
            }

            void marshall(QDataStream &qDataStream)
            {
                Bzi::marshall(mName,qDataStream);
                Bzi::marshall(mValue,qDataStream);

            }

            void unmarshall(QDataStream &qDataStream)
            {
                Bzi::unmarshall(mName,qDataStream);
                Bzi::unmarshall(mValue,qDataStream);
            }

        private:

            void _assign(const InitializingParameter &initParam)
            {
                mName = initParam.mName;
                mValue = initParam.mValue;
            }

    };

    typedef std::map<std::string,InitializingParameter> InitParameterMap;
}

#endif//INITIALIZING_PARAMETER_H	
