/*
* File: controller.h	
* Description: class defining a generic controller.
*			   Controller objects are used to communicate with Bzi internals
*			   from external applications such as GUIs. 
* Date: March 11st 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#pragma once

#ifndef CONTROLLER_H
#define CONTROLLER_H

//BZI includes
#include "core/module/realTimeModule.h"
#include "core/configuration/configurationMap.h"

//QT includes
#include <QFuture>
#include <QApplication>

//STD includes
#include <string>
#include <map>

namespace Bzi
{
	class LIBRARY_API Controller: public RealTimeModule
	{
		Q_OBJECT

		public:

			enum
			{
                DEFAULT_NET_TIMEOUT = 32 //milisecs
			};

            Controller(const std::string& name = "Controller");
			~Controller();

			inline std::string managerName(){return _mManagerName;}

		public slots:
			virtual bool networkConnect(const std::string &host, unsigned int tcpPort);

			void run();
			virtual void configureSystem(const Configuration &config);
			
			void resetSystemConfiguration();
			void resetModuleConfiguration(const std::string &moduleName);
			
			void sendModulesActivationStates(std::vector<std::string> modulesToActivate,std::vector<std::string> modulesToDeactivate);

            bool sendStart(); //start processing
            bool sendStop(); //stop porcessing (pause)
            virtual bool sendFinish(); //reset and finish everything
			bool sendFinish(const std::vector<std::string> &modulesToFinish); // note manager will not finish in this case
			
			bool receive(int milisecs);
		public:
            std::vector<Data*> getRefreshedData();
            bool refreshedContains(const std::string &dataId);

			template<class T>
			bool getData(const std::string &dataId,T &outData)
			{
			    if(_mpRepo->exist(dataId))
				if(dynamic_cast<T*>(_mpRepo->get(dataId)))
				{
				    outData = *dynamic_cast<T*>(_mpRepo->get(dataId));
				    return true;
				}

			    return false;
			}
		
		protected:
			//redefined
			virtual void _handleConfigurationMessage();
			virtual void _handleResetConfigurationMessage();
			void	_handleNetworkMessage(const ReceptionIdentifier &reception);

        protected:
            std::string _mManagerName;

		private:
			void _initialize();
			void _insertRepoInfoInMapvector(std::map<std::string, std::vector<Data*> > &result, std::map<std::string, Data*> &repoInfo);
	};
}
#endif //CONTROLLER_H
