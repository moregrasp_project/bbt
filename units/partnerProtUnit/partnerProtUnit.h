#ifndef PARTNER_PROT_UNIT_H
#define PARTNER_PROT_UNIT_H

#include "core/units/genericUnit.h"

namespace Bzi
{
    namespace Units
    {
        class PartnerProtUnit: public Bzi::GenericUnit
        {
            public:
                PartnerProtUnit();
                ~PartnerProtUnit();

                void initialize();
                void process();
                void reset();

            private:
                double          _cycle_time;    // in ms
                int             _time2event;

                int             _cycles2event;
                int             _counter_cycles;
                int             _counter_events;

                EventStream     *_mpOutputDummyEvent;
                EventStream     *_mpOutputResetEvent;
        };
    }
} // end namespace

#endif //PARTNER_PROT_UNIT_H
