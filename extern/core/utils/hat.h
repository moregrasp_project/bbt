/*
* File: precisionTime.h	
* Description: class wrapper for a high accuracy timer (win and linux)
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef HAT_H
#define HAT_H

#ifdef _WIN32
    #include <winsock2.h>
    #include <windows.h>
#else
    #include <sys/time.h>
#endif

#include <QtGlobal>
#include <time.h>
#include <iostream>
#include "core/utils/libHeader.h"

namespace Bzi
{

    class LIBRARY_API Hat
    {
        public:
            Hat();
            ~Hat() {}

            qint64 get() const;
        private:
#ifdef _WIN32
    #ifndef UTC_T
            LARGE_INTEGER _frequency;
    #endif
#else
            struct timespec _prectime;
#endif
    };
}

#endif // HAT_H
