#include "testclient.h"

#include <QByteArray>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QStatusBar>
#include <QDateTime>

#include <QThread>

#include "sockets/streamSocket.h"

#include <iostream>

TestClient::TestClient(QWidget *parent) :
    QWidget(parent)
{

    std::cout << "Create GUI" << std::endl;
    //server box
    _serverLine = new QLineEdit("localhost");
    _portLine = new QLineEdit("5555");
    _connectButton = new QPushButton("Connect");

    _connectBoxLayout = new QHBoxLayout;
    _connectBoxLayout->addWidget(_serverLine);
    _connectBoxLayout->addWidget(_portLine);
    _connectBoxLayout->addWidget(_connectButton);

    _connectBox = new QGroupBox("Server");
    _connectBox->setLayout(_connectBoxLayout);

    //load box
    _operationModeLine = new QLineEdit;
    _operationModeLine->setText("EEG status");
    _loadButton = new QPushButton("Load");
    _loadBoxLayout = new QHBoxLayout;

    _loadBoxLayout->addWidget(_operationModeLine);
    _loadBoxLayout->addWidget(_loadButton);

    _loadBox = new QGroupBox("Operation mode");
    _loadBox->setLayout(_loadBoxLayout);
    _loadBox->setEnabled(false);

    //config box
    _requestConfigButton = new QPushButton("Request configuration");
    _configTextEdit = new QPlainTextEdit;
    _sendConfigButton = new QPushButton("Upload configuration");

    _configBoxLayout = new QVBoxLayout;
    _configBoxLayout->addWidget(_requestConfigButton);
    _configBoxLayout->addWidget(_configTextEdit);
    _configBoxLayout->addWidget(_sendConfigButton);

    _configBox = new QGroupBox("Configuration");
    _configBox->setLayout(_configBoxLayout);
    _configBox->setEnabled(false);

    //run box
    _runButton = new QPushButton("Start");

    _runBoxLayout = new QHBoxLayout;
    _runBoxLayout->addWidget(_runButton);

    _runBox = new QGroupBox("Run");
    _runBox->setLayout(_runBoxLayout);
    _runBox->setEnabled(false);

    //event box
    _eventTextEdit = new QPlainTextEdit;
    _eventTextEdit->setPlainText("[{\"ID\": \"tablet_event\", \"Values\": [6]}]");
    _sendEventButton = new QPushButton("Send Event");

    _eventBoxLayout = new QVBoxLayout;
    _eventBoxLayout->addWidget(_eventTextEdit);
    _eventBoxLayout->addWidget(_sendEventButton);

    _eventBox = new QGroupBox("Events");
    _eventBox->setLayout(_eventBoxLayout);
    _eventBox->setEnabled(false);


    //main window
    _statusBar = new QStatusBar;
    _mainLayout = new QVBoxLayout;
    _mainLayout->addWidget(_connectBox);
    _mainLayout->addWidget(_loadBox);
    _mainLayout->addWidget(_configBox);
    _mainLayout->addWidget(_runBox);
    _mainLayout->addWidget(_eventBox);
    _mainLayout->addWidget(_statusBar);

    setLayout(_mainLayout);
    setWindowTitle("Test Client");

    _connected = false;
    _loaded = false;
    _running = false;

    std::cout << "Connect slots" << std::endl;
    connect(_connectButton, &QPushButton::clicked, this, &TestClient::onConnect);
    connect(_loadButton, &QPushButton::clicked, this, &TestClient::onLoad);
    connect(_requestConfigButton, &QPushButton::clicked, this, &TestClient::onRequestConfig);
    connect(_runButton, &QPushButton::clicked, this, &TestClient::onRun);
    connect(_sendConfigButton, &QPushButton::clicked, this, &TestClient::onSendConfig);
    connect(_sendEventButton, &QPushButton::clicked, this, &TestClient::onSendEvent);

}

TestClient::~TestClient()
{
    abort();
    //if (_running)
    //        stop();

    delete _serverLine;
    delete _portLine;
    delete _connectButton;
    delete _connectBoxLayout;
    delete _connectBox;

    delete _operationModeLine;
    delete _loadButton;
    delete _loadBoxLayout;
    delete _loadBox;

    delete _requestConfigButton;
    delete _sendConfigButton;
    delete _configTextEdit;
    delete _configBoxLayout;
    delete _configBox;

    delete _runButton;
    delete _runBoxLayout;
    delete _runBox;

    delete _eventTextEdit;
    delete _sendEventButton;
    delete _eventBoxLayout;
    delete _eventBox;

    delete _statusBar;
    delete _mainLayout;
}

void TestClient::abort()
{
    _abort = true;
}

void TestClient::onConnect()
{
    if (not _connected)
    {
        try
        {
            _server.Init();
            _server.Connect(_serverLine->text().toStdString(), _portLine->text().toInt());
            if (_server.Connected())
            {
                _connectBox->setEnabled(false);
                _loadBox->setEnabled(true);
                _connected = true;
                _receiverThread = new Receiver(&_server, 5000);

                std::cout << "connect" << std::endl;
                connect(_receiverThread, &Receiver::pendingMessage, this, &TestClient::onNewMessage);

                std::cout << "start" << std::endl;
                _receiverThread->start();
                _statusBar->showMessage("Connected");


                std::ostringstream oss;
                oss << "C:/data/moregrasp_client_" << QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss").toStdString() << ".log";
                _log_file.open(oss.str().c_str());
            }

        }
        catch (const Bzi::SocketException& ex)
        {
            std::cout << "Unable to connect to " << _serverLine->text().toStdString() << ":" << _portLine->text().toStdString() << std::endl;
            _statusBar->showMessage("Unable to connect");
        }
        catch (const Bzi::SocketTimeoutException& ex)
        {
            ex.debugBzi(std::cout);
        }
    }
}

void TestClient::onLoad()
{
    std::string msg = "{\"LOAD\": \"" + _operationModeLine->text().toStdString() +"\"}";
    _log_file << "SEND: " << msg << std::endl;
    _server.Write(const_cast<char*>(msg.data()), msg.length());
}

void TestClient::onSendConfig()
{
    std::string msg = "{\"CONFIG\": " + _configTextEdit->toPlainText().replace("\n"," ").simplified().toStdString() + "}";
    _log_file << "SEND: " << msg << std::endl;
    _server.Write(const_cast<char*>(msg.data()), msg.length());
}

void TestClient::onRequestConfig()
{
    std::string msg = "{\"COMMAND\": \"config request\"}";
    _log_file << "SEND: " << msg << std::endl;
    _server.Write(const_cast<char*>(msg.data()), msg.length());
}

void TestClient::onRun()
{
    if (_running)
        stop();
    else
        start();
}

void TestClient::onSendEvent()
{
    std::string msg = "{\"DATA\": " + _eventTextEdit->toPlainText().replace("\n"," ").simplified().toStdString() + "}";
    _log_file << "SEND: " << msg << std::endl;
    _server.Write(const_cast<char*>(msg.data()), msg.length());
}

bool TestClient::start()
{
    std::string msg = "{\"COMMAND\": \"start\"}";
    _log_file << "SEND: " << msg << std::endl;
    _server.Write(const_cast<char*>(msg.data()), msg.length());

    return true;
}

bool TestClient::stop()
{
    std::string msg = "{\"COMMAND\": \"stop\"}";
    _log_file << "SEND: " << msg << std::endl;
    _server.Write(const_cast<char*>(msg.data()), msg.length());
    return true;
}

void TestClient::onNewMessage(const QJsonObject& msg)
{
    QJsonDocument doc(msg);

    _log_file << "RECEIVED: " << doc.toJson(QJsonDocument::Compact).toStdString() << std::endl;

    if (msg.isEmpty())
    {
        std::cout << "[CLIENT] Error: bad message format" << std::endl;
        return;
    }

    for (QJsonObject::const_iterator entry = msg.constBegin(); entry != msg.constEnd(); ++entry)
    {
        if (entry.key() == "CONFIG")
        {
            QJsonDocument config;
            std::cout << "[CLIENT] Configuration received" << std::endl;
            if (entry.value().isArray())
            {
                config.setArray(entry.value().toArray());
                _configTextEdit->setPlainText(config.toJson());
                _statusBar->showMessage("Configuration received");
            }
            else if (entry.value().isObject())
            {
                config.setObject(entry.value().toObject());
                _configTextEdit->setPlainText(config.toJson());
                _statusBar->showMessage("Configuration received");
            }
            else
            {
                std::cout << "[CLIENT] Error: bad config format. Expected object or array." << std::endl;
            }
        }
        else if (entry.key() == "DATA")
        {
            if (entry.value().isArray())
            {
                QJsonArray data = entry.value().toArray();
                for (QJsonArray::const_iterator value = data.begin(); value != data.end(); ++value)
                {
                    QJsonObject data_value = value->toObject();
                    if (data_value.contains("ID") and data_value.value("ID").isString())
                    {
                        QString id = data_value.value("ID").toString();

                        std::cout << "[Client] Received " << id.toStdString() << std::endl;

                        if (id == "EVENT_RESET")
                        {
                            _runButton->setText("Start");
                            _loadBox->setEnabled(true);
                            _configTextEdit->clear();
                            _configBox->setEnabled(false);
                            _runBox->setEnabled(false);
                            _eventBox->setEnabled(false);
                            _running = false;
                            _loaded = false;
                            _statusBar->showMessage("Reset");
                        }
                    }
                }
            }
            else
            {
                std::cout << "[CLIENT] Error: DATA entry must be an array" << std::endl;
            }
        }
        else if (entry.key() == "STATUS")
        {
            if (msg["STATUS"].isString())
            {
                QString status = msg["STATUS"].toString();

                if (status == "OK")
                {
                    std::cout << "[CLIENT] REPLY OK" << std::endl;
                    QString operation = msg.value("OPERATION").toString();
                    if (operation == "start")
                    {
                        _running = true;

                        _loadBox->setEnabled(false);
                        _configBox->setEnabled(false);
                        _eventBox->setEnabled(true);

                        _runButton->setText("Stop");
                        _statusBar->showMessage("Running");

                    }
                    else if (operation == "stop")
                    {
                        _running = false;
                        _loaded = false;

                        _loadBox->setEnabled(true);
                        _configTextEdit->clear();
                        _configBox->setEnabled(false);
                        _runBox->setEnabled(false);
                        _eventBox->setEnabled(false);

                        _runButton->setText("Start");
                        _statusBar->showMessage("Stopped");
                    }
                    else if (operation == "LOAD")
                    {
                        _loaded = true;
                        _configBox->setEnabled(true);
                        _requestConfigButton->setEnabled(true);
                        _sendConfigButton->setEnabled(true);
                        _runBox->setEnabled(true);
                        _statusBar->showMessage("Operation mode loaded");
                    }
                }
                else if (status.toLower() == "error")
                {
                    if (msg.contains("MSG") and msg["MSG"].isString())
                        std::cout << "[CLIENT] REPLY ERROR MSG: " << msg["MSG"].toString().toStdString() << std::endl;
                    else
                        std::cout << "[CLIENT] GENERIC REPLY ERROR" << std::endl;

                }
                else
                {
                    std::cout << "[CLIENT] Error: unkwown status " << status.toStdString() << std::endl;
                }
            }
            else
            {
                std::cout << "[CLIENT] Error: bad reply format" << std::endl;
            }
        }

    }
}
