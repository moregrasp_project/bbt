#ifndef PARTNER_PROC_UNIT_H
#define PARTNER_PROC_UNIT_H

#include "core/units/genericUnit.h"

namespace Bzi
{
    namespace Units
    {
        class PartnerProcUnit: public Bzi::GenericUnit
        {
            public:
                PartnerProcUnit();
                ~PartnerProcUnit();

                void initialize();
                void process();
                void reset();

            private:
                int             _time2sleep;
                int             _tot_blocks;
                bool            _simulate_load;

                SignalStream    *_mpInputStream;
                SignalStream    *_mpOutputStream;
                EventStream     *_mpInputEvent;
        };
    }
} // end namespace

#endif //PARTNER_PROC_UNIT_H
