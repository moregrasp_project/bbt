/*
* File: configuration.h	
* Description: class defining configuration data
*			   as a container of Data (configuration parameters)
*			   it is unserializable from xml file.
* Date: May 25th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
          BitBrain Technologies
*/

#pragma once

#ifndef CONFIGURATION_H
#define CONFIGURATION_H	

#include "core/data/data.h"
#include "core/units/unitInfo.h"

#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamAttributes>

#include <map>

namespace Bzi
{
    class LIBRARY_API Parameter: public Data  //note input and output are considered parameters
    {
        public:
            Parameter();
            Parameter(const std::string &name, const std::string &type, const std::string &value);

            //DebuggingInterface
            std::ostream& debugBzi(std::ostream &os) const;

            //MarshallingInterface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //ClonningInterface
            Parameter* clone();

            //xml interface
            void unserialize(QXmlStreamReader &xmlStreamReader);
            void serialize(QXmlStreamWriter &xmlStreamWriter,const QString &xmlElementName);

            std::string name() const;
            void name(const std::string &newName);

            std::string type() const;
            void type(const std::string &newType);

            std::string value() const;
            void value(const std::string &newValue);

            const InitParameterMap& initializingParameters();
            void initializingParameters(std::vector<InitializingParameter> &initParams);
            void initializingParameters(const InitParameterMap &initParams);


        private:
            std::string	_mName,
            _mType,
            _mValue;

            InitParameterMap _mInitParamsMap;

            void _assign(const Parameter &param);
    };

    typedef std::map<std::string, Parameter> ParameterMap; //key=paramName, value=Parameter

    class LIBRARY_API UnitConfiguration: public Data
    {
        public:
            UnitConfiguration();
            UnitConfiguration(const UnitConfiguration &unitConfig);

            UnitConfiguration& operator=(const UnitConfiguration &unitConf);

            //DebuggingInterface
            std::ostream& debugBzi(std::ostream &os) const;

            //MarshallingInterface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //ClonningInterface
            UnitConfiguration* clone();

            //xml interface
            void unserialize(QXmlStreamReader &xmlStreamReader);
            void serialize(QXmlStreamWriter &xmlStreamWriter);

            std::string name() const;
            void name(const std::string &newName);

            std::string typeClass() const;
            void typeClass(const std::string& newClass);

            std::string xml() const;
            void xml(const std::string& newXml);

            std::string typeName() const ;
            UnitInfo::UnitType type() const;
            void type(UnitInfo::UnitType newType);

            void addInput(Parameter& inputConfig);
            void addInput(const std::string &name, const std::string &typeName,const std::string &value="",std::vector<InitializingParameter> initParams=_emptyInitParamVector());

            void addOutput(Parameter& outputConfig);
            void addOutput(const std::string &name, const std::string &typeName,const std::string &value="",std::vector<InitializingParameter> initParams=_emptyInitParamVector());

            void addParameter(Parameter& parameterConfig);
            void addParameter(const std::string &name, const std::string &typeName,const std::string &value="",std::vector<InitializingParameter> initParams=_emptyInitParamVector());

            bool existInput(const std::string &inputName) const;
            bool existOutput(const std::string &outputName) const;
            bool existParameter(const std::string &parameterName) const;

            Parameter& input(const std::string &inputName);
            const Parameter& input(const std::string &inputName) const;
            Parameter& output(const std::string &outputName);
            const Parameter& output(const std::string &outputName)const;
            Parameter& parameter(const std::string &parameterName);
            const Parameter& parameter(const std::string &parameterName)const;

            std::vector<Parameter> inputs() const;
            std::vector<Parameter> outputs() const;
            std::vector<Parameter> parameters() const;

            std::vector<std::string> inputNames() const;
            std::vector<std::string> outputNames() const;
            std::vector<std::string> parameterNames() const;

            bool merge(const UnitConfiguration& unitConfig);
        private:
            static std::vector<InitializingParameter> _emptyInitParamVector()
            {
                return std::vector<InitializingParameter>();
            }

            std::string _mName; //it corresponds with unit class name
            std::string _mTypeClass;
            std::string _mTypeName; //type of units: GenericUnit, DisplayUnit, DisplayUnitSynchro
            std::string _mXml;

            ParameterMap _mInputs,
            _mOutputs,
            _mParameters;

            void _assign(const UnitConfiguration &modConfig);

    };

    typedef std::map<std::string, UnitConfiguration> UnitMap; //key=unitName, value=UnitConfiguration

    class LIBRARY_API ModuleConfiguration: public Data
    {
        public:

            enum OperationMode
            {
                ONLINE_SYNC = 0,    // online synchronous
                ONLINE_ASYNC        // online asynchronous
            };

            ModuleConfiguration();
            ~ModuleConfiguration();

            //DebuggingInterface
            std::ostream& debugBzi(std::ostream &os) const;

            //MarshallingInterface
            virtual void marshall(QDataStream &qDataStream);
            virtual void unmarshall(QDataStream &qDataStream);

            //ClonningInterface
            ModuleConfiguration* clone();

            //xml interface
            virtual void unserialize(QXmlStreamReader &xmlStreamReader);
            virtual void serialize(QXmlStreamWriter &xmlStreamWriter);

            std::string name() const;
            void name(const std::string &name){_mName = name;}
            bool activated() const;
            inline void activated(const bool &activate) { _mActivationState = activate; }

            bool existUnit(const std::string &unitName) const;
            bool existUnitOfType(UnitInfo::UnitType type) const;
            const std::vector<UnitConfiguration>& units() const ;

            UnitConfiguration* unit(const std::string &unitName);
            const UnitConfiguration* unit(const std::string &unitName)const;

            void addUnit(UnitConfiguration &unitConfig, int pos =-1);
            void removeUnit(const std::string &unitName);
            bool setUnit(const std::string &unitName, UnitConfiguration &newUnitConfig);

            const std::vector<Parameter>& inputs() const;
            const std::vector<Parameter>& outputs() const;
            const std::vector<Parameter>& parameters() const;

            std::vector<Parameter>& inputs();
            std::vector<Parameter>& outputs();
            std::vector<Parameter>& parameters();

            std::vector<std::string> inputNames() const;
            std::vector<std::string> outputNames() const;
            std::vector<std::string> parameterNames() const;

            inline int stage() { return _mStage;}
            inline void stage(int stage){_mStage = stage;}

            inline std::string exe()const { return _mExe;}
            inline void exe(const std::string &exeFile){_mExe = exeFile;}

            inline std::string sshUser() const {return _mSshUser;}
            inline void sshUser(const std::string &sshUser){_mSshUser = sshUser;}

            unsigned int tcpPort() const ;
            inline std::string tcpPortStr() const { return _mTcpPort;}
            inline void tcpPort(int tcpPort){_mTcpPort = QString::number(tcpPort).toStdString();}

            inline std::string ip() const { return _mIp; }
            inline void ip(const std::string &ip){_mIp = ip;}

            inline std::string bufferSize() const {return _mBufferSize;}
            inline void bufferSize(const std::string &bufferSize){_mBufferSize = bufferSize;}

            inline OperationMode operation()const {return _mOperation;}
            inline void operation(OperationMode operation){_mOperation = operation; }

            inline bool synchronousDisplay()const {return _mSynchronousDisplay;}
            inline void synchronousDisplay(bool synchDisp){_mSynchronousDisplay = synchDisp; }

            inline bool startDetached()const {return true;}

            bool existParameter(const std::string& parameterName, const std::string& unitName);
            void updateParameter(const std::string& parameterName, const std::string& parameterValue, const std::string& unitName);
            const Parameter& parameter(const std::string &parameterName, const std::string& unitName) const;

        protected:
            std::string	_mName;
            std::string	_mIp,
            _mTcpPort,
            _mExe,
            _mSshUser,
            _mBufferSize;

            bool _mActivationState,
            _mSynchronousDisplay;

            int _mStage;
            OperationMode _mOperation;

            std::vector<UnitConfiguration> _mUnitsOrdered; //units in order as in pipe

            //union of all units inputs/outputs/parameters (duplicated avoided)
            std::vector<Parameter> _mInputs,
            _mOutputs,
            _mParameters;

            //void _assign(const ModuleConfiguration &modConfig);
            void _addUnitParameters(const UnitConfiguration &unitConfig );
            void _removeUnitParameters(const UnitConfiguration &unitConfig );
            void _replaceUnitParameters(const UnitConfiguration &unitConfig );
            bool _existInVector(const std::vector<Parameter> &paramVect, const std::string &paramName);
            void _replaceInVector(std::vector<Parameter> &outParamVect, const std::vector<Parameter> &inParamVect, const std::string &paramName);

            void _unserializeAttributes(QXmlStreamReader &xmlStreamReader);
            void _serializeAttributes(QXmlStreamWriter &xmlStreamWriter);

        private:
            void _finalize();
    };

    class LIBRARY_API ModuleMap:public std::map<std::string, ModuleConfiguration>  //key=moduleName, value=ModuleConfiguration
    {
        public:
            std::list<std::string> names() const;
    };


    class LIBRARY_API ControllerConfiguration: public ModuleConfiguration
    {
        public:
            ControllerConfiguration();

            //DebuggingInterface
            std::ostream& debugBzi(std::ostream &os) const;

            //MarshallingInterface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //ClonningInterface
            ControllerConfiguration* clone();

            //xml interface
            void unserialize(QXmlStreamReader &xmlStreamReader);
            void serialize(QXmlStreamWriter &xmlStreamWriter);

            void addInput(Parameter& inputConfig);
            void addInput(const std::string &name, const std::string &typeName,const std::string &value="",std::vector<InitializingParameter> initParams=std::vector<InitializingParameter>());

            void addOutput(Parameter& outputConfig);
            void addOutput(const std::string &name, const std::string &typeName,const std::string &value="",std::vector<InitializingParameter> initParams=std::vector<InitializingParameter>());

            inline int netConnectionTries() const { return _mNetConnectionTries;}
        private:
            int _mNetConnectionTries;


    };

    class LIBRARY_API ManagerConfiguration: public ModuleConfiguration
    {
        public:
            ManagerConfiguration();

            //DebuggingInterface
            std::ostream& debugBzi(std::ostream &os) const;

            //MarshallingInterface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //ClonningInterface
            ManagerConfiguration* clone();

            //xml interface
            void unserialize(QXmlStreamReader &xmlStreamReader);
            void serialize(QXmlStreamWriter &xmlStreamWriter);

            inline int netConnectionTries() const { return _mNetConnectionTries;}

            // ESCOLANO_XXX
            // manager is allow to contain "global" parameters to all the modules
            void addGlobalParameter(Parameter& parameterConfig);
            void addGlobalParameter(const std::string &name, const std::string &typeName,const std::string &value="",std::vector<InitializingParameter> initParams=_emptyInitParamVector());

            Parameter& globalParameter(const std::string &parameterName);
            const Parameter& globalParameter(const std::string &parameterName)const;

            std::vector<Parameter> globalParameters() const;
            std::vector<std::string> globalParameterNames() const;
        private:
            int	_mNetConnectionTries;

            static std::vector<InitializingParameter> _emptyInitParamVector()
            {
                return std::vector<InitializingParameter>();
            }
            ParameterMap _mGlobalParameters;

            void _assign(const ManagerConfiguration &controllerConfig);
    };


    class LIBRARY_API Configuration: public Data
    {
        public:
            Configuration();

            //DebuggingInterface
            std::ostream& debugBzi(std::ostream &os) const;

            //MarshallingInterface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //ClonningInterface
            Configuration* clone();

            //xml interface
            void unserialize(QXmlStreamReader &xmlStreamReader);
            void serialize(QXmlStreamWriter &xmlStreamWriter);

            std::string name() const;
            void setName(const std::string &configName);

            void addController(ControllerConfiguration &controllerConfig);
            ControllerConfiguration* controller();
            const ControllerConfiguration* controller() const;

            void addManager(ManagerConfiguration &managerConfig);
            ManagerConfiguration* manager();
            const ManagerConfiguration* manager() const;

            //note manager and controller are always preset in xml file
            //this is just to ask for general modules
            /**
             * @brief existModule Check whether a module exists on the configuration
             * @param moduleName the name of the module
             * @return true if the module exists
             */
            bool existModule(const std::string &moduleName) const;

            ModuleMap modules() const;
            ModuleMap& modules();
            ModuleMap modules(ModuleConfiguration::OperationMode mode) const;
            ModuleMap modules_by_stage(unsigned int stage) const;
            ModuleMap modules_by_stage(unsigned int stage, ModuleConfiguration::OperationMode mode) const;

            void addRTModule(ModuleConfiguration &moduleConfig);
            int totalStages();
            ModuleConfiguration* getModuleConfiguration(const std::string &moduleName);

            bool checkStr(const std::string&)
            {
                std::cout << "Configuration::checkStr()"<<std::endl;
                return true;
            }

            bool existConfiguration(const std::string &moduleName) const;

            //note if module is not already present nothing is done and false is returned
            bool replaceRtModConfiguration(const std::string &moduleName, ModuleConfiguration &newModConfig );

            void replaceController(const ControllerConfiguration &newControllerConfig);
            void replaceManager(const ManagerConfiguration &newManagerConfig);

            void merge(Configuration& config);

            std::vector<std::string> inputNames(const std::string& unitName) const;
            std::vector<std::string> outputNames(const std::string& unitName) const;
            std::vector<std::string> parameterNames(const std::string& unitName) const;

            bool existParameter(const std::string& parameterName, const std::string& unitName = "");
            void updateParameter(const std::string &parameterName, const std::string &parameterValue, const std::string& unitName = "");
            const Parameter& parameter(const std::string &parameterName, const std::string& unitName = "") const;

            std::vector<Parameter> inputs() const; //union of all inputs of every unit
            std::vector<Parameter> outputs() const; //union of all outputs of every unit
            std::vector<Parameter> parameters() const; //union of all parameters of every unit

            static const QString XML_ROOT_ELEMENT;

        private:
            std::string	_mName;

            ControllerConfiguration _mControllerConfig;
            ManagerConfiguration	_mManagerConfig;
            ModuleMap _mModules;

            std::vector< std::vector<std::string> > _modulesPerStage;

            void _assign(const Configuration &config);

    };
}
#endif //CONFIGURATION_H	
