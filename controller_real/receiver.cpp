#include "receiver.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QVariant>
#include <QDateTime>
#include <sstream>
#include <QMutexLocker>

Receiver::Receiver(Bzi::StreamSocket* const socket, unsigned int timeout):
    _reader(socket, timeout),
    _stop(false)
{
    socket->SetNoDelay(true);
}

Receiver::~Receiver()
{
    stop();
}


void Receiver::run()
{
    std::cout << "start receiving" << std::endl;
    _stop = false;

    while (true)
    {
        {
            QMutexLocker lock(&_mutex);
            if (_stop)
                break;
        }

        try
        {
            QJsonObject object = _reader.readMessage();
            emit pendingMessage(object);
        }
        catch (JsonStreamReader::Timeout& e)
        {
            continue;
        }
        catch (JsonStreamReader::Disconnected& e)
        {
            std::cout << e.what() << std::endl;
            break;
        }
    }
}

void Receiver::stop()
{
    _stop = true;
}
