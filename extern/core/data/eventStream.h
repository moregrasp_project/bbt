/*
* File: eventStream.h
* Description: class defining the properties of an event stream.
* Date: -
* Author: BitBrain Technologies
*/
#pragma once

#ifndef EVENT_STREAM_H
#define EVENT_STREAM_H

#include "core/data/dataStream.h"
#include "core/data/eventProperties.h"

namespace Bzi
{
    class LIBRARY_API EventBlock: public DataBlock
    {
        public:
            EventBlock();
            EventBlock(const EventProperties& p);
            //EventBlock(int rows, int cols, const std::vector<DataBlock::ValueType>& initValues);
            EventBlock(const EventBlock& data); // copy constructor used for clonning
            virtual ~EventBlock() {}

            DataBlock& operator=(const DataBlock& data);
            EventBlock& operator=(const EventBlock& data);

            //cloning interface
            Data *clone();

            //attributes
            void    setTimestamp(qint64 stamp) {_timestamp = stamp;}
            qint64  timestamp() const {return _timestamp;}

            //accesors
            std::vector<DataBlock::ValueType> initValues() const
            { return static_cast<EventProperties*>(_properties)->initValues(); }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //marshalling interface
            void marshall(QDataStream &qDataStream);
            void unmarshall(QDataStream &qDataStream);

            //file interface
            void toFileStream(QDataStream &qDataStream, const std::string& version);
            void fromFileStream(QDataStream &qDataStream, const std::string& version);

        private:
            void _assign(const EventBlock &data);

        private:
            qint64  _timestamp;
    };

    class LIBRARY_API EventStream: public DataStream
    {
        public:
            EventStream();
            EventStream(const EventProperties& p, int blocks);
            //EventStream(int rows, int cols, const std::vector<DataBlock::ValueType>& initValues, int blocks);
            EventStream(const EventStream& signal); // copy constructor used for clonning
            virtual ~EventStream() {}

            DataStream& operator=(const DataStream& stream);
            EventStream& operator=(const EventStream& signal);

            //cloning interface
            Data* clone();

            //values
            EventBlock* block(int block);

            //accesors
            std::vector<DataBlock::ValueType> initValues() const
            { return static_cast<EventProperties*>(_properties)->initValues(); }
            const EventProperties& prop() const
            { return *static_cast<EventProperties*>(_properties); }

            // debugging interface
            std::ostream& debugBzi(std::ostream& os) const;

            //configuration
            void initializeData(const InitParameterMap &initParams);

            //matlab conversion interface
#ifdef COMPILE_MATLAB
            void toMatlabMatrix(mxArray*& pMatlabArray);
            void fromMatlabMatrix(mxArray* pMatlabArray);
#endif
        private:
            void _resize(int blocks);
    };

    class LIBRARY_API EventStreamContainer: public DataStreamContainerT<EventStream, EventPropertiesContainer>
    {

    };
}

#endif //EVENT_STREAM_H
