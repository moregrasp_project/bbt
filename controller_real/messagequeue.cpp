#include "messagequeue.h"

#include <QMutexLocker>
#include <iostream>

MessageQueue::MessageQueue(unsigned long timeout):
    _timeout(timeout)
{

}

MessageQueue::~MessageQueue()
{
    _not_empty_condition.wakeAll();
}

bool MessageQueue::empty()
{
    QMutexLocker lock(&_mutex);
    return _queue.empty();
}

void MessageQueue::enqueue(const QJsonObject& object)
{
    QMutexLocker lock(&_mutex);
    _queue.enqueue(object);
    _not_empty_condition.wakeAll();
    emit enqueued(object);

}

QJsonObject MessageQueue::dequeue()
{
    QMutexLocker lock(&_mutex);
    while (_queue.empty())
        if (not _not_empty_condition.wait(lock.mutex(), _timeout))
            throw Timeout();

    return _queue.dequeue();

}

