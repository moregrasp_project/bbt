#include "moregraspController.h"
#include <iostream>
#include <sstream>

#include "core/data/signalStream.h"
#include "core/data/eventStream.h"
#include "core/utils/sysUtils.h"

using namespace std;
using namespace Bzi;

MoregraspController::MoregraspController(const string& name)
    : Bzi::Controller(name),
      _configured(false),
      _config_id("")
{
}

void MoregraspController::_handleDataMessage(const Bzi::ReceptionIdentifier &reception)
{
    DataStream *data;

    RealTimeModule::_handleDataMessage(reception);
    vector<Data*> dataVector = getRefreshedData();

    for (unsigned int i=0; i<dataVector.size(); i++)
    {
        if ((data = dynamic_cast<SignalStream*>(dataVector[i])) != NULL)
        {
            QString txt;
            txt.append("Signal ").append(data->getId().c_str()).append(" received!");
            emit sigStatus(txt);
        }
        else if ((data = dynamic_cast<EventStream*>(dataVector[i])) != NULL)
        {
            QString txt;
            txt.append("Event ").append(data->getId().c_str()).append(" received!");
            emit sigStatus(txt);

            if (data->getId() == "EVENT_RESET")
                onReset();
        }
    }
}

void MoregraspController::onLoadConfig(QString filename)
{
    _configsMap.setCurrentDir(SysUtils::env_resolve("BZI_FRAMEWORK_ROOT")+"/config");
    _configsMap.unserialize(filename.toStdString());

    std::vector<std::string> names = _configsMap.names();
    QStringList listNames;
    for (size_t i = 0; i < names.size(); i++) {
        listNames.append(QString::fromStdString(names[i]));
    }
    emit sigLoadedConfig(listNames);
}

void MoregraspController::onLaunch(QString name, QString params)
{
    Bzi::Configuration config = _configsMap.get(name.toStdString());

    // update config with tablet parameters
    QStringList pairs = params.split("\n");
    for (size_t i=0; i<pairs.size(); i++)
    {
        if (!pairs.at(i).isEmpty())
        {
            QStringList split = pairs.at(i).split("#");
            if (split.size() != 3)
            {
                cout << "Use format: unit_name#param_name#value" << endl;
                return;
            }
            else
            {
                std::string unit_name   = split.at(0).toStdString();
                std::string param_name  = split.at(1).toStdString();
                std::string param_value = split.at(2).toStdString();
                if (config.existParameter(param_name, unit_name))
                {
                    cout << "unit_name " << unit_name << ", param_name " << param_name << ", value " << param_value << endl;
                    config.updateParameter(param_name, param_value, unit_name);
                }
                else
                {
                    cout << "unit_name " << unit_name << ", param_name " << param_name << " does not exist" << endl;
                    return;
                }
            }
        }
    }

    // launch it
    if (!_configured) {
        Bzi::LauncherDescriptor launchDesc(config);
        Bzi::ModuleLauncher launcher;
        launcher.launchRealTimeModules(launchDesc);
        this->thread()->msleep(1000);
        launcher.launchManager(launchDesc);
        this->thread()->msleep(1000);
        networkConnect(launchDesc.managerLaunchProp().mIp.toStdString(),launchDesc.managerLaunchProp().mTcpPort.toInt());
        configureSystem(config);
        _configured = true;
        _config_id = config.name();
        emit sigLaunched(true);
    } else {
        configureSystem(config);
    }
}

void MoregraspController::onStart()
{
    sendStart();
}

void MoregraspController::onStop()
{
    sendStop();
    onReset();
}

void MoregraspController::onReset()
{
    //resetSystemConfiguration();
    sendFinish();
    _configured = false;
    emit sigReseted(true);
}

void MoregraspController::onFinish()
{
    finish();
}

void MoregraspController::onSendData(QString data)
{
    QStringList pairs = data.split("\n");
    for (size_t i=0; i<pairs.size(); i++)
    {
        if (!pairs.at(i).isEmpty())
        {
            QStringList split = pairs.at(i).split("#");
            if (split.size() != 2)
            {
                QString txt;
                txt.append("Incorrect format. Use: data_name#value");
                emit sigStatus(txt);
            }
            else
            {
                Bzi::EventStream event;
                std::string data_name   = split.at(0).toStdString();
                std::string data_value  = split.at(1).toStdString();

                if (!getData(data_name, event))
                {
                    QString txt;
                    txt.append("Event ").append(data_name.c_str()).append(" does not exist");
                    emit sigStatus(txt);
                }
                else
                {
                    bool sent = true;
                    try
                    {
                        event.setValueFromStr(data_value);
                        for (size_t b=0; b<event.size(); b++)
                            event.block(b)->setTimestamp(_clock.get());
                        sendData(&event,_mManagerName);
                    }
                    catch(Exception &e) //capture unit exceptions but go on in pipeline process!!
                    {
                        sent = false;
                    }

                    QString txt;
                    if (sent)
                        txt.append("Event ").append(data_name.c_str()).append(" sent!");
                    else
                        txt.append("Event ").append(data_name.c_str()).append(" could not be sent, dimensions mismatch!");
                    emit sigStatus(txt);
                }
            }
        }
    }
}

