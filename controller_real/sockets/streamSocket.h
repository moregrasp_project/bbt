/*! \class StreamSocket
    \brief A multi-platform wrapper to Berkeley sockets.

        A multi-platform wrapper to Berkeley sockets.
        A reliable, connection-oriented stream (TCP) socket.
        Only TCP is supported.
*/
#pragma once

#ifndef STREAM_SOCKET
#define STREAM_SOCKET


#include "socket.h"

namespace Bzi
{
    class StreamSocket : public Socket
    {

            friend class ServerSocket;

        public:

            /** Construct a new StreamSocket. */
            StreamSocket();

            /** Destructor. */
            ~StreamSocket() {}

            /** Initialize the socket. This method creates the underlying socket
                        * object.
                        *
                        * @throw SocketException If the socket could not be created.
                        */
            void Init() ;

            /** Connect the socket to a remote endpoint. This method blocks until a
                        * new connection is pending, unless a timeout has been set on the
                        * socket, in which case a TimeoutException is thrown if no
                        * connection is pending by the time the timeout expires.
                        *
                        * @param addr The address, which may either be a dot-separated IP address
                        * or a DNS name.
                        * @param port The port number.
                        * @throw TimeoutException If the operation times out.
                        * @throw SocketException If a socket error occurs.
                        */
            void Connect(const std::string& addr, int port) ;

            size_t Read(void *buffer, size_t buflen) ;
            size_t Write(void *buffer, size_t buflen) ;

            /** Shut down the socket. Closes the connection and destroys the
                        * underlying socket object.
                        */
            int Shutdown(int shutMode = SHUT_RDWR); //mode = { SHUT_RD = receive, SHUT_WR = send, SHUT_RDWR = both }

            /** Determine if the socket is connected. */
            inline bool Connected() const
            { return(mConnected); }

            std::string getRemoteHostName()
            {
                return inet_ntoa(mRaddr.sin_addr);
            }

            unsigned int getRemoteTcpPort()
            {
                return ntohs(mRaddr.sin_port);
            }

        private:
            void SetHandle(SocketHandle handle);

            bool mConnected;

            /** Remote address. */
            struct sockaddr_in mRaddr;
    };
}

#endif // STREAM_SOCKET
