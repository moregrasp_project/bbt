#pragma once

#ifndef SOCKET_DEFS
#define SOCKET_DEFS

#include <fcntl.h>
#include <cerrno>
#include <string.h>

#ifdef _WIN32
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <mswsock.h>
	
	#define INVALID_SOCKET_HANDLE	INVALID_SOCKET
	#define INADDR_NONE_HANDLE		INADDR_NONE
	#define SocketHandle			SOCKET
	#define socklen_t				int

	#ifdef FD_SETSIZE
	#undef FD_SETSIZE
	#endif
	#define FD_SETSIZE 1024

	#ifndef EINTR
	#define EINTR           WSAEINTR
	#endif
	#ifndef EACCES
	#define EACCES          WSAEACCES
	#endif
	#ifndef EFAULT
	#define EFAULT          WSAEFAULT
	#endif
	#ifndef EINVAL
	#define EINVAL          WSAEINVAL
	#endif
	#ifndef EMFILE
	#define EMFILE          WSAEMFILE
	#endif
	#ifndef EAGAIN
	#define EAGAIN          WSAEWOULDBLOCK
	#endif

    #undef EWOULDBLOCK
	#define EWOULDBLOCK     WSAEWOULDBLOCK

    #undef EINPROGRESS
	#define EINPROGRESS     WSAEINPROGRESS

    #undef EALREADY
	#define EALREADY        WSAEALREADY

    #undef ENOTSOCK
	#define ENOTSOCK        WSAENOTSOCK

    #undef EDESTADDRREQ
	#define EDESTADDRREQ    WSAEDESTADDRREQ

    #undef EMSGSIZE
    #define EMSGSIZE        WSAEMSGSIZE

    #undef EPROTOTYPE
    #define EPROTOTYPE      WSAEPROTOTYPE

    #undef ENOPROTOOPT
    #define ENOPROTOOPT     WSAENOPROTOOPT

    #undef EPROTONOSUPPORT
    #define EPROTONOSUPPORT WSAEPROTONOSUPPORT

    #undef ESOCKTNOSUPPORT
    #define ESOCKTNOSUPPORT WSAESOCKTNOSUPPORT

    #undef ENOTSUP
    #define ENOTSUP         WSAEOPNOTSUPP

    #undef EPFNOSUPPORT
    #define EPFNOSUPPORT    WSAEPFNOSUPPORT

    #undef EAFNOSUPPORT
    #define EAFNOSUPPORT    WSAEAFNOSUPPORT

    #undef EADDRINUSE
    #define EADDRINUSE      WSAEADDRINUSE

    #undef EADDRNOTAVAIL
    #define EADDRNOTAVAIL   WSAEADDRNOTAVAIL

    #undef ENETDOWN
    #define ENETDOWN        WSAENETDOWN

    #undef ENETUNREACH
    #define ENETUNREACH     WSAENETUNREACH

    #undef ENETRESET
    #define ENETRESET       WSAENETRESET

    #undef ECONNABORTED
    #define ECONNABORTED    WSAECONNABORTED

    #undef ECONNRESET
    #define ECONNRESET      WSAECONNRESET

    #undef ENOBUFS
    #define ENOBUFS         WSAENOBUFS

    #undef EISCONN
    #define EISCONN         WSAEISCONN

    #undef ENOTCONN
    #define ENOTCONN        WSAENOTCONN

    #undef ESHUTDOWN
    #define ESHUTDOWN       WSAESHUTDOWN

    #undef ETIMEDOUT
    #define ETIMEDOUT       WSAETIMEDOUT

    #undef ECONNREFUSED
    #define ECONNREFUSED    WSAECONNREFUSED

    #undef EHOSTDOWN
    #define EHOSTDOWN       WSAEHOSTDOWN

    #undef EHOSTUNREACH
    #define EHOSTUNREACH    WSAEHOSTUNREACH

    #undef ESYSNOTREADY
    #define ESYSNOTREADY    WSASYSNOTREADY

    #undef ENOTINIT
    #define ENOTINIT        WSANOTINITIALISED

    #undef HOST_NOT_FOUND
    #define HOST_NOT_FOUND  WSAHOST_NOT_FOUND

    #undef TRY_AGAIN
    #define TRY_AGAIN       WSATRY_AGAIN

    #undef NO_RECOVERY
    #define NO_RECOVERY     WSANO_RECOVERY

    #undef NO_DATA
    #define NO_DATA         WSANO_DATA

    #define SOCKET_EINTR	WSAEINTR
	#define SOCKET_errno	WSAGetLastError()

	#define EWOULDBLOCK		WSAEWOULDBLOCK
	#define EINPROGRESS		WSAEINPROGRESS
	#define ECONNABORTED	WSAECONNABORTED
	#define ECONNRESET		WSAECONNRESET
	#define ECONNREFUSED	WSAECONNREFUSED
	#define EADDRINUSE		WSAEADDRINUSE
	#define SHUT_RD			SD_RECEIVE
	#define SHUT_WR			SD_SEND
	#ifndef SHUT_RDWR
	#define SHUT_RDWR		SD_BOTH
	#endif

#else
	#include <netinet/in.h>
	#include <sys/socket.h>
	#include <sys/time.h>
	#include <unistd.h>
	#include <netinet/in.h>
	#include <netinet/tcp.h>
	#include <arpa/inet.h>
	#include <sys/select.h>
	#include <sys/ioctl.h>
	#include <netdb.h>

	#define INVALID_SOCKET_HANDLE	(-1)
	#define INADDR_NONE_HANDLE		(-1)
	#define SocketHandle			int

	#define SOCKET_EINTR	EINTR
	#define SOCKET_errno	errno

#endif

#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL 0
#endif // MSG_NOSIGNAL

#ifndef INADDR_NONE
#define INADDR_NONE ((in_addr_t)-1)
#endif // INADDR_NONE


#endif // SOCKET_DEFS
