/*
* File: repositoryInterface.h
	
* Description: interface defining methods for access to a data container (repository)
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef REPOSITORY_INTERFACE_H
#define REPOSITORY_INTERFACE_H

#include <map>
#include <vector>

#include "core/utils/libHeader.h"
#include "core/data/data.h"

namespace Bzi
{
	class LIBRARY_API RepositoryInterface: public DebuggingInterface,
										   public MatlabInterface
	{
		public:

			enum InOut
			{
				INPUT,
				OUTPUT,
				PARAMETER
			};

			enum ToDelete
			{
				RELEASE = true,
				NO_RELEASE = false
			};


			virtual ~RepositoryInterface(){ }

            virtual Data* get( const std::string &key ) = 0;
			virtual void put( Data* data, InOut type = INPUT, ToDelete released = RELEASE ) = 0;
            virtual void remove(const std::string &key) = 0;
            virtual const std::map<std::string,Data*>& getOutputData() = 0;
            virtual const std::map<std::string,Data*>& getParameterData() = 0;
            virtual bool isRefreshed(const std::string &key) = 0;

			virtual RepositoryInterface* clone() = 0;
			virtual void reset()=0;

            virtual void markAsRefreshed(const std::string &key) = 0;
            virtual void resetRefreshed(const std::string &key) = 0;
			virtual void resetRefreshed() = 0;
			virtual void resetModified() =0;
            virtual const std::vector<Data*> getRefreshed() = 0;
            virtual const std::map<std::string,Data*> getRefreshedMap(const std::string id="")=0;
            virtual bool exist(const std::string &dataId) =0;
	};
}
#endif //REPOSITORY_INTERFACE_H
