/* HIGH ACCURACY TIMER (WINDOWS)
 * mex -O mexhat.c
 */

#include "mex.h"
 
#if defined(_WIN32)
//Include the windows SDK header for handling time functions.
#include <windows.h>
__inline void hightimer(double *hHighTime, double *hLowTime)
{
    FARPROC fp;
    
    FILETIME time;
    
    HMODULE m = LoadLibraryW(L"kernel32.dll");
    fp = GetProcAddress(m, "GetSystemTimePreciseAsFileTime"); 
    if (fp != NULL) {
        (*fp)(&time);
        *hHighTime  = time.dwHighDateTime;
        *hLowTime   = time.dwLowDateTime;
    }
}
#endif

void mexFunction( int nlhs, mxArray *plhs[], int nrhs,
                  const mxArray *prhs[] )
{
    double hHighTime;
    double hLowTime;
    
    // check for proper number of arguments
    if ( nrhs != 0 ) {
        mexErrMsgTxt( "No arguments required." );
    }

    // do the actual computations in a subroutine
    hightimer(&hHighTime, &hLowTime);

    // create a matrix for the return argument
    plhs[0] = mxCreateDoubleScalar(hHighTime);
    plhs[1] = mxCreateDoubleScalar(hLowTime);

}
