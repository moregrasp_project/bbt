/*
* File: libHeader.h
	
* Description: Global header for library exportation and importation.
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef BZI__LIB_HEADER__H
#define BZI__LIB_HEADER__H


#ifdef _WIN32
#    ifdef LIBRARY_EXPORTS
#        undef LIBRARY_API
#		 define LIBRARY_API __declspec(dllexport)
#    endif
#    ifdef LIBRARY_IMPORTS
#        undef LIBRARY_API
#        define LIBRARY_API __declspec(dllimport)
#    endif
#	 ifndef LIBRARY_EXPORTS
#        ifndef LIBRARY_IMPORTS
#             define LIBRARY_API
#        endif
#    endif
#else
#    define LIBRARY_API
#endif

#endif //BZI__LIB_HEADER__H
