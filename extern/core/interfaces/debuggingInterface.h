/*
* File: debuggingInterface.h	
* Description: interface for debugging an object (put all its member values in a stream)
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef DEBUGGING_INTERFACE_H
#define DEBUGGING_INTERFACE_H

#include <string>
#include <iostream>

#include "core/utils/libHeader.h"

namespace Bzi
{
	
	class LIBRARY_API DebuggingInterface
	{
		public:
            virtual ~DebuggingInterface() {}
			virtual std::ostream& debugBzi(std::ostream &os) const =0;
	};

    inline std::ostream& operator<<(std::ostream &os, const DebuggingInterface &debugObj)
	{
		return debugObj.debugBzi(os);
	}

    inline std::ostream& operator<<(std::ostream &os, const DebuggingInterface *pDebugObj)
	{
		return ( (pDebugObj) ? pDebugObj->debugBzi(os): os << "pDebugObj: unknown (null pointer)" );
	}
}

#endif //DEBUGGING_INTERFACE_H

