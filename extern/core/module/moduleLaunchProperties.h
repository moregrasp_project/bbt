/*
* File: moduleLaunchProperties.h	
* Description: Properties to generate command line arguments
*				to launch modules
* Date: May 24th 2012 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#pragma once

#ifndef MODULE_LAUNCH_PROPERTIES_H
#define MODULE_LAUNCH_PROPERTIES_H

#include <QString>
#include <QStringList>

#include <vector>

#include "core/interfaces/debuggingInterface.h"
#include "core/utils/libHeader.h"

namespace Bzi
{

    class LIBRARY_API ModuleLaunchProperties
	{
		public:
			ModuleLaunchProperties();
			~ModuleLaunchProperties();

			static bool isRemote(const QString &ip);

			//attributes
			QString	mExeName,
					mName,
					mSshUser;

	};

	class LIBRARY_API RealTimeModuleLaunchProperties: public ModuleLaunchProperties,
													  public DebuggingInterface
	{
		public:
			//attributes
			QString	mTcpPort,
					mIp,
					mDisplayType,
					mStartDetached;

			//methods
            std::ostream& debugBzi(std::ostream &os) const;

	};

	class LIBRARY_API ManagerLaunchProperties:  public ModuleLaunchProperties,
												public DebuggingInterface
	{
		public:
			//attributes
			QString	mTcpPort,
					mIp,
					mNetConnectionTries,
					mStartDetached;

            typedef std::pair<QString,QString> Connection; //ip,port

            std::vector<Connection> mConnections; //connections to remote modules: real time or not

			//methods
            std::ostream& debugBzi(std::ostream &os) const;
	};

	typedef ManagerLaunchProperties::Connection ManagerConnection;

	class LIBRARY_API ControllerLaunchProperties:  public ModuleLaunchProperties,
												   public DebuggingInterface
	{
		public:
			//attributes
			QString	mManagerTcpPort,
					mManagerIp,
					mNetConnectionTries,
					mConfigFile,
					mStartDetached;

			QStringList mConfigs; //configuration names ordered
			//methods
            std::ostream& debugBzi(std::ostream &os) const;
	};
	
}


#endif //MODULE_LAUNCH_PROPERTIES_H
