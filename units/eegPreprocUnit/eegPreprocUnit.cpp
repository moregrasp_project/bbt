#define EEG_PREPROC_CPP

#include "eegPreprocUnit.h"

#ifdef _WIN32
#include <windows.h>
#endif

using namespace std;

namespace Bzi
{
    namespace Units
    {
        UNIT_FACTORY( EegPreprocUnit ) //register Unit in compilation time.

        //IIR
        IIR::IIR(int chans, int SR)
        {
            _mpNotchFilter = new MkfilterWrapper(chans, SR);
            _mpNotchFilter->Butterworth((filter_pass_t)BAND_STOP, 4, 48, 52);

            _mpFilter = new MkfilterWrapper(chans, SR);
            _mpFilter->Butterworth((filter_pass_t)HIGH_PASS, 4, 1, 30);
        }

        IIR::~IIR()
        {
            if (_mpNotchFilter)
                delete _mpNotchFilter;
            if (_mpFilter)
                delete _mpFilter;
        }

        void IIR::filter(SignalStream* in, SignalStream* out)
        {
            int n_blocks = in->size();
            out->resize(n_blocks);
            for (int b = 0; b < n_blocks; b++) {
                _mpNotchFilter->Apply(*in->block(b), *out->block(b));
                _mpFilter->Apply(*out->block(b));
            }
        }


        // BATT ESTIMATE
        BattEstimate::BattEstimate(int blockSize, int samplingRate, double sec_init, double sec)
            : _init(true),
              _counter(0),
              _sumValue(0),
              _currentState(-2)
        {
            // threshold
            _threshold.push_back(550);
            _threshold.push_back(560);
            _threshold.push_back(570);
            _threshold.push_back(580);
            _threshold.push_back(590);

            // max number of blocks in the first iteration and afterwards
            double blocksPerSec = double(samplingRate)/double(blockSize);
            _blocks_init = round(blocksPerSec*double(sec_init));
            _blocks = round(blocksPerSec*double(sec));
        }

        bool BattEstimate::take(SignalStream* stream, int& state)
        {
            int prevState = _currentState;

            for (size_t block=0; block<stream->size(); block++)
            {
                int batt_value = stream->block(block)->value(stream->rows()-1,0);    // battery level
                if (batt_value == -1)
                {
                    // usb charging
                    _currentState = -1;
                    // reset parameters
                    _init = true;
                    _counter = 0;
                    _sumValue = 0;
                }
                else
                {
                    // else...
                    _sumValue += batt_value;
                    _counter++;
                    if (_init)
                    {
                        if (_counter==_blocks_init)
                        {
                            double meanValue = double(_sumValue)/double(_counter);
                            int newState = level(meanValue);

                            _currentState = newState;

                            _counter = 0;
                            _sumValue = 0;
                            _init = false;
                        }
                    }
                    else
                    {
                        if (_counter==_blocks)
                        {
                            double meanValue = double(_sumValue)/double(_counter);
                            int newState = level(meanValue);

                            _counter = 0;
                            _sumValue = 0;

                            if (_currentState<0)
                            {
                                _currentState = newState;
                            }
                            else
                            {
                                if (newState < _currentState)
                                {
                                    _currentState = newState;
                                }
                            }
                        }
                    }
                }
            }

            state = _currentState;
            return (_currentState != prevState);
        }

        int BattEstimate::level(double meanValue)
        {
            for (size_t i=0; i<_threshold.size(); i++)
            {
                if (meanValue < _threshold[i])
                    return i;
            }
            return _threshold.size();
        }

        //CU ESIMATE
        CuEstimate::CuEstimate(EventStream* cuBattery)
        {
            _cuBattery = cuBattery;
            _cuBattery->resize(1);
            _cuBattery->block_n()->setValue(0,0,-1);
        }

        bool CuEstimate::get()
        {
             GetSystemPowerStatus(&stats);
             int value = stats.BatteryLifePercent;
             if (value != _cuBattery->block_n()->value(0,0)) {
                _cuBattery->block_n()->setValue(0,0,value);
                return true;
             }
             return false;
        }

        //EEG PREPROC
        EegPreprocUnit::EegPreprocUnit()
        {
            // constructor will be called once before initialize()
            // parameters CANNOT be accessed
        }

        EegPreprocUnit::~EegPreprocUnit()
        {
            // destructor will be called once after reset()
        }

        void EegPreprocUnit::initialize()
        {
            //params
            _cycle_time = getDouble("softCycle")*1e-3; //ms to sec

            //signals
            _inputEEGStream  = getSignalStream("eeg_raw_signal");
            _outputEEGStream = getSignalStream("eeg_signal");

            //events
            _eegImpedance   = getEventStream("eeg_impedance");
            _eegBattery     = getEventStream("eeg_battery");

            _eegConnection  = getEventStream("eeg_connection");
            _cuBattery      = getEventStream("cu_battery");

            //initialize
            _current_cycle = 0;
            _cycles_missed = 0;
            _iir = new IIR(_outputEEGStream->rows(),_outputEEGStream->samplingRate());
            std::vector<float> thresh;
            thresh.push_back(1);
            thresh.push_back(5);
            _imp = new Impedance(_outputEEGStream->rows(),_outputEEGStream->cols(),_outputEEGStream->samplingRate(), 12.0e4, thresh);
            _bat = new BattEstimate(_inputEEGStream->cols(), _inputEEGStream->samplingRate(), 5, 60);
            _cu  = new CuEstimate(_cuBattery);

        }


        void EegPreprocUnit::process()
        {
            //connection, initial
            if (_current_cycle==0) {
                _eegConnection->resize(1);
                _eegConnection->block_n()->setValue(0,0,1);
                setDataModified(_eegConnection);
            }

            _current_cycle++;

            //get data
            if (isDataRefreshed(_inputEEGStream))
            {
                //raw data
                _outputEEGStream->resize(_inputEEGStream->size());
                for (int b = 0; b < _outputEEGStream->size(); ++b) {
                    for (size_t i=0; i<_outputEEGStream->rows(); i++) {
                        for (size_t j=0; j<_outputEEGStream->cols(); j++) {
                            _outputEEGStream->block(b)->setValue(i,j,_inputEEGStream->block(b)->value(i,j));
                        }
                    }
                }

                //impedance
                bool full = _imp->process(_outputEEGStream);
                if (full) {
                    _eegImpedance->resize(1);
                    for (size_t ch=0; ch<_outputEEGStream->rows(); ch++) {
                        _eegImpedance->block_n()->setValue(ch,0,_imp->getValue(ch));
                        //cout << "ch " << ch << ": " << _imp->getValue(ch) << endl;
                    }
                    setDataModified(_eegImpedance);

                    //cu batt
                    bool newCu = _cu->get();
                    if (newCu)
                        setDataModified(_cuBattery);

                }

                //iir filter
                _iir->filter(_inputEEGStream, _outputEEGStream);
                for (int b = 0; b < _inputEEGStream->size(); ++b)
                    _outputEEGStream->block(b)->setTimestamp(_inputEEGStream->block(b)->timestamp());
                setDataModified(_outputEEGStream);

                //bat
                int battEvent;
                bool newBattEvent = _bat->take(_inputEEGStream, battEvent);
                if (newBattEvent) {
                    _eegBattery->resize(1);
                    _eegBattery->block(0)->setValue(0,0,battEvent);
                    setDataModified(_eegBattery);
                }

                //connection
                _cycles_missed = 0;
                if (_eegConnection->block_n()->value(0,0)!=1) {
                    _eegConnection->block_n()->setValue(0,0,1);
                    setDataModified(_eegConnection);
                }
            } else {
                //connection
                _cycles_missed++;
                if (_cycles_missed > (32*5)) {
                    if (_eegConnection->block_n()->value(0,0)!=0) {
                        _eegConnection->block_n()->setValue(0,0,0);
                        setDataModified(_eegConnection);
                    }
                }
            }
        }

        void EegPreprocUnit::reset()
        {
            delete _iir;
            delete _imp;
            delete _bat;
            delete _cu;
        }
    }
} // end namespace
