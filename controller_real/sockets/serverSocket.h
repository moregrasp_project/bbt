    /*! \class ServerSocket
    \brief A multi-platform wrapper to Berkeley sockets.

	A multi-platform wrapper to Berkeley sockets.
	A server (listening) socket for StreamSocket (TCP) connections.
	Only TCP is supported.
*/
#pragma once

#ifndef SERVER_SOCKET
#define SERVER_SOCKET

#include "socket.h"
#include "streamSocket.h"

namespace Bzi
{
    class ServerSocket : public Socket
	{
		public:

			/** Construct a new ServerSocket that will listen on the given port.
			*
			* @param port The port number to listen on.
			* @param backlog The size of the connection backlog.
			*/
			ServerSocket(int port, int backlog = 5);

			/** Destructor. Shuts down the socket. */
            ~ServerSocket() {}

			/** Initialize the socket. This method creates the underlying socket
			* object.
			*
			* @throw SocketException If the socket could not be created.
			*/
			void Init() ;

			/** Begin listening for connections on the socket.
			*
			* @throw SocketException If a socket error occurs.
			*/
			void Listen() ;

			/** Determine if the socket is in a listening state. */
			inline bool Listening() const
			{ return(mListening); }

			/** Accept a connection on the socket. This method blocks until a
			* new connection is pending, unless a timeout has been set on the
			* socket, in which case a TimeoutException is thrown if no
			* connection is pending by the time the timeout expires.
			*
			* @param socket A socket object which will be initialized to represent
			* the newly-established connection.
			* @throw TimeoutException If the operation times out.
			* @throw SocketException If a socket error occurs.
			*/
			StreamSocket* Accept() ;

			/** Shut down the socket. Closes the connection and destroys the
			* underlying socket object.
			*/
			int Shutdown(int shutMode=SHUT_RDWR);

		private:

			/** Local address. */
			struct sockaddr_in mLaddr;

			int		mBacklog;
			bool	mListening;
	};
}

#endif // SERVER_SOCKET
