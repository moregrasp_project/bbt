/*
* File: unitInfo.h	
* Description: class to keep info about an specific unit
* Date: April 08th 2011 
* Author: Carlos Escolano (cescolano@gmail.com)
*		  Soraya Santana (sorayasantana@bitbrain.es)
		  BitBrain Technologies
*/

#pragma once

#ifndef UNIT_INFO_H
#define UNIT_INFO_H

#include <string>

#include "core/data/repositoryInterface.h"

namespace Bzi
{
	class LIBRARY_API UnitInfo : public DebuggingInterface
	{
		public:

			enum UnitType 
			{
				INVALID_UNIT = -1,
				GENERIC_UNIT = 0,
				DISPLAY_UNIT,
                DISPLAY_UNIT_SYNCHRO,
				MAX_UNIT_TYPES
			};

			UnitInfo();
			
			UnitInfo(const UnitInfo &unitInfo);

            std::ostream& debugBzi(std::ostream &os) const;

			void marshall(QDataStream &qDataStream);
			void unmarshall(QDataStream &qDataStream);

            static UnitType type(const std::string &typeName);
            static std::string	typeName(UnitType type);

            std::string mClass;
            std::string mAlias;
			int	   mType;

		private:

            static std::string _msUnitTypeNames[MAX_UNIT_TYPES]; // to keep mapping of type values and its names
            static std::map<std::string,UnitType> _msUnitTypeMap; // to keep mapping of typenames and its values

            static std::map<std::string,UnitType> initTypeMap(); //_msUnitTypeMap initializer

			void _assign(const UnitInfo& unitInfo);
	};
}

#endif //UNIT_INFO_H
