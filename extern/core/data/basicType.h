/*
* File: basicType.h	
* Description: template class for wrapping any basic type
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef BASIC_TYPE_H
#define BASIC_TYPE_H

#include <string>
#include <sstream>

#include "core/data/data.h"
#include "core/exceptions/unknownDataException.h"

namespace Bzi
{
    template<typename T>
    class LIBRARY_API BasicType: public Data
    {
        public:
            BasicType(){ }

            BasicType(const T&val){_mVal = val;}

            BasicType(const BasicType<T> &obj)
            {
                _mVal = obj._mVal;
                setId(obj.getId());
            }

            ~BasicType(){ }

            inline void setValue(T val)
            {
                _mVal = val;
            }

            inline T value()
            {
                return _mVal;
            }

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const
            {
                os << _mVal;
                return os;
            }

            //MarshallingInterface
            inline void marshall(QDataStream &qDataStream)
            {
                Bzi::marshall(_mVal,qDataStream);
            }
            inline void unmarshall(QDataStream &qDataStream)
            {
                Bzi::unmarshall(_mVal,qDataStream);
            }

            Data* clone()
            {
                return new BasicType<T>(*this);
            }

            void setValueFromStr(const std::string &strValue);

            static T convertFromStr(const std::string &strValue);

            //matlab conversion interface
#ifdef COMPILE_MATLAB
            virtual void toMatlabMatrix(mxArray*& pMatlabArray);
            virtual void fromMatlabMatrix(mxArray* pMatlabArray);

        private:
            static void _toMatlabMatrixAsDouble(mxArray*& pMatlabArray,double val,const std::string &id)
            {
                pMatlabArray = mxCreateDoubleScalar( val );
            }

            static void _fromMatlabMatrixAsDouble(mxArray* pMatlabArray,double &val,const std::string& id)
            {
                if ( !pMatlabArray )
                {
                    std::ostringstream stringStream;
                    stringStream<<"Bzi::BasicType<T>::fromMatlabMatrix error:\n\t Data  not found in Matlab workspace.\n"
                               <<"\t"<<__FILE__<<":"<<__LINE__<<std::endl;
                    throw UnknownDataException( id, stringStream.str() );
                }

                double* pMatlabResulValue = mxGetPr( pMatlabArray );
                if( pMatlabResulValue )
                    val = *pMatlabResulValue;
            }

            virtual bool checkStr(const std::string &)
            /**
             *@param const std::string &strValue: std::string to check format.
             *@pre PRE: --
             *@post POST: Always return false... (Must be redefined in childs)
             */
            {
                return false;
            }
#endif //COMPILE_MATLAB

        protected:
            T _mVal;
            std::string _mAllias;
    } ;
}
#endif //BASIC_TYPE_H
