/*
* File: data.h	
* Description: class defining data that modules process (in its pipeline) and emits as a result.
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef DATA_H
#define DATA_H	

#include "core/configuration/initializingParameter.h"
#include "core/network/marshallingInterface.h"
#include "core/interfaces/debuggingInterface.h"
#include "core/interfaces/cloningInterface.h"
#include "core/interfaces/armadilloInterface.h"
#include "core/interfaces/matlabInterface.h"
#include "core/exceptions/noMemoryException.h"
#include "core/utils/libHeader.h"

#include <typeinfo>
#include <set>
#include <sstream>
#include <memory>

namespace Bzi
{

#define RegisterData( name )  Data::GenericRegister<name> name##Register;

    //class to manage different data polimorfically from NetworkMessage
    class LIBRARY_API Data :public DebuggingInterface,
            public MarshallingInterface,
            public DataClonningInterface,
            public ArmadilloInterface,
            public MatlabInterface
    {
            friend class GenericModule;
            friend class RealTimeModule;
            friend class Manager;
            friend class Controller;
            friend class Repository;
            friend class DataMessage;
            friend class NetworkBox;
            friend class GenericUnit;
            friend class DisplayUnit;

        public:
            Data();
            virtual ~Data();

            void setId(const std::string &id);
            const std::string getId() const;

        protected:
            void setModified();
            void resetModified();
            bool isModified();

            //redefinition allowed
            virtual void setValueFromStr(const std::string &) { }
            virtual void initializeData(const InitParameterMap & ){ }
            virtual bool checkStr(const std::string &)
            {
                std::cout << "Data::checkStr() ..."<< std::endl;
                return false;
            }

        public:
            //matlab conversion interface
#ifdef MATLAB
            virtual void toMatlabMatrix(Engine *)
            {
                std::cout << " id: "<< _mId <<" Data::toMatlabMatrix() MUST BE DEFINED" <<std::endl;
            }

            virtual void fromMatlabMatrix(Engine *)
            {
                std::cout << " id: "<< _mId<< " Data::fromMatlabMatrix() MUST BE DEFINED" <<std::endl;
            }
#endif

            static Data* newData( const std::string& name );

        protected:
            std::string	_mId;
            bool	_modified;
            virtual void resetModifiedDerived() {}

        private:
            class LIBRARY_API Register
            {
                public:

                    Register();
                    virtual ~Register();

                    virtual const std::type_info& typeId() const = 0;
                    virtual Data* newInstance() const = 0;

                    typedef std::set<Register*> RegisterSet_;
                    static RegisterSet_& registers();

                private:
                    int				_mInstance;
                    static int		_mCreatedInstances;
            };
            typedef Register::RegisterSet_ RegisterSet;

        public:
            template<class T> class GenericRegister : public Register
            {
                public:
                    GenericRegister()
                        : Register() {}

                    virtual const std::type_info& typeId() const
                    {
                        return typeid( T );
                    }

                    virtual Data* newInstance() const
                    {
                        std::auto_ptr<T> dataPtr(new T);
                        if( !dataPtr.get() )
                        {
                            std::ostringstream stringStream;
                            stringStream<<"GenericRegister::newInstance() error instanciatinf Data: \n \t Not enough memory.\n"
                                       <<"\t"<<__FILE__<<":"<<__LINE__<<std::endl;
                            throw NoMemoryException(stringStream.str().c_str());
                        }

                        return dataPtr.release();
                    }
            };
    };
}

#endif // DATA_H	
