#ifndef PBAND_H
#define PBAND_H

#include <iostream>
#include <vector>
#include <fstream>

#include "core/utils/libHeader.h"
#include "mkfilterWrapper.h"

//#define POW_BAND_DEBUG

namespace Bzi
{
    class LIBRARY_API Pband
    {
        public:

            Pband(int channels, int bufferSize, int blockstoUpdate, int samplingRate, int blockSize, double leftCorner, double rightCorner);
            ~Pband();

            bool process(DataBlock* signal, std::vector<float>& out);
            void addNotch(double leftCorner, double rightCorner);
            void notch(bool on);

        private:
            int					_channels;
            int					_bufferSize;
            int					_blockstoUpdate;
            int                 _samplingRate;
            int                 _blockSize;
            int					_pointerWrite;
            int					_newBlock;
            std::vector<double>	_buffer;
            bool                _full;
            bool                _ready2Update;
            MkfilterWrapper *	_notchFilter;
            MkfilterWrapper *	_bandFilter;
            DataBlock *         _signalNotchFiltered;
            DataBlock *         _signalFiltered;
            double              _leftCornerNotch, _leftCorner;
            double              _rightCornerNotch, _rightCorner;

            bool                _hasData();
            void                _compute(std::vector<float>& out);

            #ifdef POW_BAND_DEBUG
            std::ofstream       outStream;
            #endif

    };
}
#endif // PBAND
