/*! \class SocketList
    \brief A class that allows performing select operations over a socket list.

	This class allows setting up a list of socket from which perform a select operation
	to evaluate wheter there are some reading pending connections on them. The usage is
	as follows:
		Add( socket1 );
		...
		Add( socketN );
		n = Select();
		for (int i=0; i<n; i++) {
			readySocket = operator( i );
			...
		}
*/
#pragma once

#ifndef SOCKET_LIST_H
#define SOCKET_LIST_H

#include "socket.h"
#include "streamSocket.h"
#include <vector>
#include <list>

namespace Bzi
{
    class SocketList
	{
		public:
			SocketList();
            virtual ~SocketList() {}

			/** Adds an initialized socket to the list */
			void	Add( Socket* s );

			/** Deletes a socket from the list */
			void	Delete( Socket* s );

			/** Performs a select operation over the socket list. The available sockets must be accessed
			* throught the operator().
			*
			* @param msec The timeout, in milliseconds. Timeout < zero disables the timeout.
			* @return The number of sockets which are ready to read data.
			*/
			int		Select( int msec );

			/** Select method fills a ready socket list, which must be accessed throught this method.
			*
			* @param index The available socket index, which must be in the range [0..n], being n the
			* last returned value in the call to Select method.
			* @return An available socket to read.
			*/
			Socket*	operator[](size_t index)
            { return mReadySocketList[ index ]; }

			/*
			* debugging purposes
			*/
            std::ostream& debugBzi(std::ostream& os) const
			{
                os << "Bzi::SocketList: "<< std::endl;
				for (SocketsVector::const_iterator it = mSocketList.begin(); it != mSocketList.end(); it++) 
                    os <<"\t Socket: " << (*it) << std::endl;
				
				return os;
			}

            std::list<std::pair<std::string,unsigned int> > remoteConnectionList();


            typedef std::vector<Socket*> SocketsVector;

			/** Socket list to perform the select operations */
			SocketsVector mSocketList;
			/** Ready socket list, filled by the Select method */
			SocketsVector mReadySocketList;

        private:
            void _updateMaxFd();

			/** Select attributes */
			fd_set mFdSet;
			fd_set mFdSetTmp;
			int mMaxFd;
			struct timeval* mpTimeval;
	};
}
#endif // SOCKET_LIST_H
