/*!	\class MkfilterWrapper

	\brief A base class that implements an Infinite Impulse Response (IIR) filter .

	A base class that implements an Infinite Impulse Response (IIR) filter.
	Implements Butterworth and Chebyshev filters of order n.
	It is based on the A.J. Fisher's mkfilter library.
	http://www-users.cs.york.ac.uk/~fisher/mkfilter/

	Filter parameters:
		butterworth, {lowpass | highpass | bandpass | bandstop}, order, corner1, corner2
		chebyshev, {lowpass | highpass | bandpass | bandstop}, ripple_dB, order, corner1, corner2

	Those parameters design the filter properties:
		- overall gain,
		- complex roots of the numerator polynomial ("zeros"),
		- complex roots of the denominator polynomial ("poles").

*/
#pragma once

#ifndef MKFILTER_WRAPPER_H
#define MKFILTER_WRAPPER_H

#include "libraries/mkFilter/mkfilter.h"
#include "core/data/dataStream.h"
#include "core/data/dataBuffer.h"
#include "core/utils/libHeader.h"

namespace Bzi
{

    class LIBRARY_API MkfilterWrapper
	{
		public:
			//	Initialize with the input signal properties
            MkfilterWrapper( int inChannels, int inSamplingRate );
            ~MkfilterWrapper() {}

			//	Filter Designs
			void Butterworth( filter_pass_t inFilterPass, int inOrder, double inCorner1, double inCorner2 = -1 );
			void Chebyshev( filter_pass_t inFilterPass, double inRipple_dB, int inOrder, double inCorner1, double inCorner2 = -1 );

			// Apply the Designed Filter to an input signal
            void	Apply( DataBlock& inSignal, DataBlock& outSignal );
            void	Apply( DataBlock& inoutSignal );
            void	Apply( DataBuffer& inoutSignal );

			// Return the coefs
			void	Coefs( int& zeros, double*& xcoefs, int& poles, double*& ycoefs, double& gain );

		private:
			// Signal Properties
			int mChannels,
				mSamplingRate;

			// Filter Properties
			int				mNumZeros,mNumPoles;
			real_vector		mXcoeffs, mYcoeffs;
			real_type		mGain;

			// Filtering Buffers
            typedef std::vector<real_vector> ChannelBuffers;
			ChannelBuffers	mXbuffers, mYbuffers;

	};
}

#endif // MKFILTER_WRAPPER_H
