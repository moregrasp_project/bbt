#ifndef RECEIVER_H
#define RECEIVER_H

#include <fstream>

#include <QThread>
#include <QMutex>
#include "messagequeue.h"
#include "sockets/streamSocket.h"
#include "jsonstreamreader.h"
#include "core/utils/hat.h"


class Receiver : public QThread
{
        Q_OBJECT
    public:
        Receiver(Bzi::StreamSocket* const socket, unsigned int timeout = 0);
        ~Receiver();
        void stop();

    signals:
        void pendingMessage(const QJsonObject& msg);
    private:
        void run();
        JsonStreamReader _reader;
        QMutex _mutex;
        bool _stop;
};

#endif // RECEIVER_H
