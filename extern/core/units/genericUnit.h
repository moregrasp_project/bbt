/*
* File: genericUnit.h	
* Description: class defining a generic unit as an abstract class.
*			   This is the base class for all implemented units and
*			   provides the static methods and attributes to register all units
*			   and create unit instances.
* Date: April 08th 2011 
* Author: Carlos Escolano (cescolano@gmail.com)
          BitBrain Technologies
*/


#pragma once


#ifndef GENERIC_UNIT_H
#define GENERIC_UNIT_H

#include <string>
#include <set>

#ifdef UNIX
#include <dlfcn.h>
#endif

#ifndef UNIX
#include <winsock2.h>
#include <windows.h>
#endif

#include <QLibrary>

#include "core/utils/libHeader.h"
#include "core/data/signalStream.h"
#include "core/data/eventStream.h"

namespace Bzi 
{
#ifndef UNIX
#define UNIT_FACTORY(MyUnit) extern "C" __declspec(dllexport) Bzi::GenericUnit* create_unit() { return new MyUnit(); }
#else
#define UNIT_FACTORY(MyUnit) extern "C" Bzi::GenericUnit* create_unit() { return new MyUnit(); }
#endif
#define UNIT_FACTORY_FUNCTION "create_unit"


	/**
	 * @brief GenericUnit Class
	 * Parent class of all Units
	 * Includes the functionality to dynamically load unit in runtime
	 */
    class Configuration;
    class RepositoryInterface;
    class Hat;

	class LIBRARY_API GenericUnit
	{
        friend class DisplayUnit;

	public:

		/**
		 * @brief GenericUnit Default Constructor
		 */
		GenericUnit()
			:_mpRepo(NULL),
			  _mAlias("")
		{
		}

		/**
		 * @brief ~GenericUnit Destructor
		 */
		virtual ~GenericUnit() {
		}

		/**
         * @brief createFromLibrary Static function to create a unit from library at runtime
         * @param libraryName The name of the library without spaces
		 * @return
		 */
		static GenericUnit* createFromLibrary(std::string libraryName, QMap<std::string,QLibrary*> &lib_map);

		/**
		 * @brief setRepo Sets the repository to load/write data
         * @param pRepo Pointer to the respository
		 */
        void setRepo(RepositoryInterface *pRepo, Hat* pClock);

        /**
         * @brief setAlias Sets an alias name to the unit
         * @param alias Alias name
         */
        void setAlias(const std::string &alias);
        std::string getAlias() const;

        /**
         * @brief setGlobalConfig Passes the configuration
         * @param pConfig Pointer to the configuration
         * @return
         */
        void setGlobalConfig(Configuration *pConfig);

        /**
         * @brief setBlockTime Passes a timestamp
         * @param timestamp
         * @return
         */
        void setBlockTime(qint64 timestamp);

        /**
         * @brief inputNames returns the names of the subcripted signals and event
         */
        std::vector<std::string> inputNames() const;

        /**
         * @brief outputNames returns the names of the published signals and event
         */
        std::vector<std::string> outputNames() const;

        /**
         * @brief parameterNames returns the names of the parameters
         */
        std::vector<std::string> parameterNames() const;

        /**
         * @brief hat implements a high accuracy timer, returns a timestamp
         */
        qint64 hat() const;

        /**
         * @brief isDataRefreshed returns whether a signal or event is refreshed
         */
        bool isDataRefreshed(const std::string& name);
        bool isDataRefreshed(Data* d);

        /**
         * @brief setDataModified marks a signal or event as modified
         */
        void setDataModified(const std::string& name);
        void setDataModified(Data* d);

        /**
         * @brief accessors for parameters
         */
        bool                        existParam(const std::string& name) const;

        int                         getInt(const std::string& name) const;
        double                      getDouble(const std::string& name) const;
        bool                        getBool(const std::string& name) const;
        std::string                 getString(const std::string& name) const;

        std::vector<int>            getIntVector(const std::string& name) const;
        std::vector<double>         getDoubleVector(const std::string& name) const;
        std::vector<bool>           getBoolVector(const std::string& name) const;
        std::vector<std::string>    getStringVector(const std::string& name) const;

        Data*                       getGenericParam(const std::string& name);

        /**
         * @brief accessors for signals and events
         */
        bool                        isSignalStream(const std::string& name) const;
        bool                        isEventStream(const std::string& name) const;
        SignalStream*               getSignalStream(const std::string& name);
        EventStream*                getEventStream(const std::string& name);

        Data*                       getGenericData(const std::string& name);

		/**
		 * @brief initialize function. It will be called before the process() function of all loaded units
		 */
        virtual void initialize() = 0;
		/**
		 * @brief initialize function. It will be called after the initialize() function of all loaded units
		 */
		virtual void process() = 0;
		/**
		 * @brief reset function. It will be called before closing the unit and once all process() functions have finished
		 */
        virtual void reset() = 0;

    private:
        std::string _mAlias; ///< Alias of the unit
        RepositoryInterface *_mpRepo; ///< Repository to write/read data
        Hat* _mpClock;

        std::string fullName(const std::string& name) const;

	protected:
		Configuration *_mpConfig; ///< Repository to write/read data
        qint64  _mBlockTime;
	};
}

#endif //GENERIC_UNIT_H
