/*
* File: noMemoryException.h	
* Description: exception produced when not enough memory for allocating data
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef NO_MEMORY_EXCEPTION_H
#define NO_MEMORY_EXCEPTION_H

#include "core/exceptions/exception.h"
#include <string>

namespace Bzi
{
	class NoMemoryException : public Exception
	{
		public:

            NoMemoryException(const std::string& message)
				:_mMessage(message){}

            std::ostream& debugBzi(std::ostream& os) const
			{ 
				return os << _sName
						  << ":\n\t"
						  <<_mMessage
                          <<std::endl;
			}

		private:
		
            std::string _mMessage;
            static const std::string _sName;
		
	};
}

#endif //NO_MEMORY_EXCEPTION_H
