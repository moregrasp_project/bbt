/*
* File: vectorType.h	
* Description: template class for wrapping any vector of basic type
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef VECTOR_TYPE_H
#define VECTOR_TYPE_H	

#include "core/data/data.h"
#include "core/exceptions/unknownDataException.h"
#include "core/data/basicDataTypes.h"

#include <vector>
#include <sstream>

namespace Bzi
{

    template<typename T>
    class LIBRARY_API VectorType: public std::vector<T>,
            public Data
    {
        public:

            VectorType(){}
            VectorType(const std::vector<T>&vect)
                :std::vector<T>(vect)
            {}
            virtual ~VectorType(){}

            //debugging interface
            std::ostream& debugBzi(std::ostream& os) const
            {
                os << "Vector: [";
                for (size_t i = 0; i < this->size(); i++)
                {
                    os << " ";
                    (*this)[ i ].debugBzi(os);
                }

                os << " ];" << std::endl;

                return os;
            }

            //MarshallingInterface
            void marshall(QDataStream &qDataStream)
            {
                int size = this->size();
                Bzi::marshall ( size, qDataStream );
                for (size_t i = 0; i < this->size(); i++)
                    Bzi::marshall ( this->at( i ), qDataStream );
            }

            void unmarshall(QDataStream &qDataStream)
            {
                int size;
                this->clear();
                Bzi::unmarshall ( size, qDataStream );
                for (int i = 0; i < size; i++)
                {
                    T elem;
                    Bzi::unmarshall ( elem, qDataStream );
                    this->push_back(elem);
                }
            }

            Data* clone()
            {
                return new VectorType<T>(*this);
            }

            //virtual void setValueFromStr (const std::string &strValue) {} //number sintaxs can be different
            //depending on T so this method will ve redefined
            virtual bool checkStr(const std::string &)
            {
                std::cout <<"checkStr de vectorType..." << std::endl;
                return false;
            }

            void setValue(T value, int index)
            {
                (*this)[index] = value;
            }

            std::string toStr() const
            {
                std::stringstream ss;
                ss << "[";
                for (size_t i = 0; i < this->size(); i++) {
                    ss << this->at(i);
                    if (i < (this->size()-1)) ss << " ";
                }
                ss << "]";
                return ss.str();
            }

            //matlab conversion interface
#ifdef COMPILE_MATLAB
            virtual void toMatlabMatrix(mxArray*& pMatlabArray);
            virtual void fromMatlabMatrix(mxArray* pMatlabArray);
#endif
    };

}

#endif // VECTOR_TYPE_H	


