/*
* File: MoregraspGui.h
* Description: main application to load a configuration file
* Author: Carlos Escolano (carlos.escolano@bitbrain.es)
*		  BitBrain Technologies
* Date: 01/04/2015
*/
#ifndef MOREGRASP_GUI_H
#define MOREGRASP_GUI_H

// widgets stuff
#include <QtWidgets>

class MoregraspGui : public QWidget
{
	Q_OBJECT

	public:
        MoregraspGui(QString name);
        ~MoregraspGui() {}

	private:

		// Controller
		bool				_configured;
		QString				_configFilename;
		QString				_subconfig;

		// Widgets
		// -- Config Groupbox
		QLineEdit*			_pLoadLine;
		QFormLayout*		_pLoadFormLayout;
		QPushButton*		_pLoadButton;
		QGridLayout*		_pConfigLayout;
		QGroupBox*			_pConfigBox;
		// -- Subconfig Combobox
		QComboBox*			_pComboBox;
		QPushButton*		_pLaunchButton;
        QPlainTextEdit*     _pConfigText;
		QGridLayout*		_pSubconfigLayout;
		QGroupBox*			_pSubconfigBox;
		// -- Status Line
        QPlainTextEdit*		_pStatusLog;
		// -- Control
		QPushButton*		_pStartButton;
		QPushButton*		_pStopButton;
		QPushButton*		_pResetButton;
        QPushButton*		_pSendDataButton;
        QPlainTextEdit*     _pOperationText;
        QGridLayout*		_pOperationLayout;
        QGroupBox*          _pOperationBox;
		// -- Global
		QGridLayout*		_pGridLayout;

        void	closeEvent(QCloseEvent*) {emit sigFinish();}

	signals:
		void	sigLoadConfig(QString filename);
        void	sigLaunch(QString name, QString params);
        void	sigStart();
		void	sigStop();
		void	sigReset();
        void    sigFinish();
        void    sigSendData(QString data);

	public slots:
		void	onLoadedConfig(QStringList names);
		void	onLaunched(bool ok);
        void    onReseted(bool ok);
        void    onStatus(QString text);

	private slots:
		void	onLoadButton();
		void	onLaunchButton();
		void	onStartButton();
		void	onStopButton();
		void	onResetButton();
        void    onSendDataButton();

};

#endif // MOREGRASP_GUI_H
