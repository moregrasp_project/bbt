/*
* File: matlabInterface.h	
* Description: interface to convert a data object to-from matlab matrixes
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef MATLAB_INTERFACE_H
#define MATLAB_INTERFACE_H

#ifdef COMPILE_MATLAB
//matlab engine inclusion
#include "engine.h"
#endif

#include "core/utils/libHeader.h"
#include <string>

namespace Bzi
{
    /**
     * @brief The MatlabInterface class
     *      BziInt, BziDouble, BziBool, BziString, BziChar
     *      BziIntVector, BziDoubleVector, BziBoolVector, BziStringVector
     *      DataStream, SignalStream, EventStream
     */
    class LIBRARY_API MatlabInterface
    {
        public:
            virtual ~MatlabInterface() {}

#ifdef COMPILE_MATLAB
            enum
            {
                DEFAULT_VERBOSE_BUFFER = 512
            };

            // allocates dynamic memory, it is programmers' responsability to destroy it
            virtual void toMatlabMatrix(mxArray*&) {}
            virtual void fromMatlabMatrix(mxArray*) {}

            static Engine*  eng;
            static int      n_instances;
            static char*    outputBuffer;


            static Engine *initMatlabSession();
            static Engine *initMatlabSingleSession();
            static void closeMatlabSession();
            static void engBuffer(bool enable);

            static void engPutData(Engine *pMatlabEng, std::string id, mxArray *pMatArray);
            static mxArray* engGetData(Engine *pMatlabEng, std::string id);
            static char* engEvalCommand(Engine *pMatlabEng, const std::string &command,bool verbose=false);

#endif      
    };
}
#endif //MATLAB_CONVERSION_INTERFACE_H

