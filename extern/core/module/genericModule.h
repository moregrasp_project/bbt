/*
* File: genericModule.h
* Description: abstract class defining modules structure
*			   and basic methods
* Date: March 11th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
*		  BitBrain Technologies
*/

#pragma once

#ifndef GENERIC_MODULE_H
#define GENERIC_MODULE_H

#include <QThread>
#include <QApplication>

#include "core/interfaces/debuggingInterface.h"
#include "core/configuration/configuration.h"
#include "core/network/receptionIdentifier.h"
#ifdef LOGGING_1
#include "core/utils/logger.h"
#include "core/utils/hat.h"
#endif

namespace Bzi
{
    class NetworkBox;
    class DataMessage;

    class LIBRARY_API GenericModule : public QObject, public DebuggingInterface
    {
            Q_OBJECT

        public:

            enum
            {
                DEFAULT_TCP_PORT = 1950,
                DEFAULT_REPO_BUFFER_LENGTH = 1
            };

            enum DataIssueMode
            {
                ALL_OUTPUTS,
                MODIFIED_OUTPUTS,
                MODIFIED_AND_REFRESHED_OUTPUTS

            };

            enum TickMode
            {
                ON,
                OFF

            };

            // Module execution states
            enum ModuleState
            {
                RUNNING,
                STOPED,
                FINISHED
            };

            GenericModule(const std::string& name,
                          bool secondaryThread,
                          unsigned int tcpPort);
            virtual ~GenericModule();

            // naming interface
            virtual const std::string name() const = 0;

            void start();
            void startThread(){_mpMyThread->start();}
            void quitThread(){_mpMyThread->quit();}
            inline bool multiThread(){ return _mMultiThread; }

            const Configuration& currentConfiguration();

        signals:
            void startSignal();
            void quitSignal();

        public slots:
            //Entry into network loop
            virtual void run(){ }
            virtual void finish();

            void sendData(DataMessage *message, const std::string& moduleName);
            void sendData(Data *pData, const std::string& moduleName);
            void sendData(std::vector<Data *>data, const std::string& moduleName);
            void sendRepository(DataIssueMode issueMode = MODIFIED_OUTPUTS,TickMode tickMode=ON);
            void sendRepository(const std::string &moduleName,DataIssueMode issueMode = MODIFIED_OUTPUTS,TickMode tickMode=ON);
            void sendTick(const std::string &moduleName);
            void sendFinish(const std::string &moduleName);

        protected:

            //attributes
            const std::string _mName;
            NetworkBox* _mpNetBox;
            RepositoryInterface *_mpRepo;
            ModuleState  _mCurrentState;
            bool	_mMultiThread;

            DataMessage *_mpOutputDataMessage;
            DataIssueMode _mIssueMode;

            //methods
            void _disconnectRemoteModules();
            virtual void _packageOutputData(DataIssueMode issueMode,DataMessage* pOutputDataMsg);

#ifdef LOGGING_1
            class DataStats
            {
                public:
                    DataStats(const std::string& in_id, int in_n):
                        id(in_id), n(in_n) {}

                    std::string id;
                    int n;
            };

            std::vector<DataStats> _mLastDataRecv;
            std::vector<DataStats> _mLastDataSent;
            DataStats _getDataStats(Data* data);
            QString _dataStatsStr(const std::vector<DataStats>& dataStats);
#endif

        private:
            QThread *_mpMyThread;

            //methods
            void _initialize();
        private slots:
            void _finalize();
            void _threadFinished()
            {
                std::cout << "_threadFinished" << std::endl;
            }
    };
}

#endif //GENERIC_MODULE_H
