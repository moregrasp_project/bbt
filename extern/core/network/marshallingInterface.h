/*
* File: marshallingInterface.h	
* Description: class defining an interface with methods to marshall/unmarshall network messages
* Date: -
* Author: BitBrain Technologies
*/

#pragma once

#ifndef MARSHALLING_INTERFACE_H
#define MARSHALLING_INTERFACE_H

#include "marshallingInterfaceWrapper.h"
#include "core/utils/libHeader.h"

namespace Bzi
{

	class LIBRARY_API MarshallingInterface
	{
		public:
            virtual void marshall(QDataStream & qDataStream) = 0;
            virtual void unmarshall(QDataStream & qDataStream) = 0;
	};
}

#endif //MARSHALLING_INTERFACE_H
