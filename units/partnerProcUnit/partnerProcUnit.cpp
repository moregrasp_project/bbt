    #define PARTNER_PROC_UNIT_CPP

#include "partnerProcUnit.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace std;

namespace Bzi
{
    namespace Units
    {
        UNIT_FACTORY( PartnerProcUnit ) //register Unit in compilation time.

        PartnerProcUnit::PartnerProcUnit()
        {
            // constructor will be called once before initialize()
            // parameters CANNOT be accessed
        }

        PartnerProcUnit::~PartnerProcUnit()
        {
            // destructor will be called once after reset()
        }

        void PartnerProcUnit::initialize()
        {
            // PARAMETERS CAN BE ACCESSED ------------------------------------------
                // basic data types
                int dummyInt            = getInt("dummyInt");
                double dummyDouble      = getDouble("dummyDouble");
                bool dummyBool          = getBool("dummyBool");
                std::string dummyString = getString("dummyString");
                // vector data types of basic data types
                std::vector<int> dummyIntVector             = getIntVector("dummyIntVector");
                std::vector<double> dummyDoubleVector       = getDoubleVector("dummyDoubleVector");
                std::vector<bool> dummyBoolVector           = getBoolVector("dummyBoolVector");
                std::vector<std::string> dummyStringVector  = getStringVector("dummyStringVector");
            // ---------------------------------------------------------------------


            // PROPERTIES OF THE SIGNAL STREAMS AND EVENTS CAN BE ACCESSED ---------
            // - only those signal streams and events declared as inputs or outputs in
            // in the configuration file are accessible
            // - note that at this point only the following properties are readable,
            // data matrix values and timestamps are only accesible in process()
            _mpInputStream  = getSignalStream("STREAM_EEG");
            int in_stream_rows = _mpInputStream->rows();
            int in_stream_cols = _mpInputStream->cols();
            int in_stream_sr   = _mpInputStream->samplingRate();

            _mpOutputStream  = getSignalStream("STREAM_DUMMY");
            int out_stream_rows = _mpOutputStream->rows();
            int out_stream_cols = _mpOutputStream->cols();
            int out_stream_sr   = _mpOutputStream->samplingRate();

            _mpInputEvent  =getEventStream("EVENT_DUMMY");
            int out_event_rows_dummy = _mpInputEvent->rows();
            int out_event_cols_dummy = _mpInputEvent->cols();
            std::vector<double> out_event_init_dummy = _mpInputEvent->initValues();
            // ---------------------------------------------------------------------


            // We want to do some dummy processing (copy identical signal from STREAM_EEG to STREAM_DUMMY)
            // when we receive an DUMMY EVENT then we sleep for some ms to simulate overload
            _time2sleep = getInt("time2sleep");
            _tot_blocks = 0;
            _simulate_load = false;
        }


        void PartnerProcUnit::process()
        {
            bool refreshed;
            int n_blocks = 0;

            // usually we would check whether the input data (either signal stream or event) was modified in last processing cycle
            refreshed = isDataRefreshed("EVENT_DUMMY");
            if (refreshed)
            {
                _simulate_load = true;
            }

            refreshed = isDataRefreshed("STREAM_EEG");
            if (refreshed)
            {
                // when data was refreshed, we can receive one or more data blocks along with their timestamp
                // a timestamp is received for each data block
                n_blocks = _mpInputStream->size();

                _tot_blocks+=n_blocks;
                cout << "n blocks: " << _tot_blocks << endl;

                // this code shows how to get the timestamp and data matrix values for each block
                _mpOutputStream->resize(n_blocks);
                for (size_t b = 0; b < n_blocks; b++)
                {
                    SignalBlock* input_block  = _mpInputStream->block(b);
                    SignalBlock* output_block = _mpOutputStream->block(b);

                    for (size_t ch = 0; ch < _mpInputStream->rows(); ch++)
                        for (size_t sample = 0; sample < _mpInputStream->cols(); sample++)
                            output_block->setValue(ch, sample, input_block->value(ch, sample));
                    output_block->setTimestamp(hat());
                }
                setDataModified("STREAM_DUMMY");

                if (_simulate_load)
                {
                    if (_time2sleep>0)
                        Sleep(_time2sleep);
                    _simulate_load = false;
                }
            }
        }

        void PartnerProcUnit::reset()
        {
        }
    }
} // end namespace
