/*
* File: networkException.h	
* Description: exception produced by any kind of network error	   
* Date: October 28th 2011 
* Author: Soraya Santana (sorayasantana@bitbrain.es)
*		  BitBrain Technologies
*/

#pragma once

#ifndef NETWORK_EXCEPTION_H
#define NETWORK_EXCEPTION_H

#include "core/exceptions/exception.h"
#include <string>

namespace Bzi
{
    class NetworkException : public Exception
	{
		public:

            NetworkException(const std::string& message);
            virtual std::ostream& debugBzi(std::ostream& os) const ;

		protected:

            std::string _mMessage;

		private:

            static const std::string _sName;
	};
}

#endif //NETWORK_EXCEPTION_H
